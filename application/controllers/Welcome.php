<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct(){
			parent::__construct();
			$this->load->model('login_model');
			$this->load->library('Pdf_report');
			$this->load->library('csvreader');
			
			
		}
	public function index()
	{

		if(isset($_POST['login'])){
			$query = $this->login_model->getLoginAccount();
			$empCount = $this->login_model->employeeCount();
			$userinfo = $this->login_model->getUserinfo($query->row()->employee_ID);
			if($query->row()==TRUE && $query->row()->user_type=="admin"){
				$_SESSION['user_logged'] = TRUE;
				$_SESSION['admin_logged']= TRUE;
				$_SESSION['user_type'] = $query->row()->user_type;
				$_SESSION['employee_id'] = $query->row()->employee_ID;
				$_SESSION['user_id'] = $query->row()->user_ID;
				$_SESSION['fullname'] = $userinfo->row()->FirstName." ".$userinfo->row()->LastName;
				redirect('Welcome/inside/','refresh');
			}
			else if($query->row()==TRUE && $query->row()->user_type=="employee"){
				$_SESSION['user_logged'] = TRUE;
				$_SESSION['employee_logged'] = TRUE;
				$_SESSION['user_type'] = $query->row()->user_type;
				$_SESSION['employee_id'] = $query->row()->employee_ID;
				$_SESSION['user_id'] = $query->row()->user_ID;
				$_SESSION['fullname'] = $userinfo->row()->FirstName." ".$userinfo->row()->LastName;
				redirect('Welcome/inside/','refresh');
			}
			
			else{
				$this->session->set_flashdata("errorlog","Account does not exist.");
				redirect('Welcome/index');
			}
		}
		else if(isset($_SESSION['user_logged'])){
			redirect("Welcome/inside");
		}
		else{
			
			$this->load->view('login/index');
		}
	}
	public function logout(){
		session_destroy();
		redirect('Welcome','refresh');
	}
	public function inside(){
		if(isset($_SESSION['user_logged'])){
			$data['employees'] =  $this->login_model->getAllEmployee();
			if($_SESSION['user_type']=="admin"){
				$data['attendanceList'] = $this->login_model->getAttendanceList();
			}else{
				$data['attendanceList'] = $this->login_model->getAttendanceListOne($_SESSION['employee_id']);
			}
			
			$this->load->view('user/index',$data);
		}else{
			redirect('Welcome/index/','refresh');
		}
	}

	public function employee(){
		if(isset($_SESSION['admin_logged'])){
			$data['employees'] =  $this->login_model->getAllEmployee();
			$data['divisionlist'] = $this->login_model->getDivisions();
			$this->load->view('user/employee-list',$data);
		}else{
			redirect('Welcome/inside');
		}
	}
	public function employee_info($employee_id){
		$data['employee_info'] =  $this->login_model->getAllinfo($employee_id);
		$data['division'] =$this->login_model->getDivisions();
		$this->load->view('user/employee-full',$data);
	}
	public function employee_add(){
		if(isset($_POST['addEmp'])){
			$data= array(
				'FirstName'=>$_POST['FirstName'],
				'MiddleName'=>$_POST['MiddleName'],
				'LastName'=>$_POST['LastName'],
				'BioName'=>$_POST['BioName'],
				'Gender'=>$_POST['Gender'],
				'Address'=>$_POST['Address'],
				'ContactNumber'=>$_POST['ContactNumber'],
				'Birthdate'=>$_POST['Birthdate'],
				'MaritalStatus'=>$_POST['MaritalStatus'],
				'DateHired'=>$_POST['DateHired'],
				'PreviousWork'=>$_POST['PreviousWork'],
				'Salary'=>$_POST['Salary'],
				'Position'=>$_POST['Position'],
				'Division'=>$_POST['Division'],
				'EmploymentStatus'=>$_POST['EmploymentStatus'],
				'OfficeAssign'=>$_POST['OfficeAssign'],
				'email'=>$_POST['email']
			);
			$status =$this->login_model->addEmployee($data); 
			if($status==0){
				echo $status;
			//get ID of added employee then register in users using email and combination of name as password.
			$empID = $this->login_model->getEmployeeID($_POST['FirstName'],$_POST['MiddleName'],$_POST['LastName'],$_POST['ContactNumber']);
			$employeeID = $empID->row()->employee_ID;
			$pass1 = strtolower($_POST['FirstName'].$_POST['LastName']);
			$passgen = str_replace(' ', '', $pass1);
			$data2 = array(
				'username'=>$_POST['BioName'],
				'password'=>base64_encode($passgen),
				'employee_ID'=>$employeeID
			);
			
			
				$this->login_model->addUser($data2);
				$this->session->set_flashdata("success-add","Successfully Added an Employee!");
				redirect('Welcome/employee','refresh');
			}else{
				$this->session->set_flashdata("success-fail","Email already used.");
				redirect('Welcome/employee_add','refresh');
			}
			

			
			
		}else{
			$this->load->view('user/employee-add');
		}
		
	}
	public function resetPass($employeeID){
		$datafull =  $this->login_model->getUserinfo($employeeID);
		$pass1 = strtolower($datafull->row()->FirstName.$datafull->row()->LastName);
		$passgen = str_replace(' ', '', $pass1);
		$this->login_model->resetPass($employeeID,base64_encode($passgen));
		$this->session->set_flashdata("success-reset","Password reset complete!");
		redirect('Welcome/employee_info/'.$employeeID,'refresh');
	}

	public function disableUser($employeeID){
		$this->login_model->disableUser($employeeID);
		$this->session->set_flashdata("success-disable","User Disabled!");
		redirect('Welcome/employee_info/'.$employeeID,'refresh');
	}
	public function enableUser($employeeID){
		$this->login_model->enableUser($employeeID);
		$this->session->set_flashdata("success-enable","User Enabled!");
		redirect('Welcome/employee_info/'.$employeeID,'refresh');
	}
	public function updateEmp($employeeID){
		if(isset($_POST['saveEmp'])){
			$data= array(
				'FirstName'=>$_POST['FirstName'],
				'MiddleName'=>$_POST['MiddleName'],
				'LastName'=>$_POST['LastName'],
				'Gender'=>$_POST['Gender'],
				'Address'=>$_POST['Address'],
				'ContactNumber'=>$_POST['ContactNumber'],
				'Birthdate'=>$_POST['Birthdate'],
				'MaritalStatus'=>$_POST['MaritalStatus'],
				'DateHired'=>$_POST['DateHired'],
				'PreviousWork'=>$_POST['PreviousWork'],
				'Salary'=>$_POST['Salary'],
				'Position'=>$_POST['Position'],
				'Division'=>$_POST['Division'],
				'EmploymentStatus'=>$_POST['EmploymentStatus'],
				'OfficeAssign'=>$_POST['OfficeAssign'],
				'email'=>$_POST['email'],
				'sick_leave_credits'=>$_POST['sickleavecredits'],
				'vacation_leave_credits'=>$_POST['vacationleavecredits'],
			);
			$this->login_model->updateEmp($employeeID,$data);
			$this->session->set_flashdata("success-enable","Successfully updated info!");
			redirect('Welcome/employee_info/'.$employeeID,'refresh');
		}
	}
	public function updateOwn(){
		if(isset($_POST['saveOwn'])){
			$data= array(
				'FirstName'=>$_POST['FirstName'],
				'MiddleName'=>$_POST['MiddleName'],
				'LastName'=>$_POST['LastName'],
				'BioName'=>$_POST['BioName'],
				'Gender'=>$_POST['Gender'],
				'Address'=>$_POST['Address'],
				'ContactNumber'=>$_POST['ContactNumber'],
				'Birthdate'=>$_POST['Birthdate'],
				'MaritalStatus'=>$_POST['MaritalStatus'],
				'DateHired'=>$_POST['DateHired'],
				'PreviousWork'=>$_POST['PreviousWork'],
				'Salary'=>$_POST['Salary'],
				'Position'=>$_POST['Position'],
				'Division'=>$_POST['Division'],
				'EmploymentStatus'=>$_POST['EmploymentStatus'],
				'email'=>$_POST['email']
			);
			$this->login_model->updateOwn($data);
			$this->session->set_flashdata("success-enable","Successfully updated info!");
			redirect('Welcome/profile','refresh');
		}
	}
	public function leaveRequest(){
		// leave-request-add
		if(!isset($_SESSION['user_logged'])){
				redirect('Welcome/inside');
			}
		if(isset($_POST['sendReq'])){
			//SEND REQUEST
			$data = array(
				'leavetype'=>$_POST['LeaveType_ID'],
				'Employee_ID'=>$_SESSION['employee_id'],
				'other'=>$_POST['other'],
				'Reason'=>$_POST['Reason'],
				'Date_Applied'=>date("Y-m-d"),
				'Date_of_LeaveFrom'=>$_POST['Date_of_LeaveFrom'],
				'Date_of_LeaveTo'=>$_POST['Date_of_LeaveTo'],
				'LeaveSpent'=>$_POST['leavespent'],
				'NumberofWorkingDays'=>$_POST['NumberofWorkingDays'],
				'commutations'=>$_POST['commutations']
			);

			$this->login_model->addLeaveRequest($data);

		}
		$data['leaveCredits'] = $this->login_model->getLeaveCredits($_SESSION['employee_id']);
		$this->load->view('user/leave-request-add',$data);
	}
	public function leaveReqList(){
		if(isset($_SESSION['admin_logged'])){
			$data['requestList'] = $this->login_model->getRequestsPending();
			$data['OnLeave'] = $this->login_model->OnLeaveList();
			$data['OnLeaveHistoryList1'] = $this->login_model->OnLeaveHistoryList1();
			$data['OnLeaveHistoryList2'] = $this->login_model->OnLeaveHistoryList2();
			$this->load->view('user/leave-request-list',$data);
		}else{
			redirect('Welcome/inside');
		}
	}

	public function leaveDetails($requestID){
		if(!isset($_SESSION['admin_logged'])){
				redirect('Welcome/inside');
			}
		$data['recom_signatories'] = $this->login_model->recom_signatories();
		$data['app_signatories']= $this->login_model->app_signatories();
		$data['requestDetails'] = $this->login_model->getRequestsPendingById($requestID);
		$this->load->view('user/leave-full',$data);

	}
	public function approveReq(){
		$Request_ID = $_POST['request_id'];
		$recom = $_POST['recommendation'];
		$workdays = $_POST['workdays'];
		$leavetype = $_POST['leavetype'];
		$empid = $_POST['empid'];
		$app = $_POST['approval'];
		// SET NEW SICK OR VACATION CREDS IN EMPLOYEE TABLE
		$row = $this->login_model->setNewLeaveCredits($workdays,$leavetype,$empid);
		if($leavetype!="Maternity"){
			$sickLeave = $row[0]->sick_leave_credits;
			$vacationLeave = $row[0]->vacation_leave_credits;
			$this->login_model->approveReq($Request_ID,$recom,$app,$workdays,$sickLeave,$vacationLeave);
		}else{
			$this->login_model->approveReq($Request_ID,$recom,$app,$workdays,0,0);
		}
		redirect('Welcome/leaveReqList/','refresh');
	}
	public function declineReq($Request_ID){
		$this->login_model->declineReq($Request_ID);
		redirect('Welcome/leaveReqList/','refresh');
		
	}
	public function profile(){
		if(!isset($_SESSION['user_logged'])){
				redirect('Welcome/inside');
			}
		$data['employee_info'] = $this->login_model->getAllinfo($_SESSION['employee_id']);
		$this->load->view('user/profile',$data);
	}
	public function changepass(){
		if(!isset($_SESSION['user_logged'])){
				redirect('Welcome/inside');
			}
		$datastore = $this->login_model->getAllinfo($_SESSION['employee_id']);
		if(isset($_POST['changeNow'])){
			if($datastore->row()->password==base64_encode($_POST['currentPass'])){
				$this->session->set_flashdata("success","Successfully changed password.");
				
				$this->login_model->changePass(base64_encode($_POST['newpass']));
				redirect('Welcome/changepass','refresh');

			}else{
				$this->session->set_flashdata("fail","Wrong current password.");
				redirect('Welcome/changepass','refresh');
			}
		}
		$this->load->view('user/changepass');
	}
	public function myleave(){
		if(!isset($_SESSION['user_logged'])){
				redirect('Welcome/inside');
			}
		$data['approveList'] = $this->login_model->approveList();
		$data['pendingList'] = $this->login_model->pendingList();
		$data['rejectedList'] = $this->login_model->rejectedList();
		$this->load->view('user/myleave',$data);
	}
	public function editLeaveRequest($Request_ID){
		if(!isset($_SESSION['user_logged'])){
				redirect('Welcome/inside');
			}
		$data['pendingList'] = $this->login_model->getRequestsPendingById($Request_ID);
		if(isset($_POST['saveReq'])){
			$data = array(
				'leavetype'=>$_POST['LeaveType_ID'],
				'Employee_ID'=>$_SESSION['employee_id'],
				'other'=>$_POST['other'],
				'Reason'=>$_POST['Reason'],
				'Date_Applied'=>date("Y-m-d"),
				'Date_of_LeaveFrom'=>$_POST['Date_of_LeaveFrom'],
				'Date_of_LeaveTo'=>$_POST['Date_of_LeaveTo'],
				'LeaveSpent'=>$_POST['leavespent'],
				'NumberofWorkingDays'=>$_POST['NumberofWorkingDays'],
				'commutations'=>$_POST['commutations']
			);
			$this->login_model->editLeaveRequest($Request_ID,$data);
			redirect('Welcome/editLeaveRequest/'.$Request_ID,'refresh');
		}
		$this->load->view('user/editleaverequest',$data);
	}
	public function uploadUser(){
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'csv';
		
    	
    	$this->load->library('upload', $config);
	    if ( ! $this->upload->do_upload('userFile'))
        {

                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata("fail-add","An error occurred during import.");
                redirect("Welcome/employee",'refresh');
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
                list($result,$i) =   $this->csvreader->parse_file($data['upload_data']['full_path']);
                $this->login_model->batchRegister($result,$i);
                $this->session->set_flashdata("success-add","Successfully imported new employee list.");
                redirect("Welcome/employee",'refresh');
        }
	}
	public function uploadAttendance(){
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'csv';
		
    	
    	$this->load->library('upload', $config);
	    if ( ! $this->upload->do_upload('userFile'))
        {

                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata("fail-add","Failed to import attendance.");
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
                list($result,$i) =   $this->csvreader->parse_file($data['upload_data']['full_path']);
                $dateArray=array();
                
                foreach ($result as $key) {
                	$tempAttend = (explode("/",$key['Date']));
            		$key_date = $tempAttend[2]."-".$tempAttend[0]."-".$tempAttend[1];
                	if( !in_array($key_date,$dateArray)) array_push($dateArray,$key_date);
                }
                // print_r($dateArray);

                usort($dateArray, function($a, $b) {
				    $dateTimestamp1 = strtotime($a);
				    $dateTimestamp2 = strtotime($b);

				    return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
				});
				// var_dump($dateArray[0],$dateArray[count($dateArray) - 1]);
                $this->login_model->batchAttendance($result,$i);
				
				$this->login_model->leaveCreditsAlgo($dateArray[0],$dateArray[count($dateArray) - 1],$dateArray);


                $this->session->set_flashdata("success-add","Successfully imported attendance.");
                
        }
        $files = glob('./uploads/*'); // get all file names
		foreach($files as $file){ // iterate files
		  if(is_file($file))
		    unlink($file); // delete file
		}
        redirect("Welcome/inside",'refresh');
	}
	public function popPrint($reqID){

		$data['reqDATA'] = $this->login_model->getLeaveRequest($reqID);
		$this->load->view('user/g_report',$data);
	}

	public function undertime(){
		if(!isset($_SESSION['admin_logged'])){
			redirect('Welcome/inside');
		}
		if(isset($_POST['selectdate'])){
			$temp1 = (explode(" ",$_POST['selectdate']));
			
			$data['undertime'] = $this->login_model->getUndertime($temp1[0],$temp1[1]);
			$this->load->view('user/undertime',$data);
		}else{
			$data['undertime'] = false;
			$this->load->view('user/undertime',$data);
		}
		
	}
	public function workingdays(){
		if(!isset($_SESSION['admin_logged'])){
			redirect('Welcome/inside');
		}
		if(isset($_POST['workingDays'])){
			$temp1 = (explode(" ",$_POST['workingDays']));
			$employee_ID = $_POST['employeeID'];
			$data['AllEmployee'] = $this->login_model->getAllEmployee();
			$data['workingHours'] = $this->login_model->getWorkingHours($employee_ID,$temp1[0],$temp1[1]);
			$this->load->view('user/workingdays',$data);
		}else{
			$data['AllEmployee'] = $this->login_model->getAllEmployee();
			$data['workingHours'] = false;
			$this->load->view('user/workingdays',$data);
		}
	}
	public function attendance_summary(){
		if(!isset($_SESSION['admin_logged'])){
			redirect('Welcome/inside');
		}
		if(isset($_POST['selectdate'])){
			$temp1 = (explode(" ",$_POST['selectdate']));
			$employee_ID = $_POST['employeeID'];
			$data['AllEmployee'] = $this->login_model->getAllEmployee();
			$datediff = strtotime($temp1[1])-strtotime($temp1[0]);
			$data['finalDays']=round($datediff / (60 * 60 * 24));
			$data['totalHours']=(int)$data['finalDays']*8;
			$data['workingHours'] = $this->login_model->getAttendanceSummary($employee_ID,$temp1[0],$temp1[1]);
			
			$this->load->view('user/attendance_summary',$data);
		}else{
			$data['AllEmployee'] = $this->login_model->getAllEmployee();
			$data['workingHours'] = false;
			$this->load->view('user/attendance_summary',$data);
		}
	}
	public function lateness(){
		if(!isset($_SESSION['admin_logged'])){
			redirect('Welcome/inside');
		}
		if(isset($_POST['selectdate'])){
			$temp1 = (explode(" ",$_POST['selectdate']));
			$latetime = $_POST['latetime'];
			$data['lates'] = $this->login_model->getLate($latetime,$temp1[0],$temp1[1]);
			$this->load->view('user/lateness',$data);
		}else{
			$data['lates'] = false;
			$this->load->view('user/lateness',$data);
		}
	}
	
	public function absentsummary(){
		if(!isset($_SESSION['admin_logged'])){
			redirect('Welcome/inside');
		}
		if(isset($_POST['workingDays'])){
			$temp1 = (explode(" ",$_POST['workingDays']));
			$data['AllEmployee'] = $this->login_model->getAllEmployee();
			// $data['workingHours'] = $this->login_model->getAttendanceSummary($employee_ID,$temp1[0],$temp1[1]);
			$data['workingAttendance'] = $this->login_model->getAttendanceRecord($_POST['employeeID'],$temp1[0],$temp1[1]);
			$data['date_array'] = array();
			if(count($data['workingAttendance'])>1){
				foreach ($data['workingAttendance'] as $x) {
					array_push($data['date_array'], $x->attendance_date);
				}
			}else{
				$data['workingAttendance'] = true;
			}
			
			$data['startingDate'] = $temp1[0];
			$data['endingDate'] = $temp1[1];
			$this->load->view('user/absentsummary',$data);
		}else{

			$data['AllEmployee'] = $this->login_model->getAllEmployee();
			$data['workingAttendance'] = false;
			$this->load->view('user/absentsummary',$data);
		}
		// $this->load->view('user/absentsummary');
	}
	public function loadDTRUser($date1,$date2,$empID){
		
		$data['reqDATA'] = $this->login_model->getDTRUser($empID,$date1,$date2);
		// print_r($data['reqDATA']);
		$this->load->view('user/dtr_report',$data);
	}
	public function loadDTRDepartment($date1,$date2,$dep){
		$datas  = $this->login_model->getDTRDepartment($dep,$date1,$date2);
		$array = json_decode(json_encode($datas), True);
		// print_r($array);

		function arrayChunkByKeyValue($data, $groupByKey)
		{
		    $groupArray = [];
		    foreach ($data as $singleData) {
		        $groupArray[$singleData[$groupByKey]][] =(object) $singleData;
		    }
		    return $groupArray;
		}
		$data['chunked'] = arrayChunkByKeyValue($array, 'employee_ID');
		// $data['chunked']= (object) $data['temp'];
		// print_r($data['temp']);
		$this->load->view('user/dtr_reportdep',$data);
	}
	public function genDTR(){
			if(!isset($_SESSION['user_logged'])){
				redirect('Welcome/inside');
			}
			if(isset($_GET['empID'])){
				$temp1 = (explode(" ",$_GET['workingDays']));
				$employee_ID = $_GET['empID'];
				$department=$_GET['dep'];
				

				if($department!=""){
					redirect('Welcome/loadDTRDepartment/'.$temp1[0]."/".$temp1[1]."/".$department,'refresh');
				}else{
					redirect('Welcome/loadDTRUser/'.$temp1[0]."/".$temp1[1]."/".$employee_ID,'refresh');
				}

			}else{
				$data['AllEmployee'] = $this->login_model->getAllEmployee();
				$data['division'] =$this->login_model->getDivisions();
				$this->load->view('user/gendtr',$data);
			}
			
		
	}
	
}
