<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('max_execution_time', 0);
class Login_model extends CI_Model{   
   
   public function getLoginAccount(){
        $username = $_POST['username'];
        $password = base64_encode($_POST['password']);
        $this->db->select("*");
        $this->db->from("complete_info");
        $this->db->where(array('username'=>$username,'password'=>$password));
        $query = $this->db->get();
        return $query;        
    }  
    public function getAllUsers(){
        $this->db->select("*");
        $this->db->from("user");

        $query=$this->db->get();
        return $query->result();
    }
    public function getLeaveCredits($empID){
        $this->db->select("*");
        $this->db->from("complete_info");
        $this->db->where("employee_ID",$empID);
        $query=$this->db->get();
        return $query;

    }
    public function getAllEmployee(){
        $this->db->select("*");
        $this->db->from("employee");
        $this->db->order_by("BioName", "ASC");
        $query=$this->db->get();
        return $query->result();
    }
    public function getDivisions(){
        $this->db->select("*");
        $this->db->from("division");

        $query=$this->db->get();
        return $query->result();
    }

    public function getUserinfo($employeeID){

        $this->db->from("employee");
        $this->db->where("employee_ID",$employeeID);
        $query=$this->db->get();
        return $query;
    }
    public function getAllinfo($employeeID){

        $this->db->from("complete_info");
        $this->db->where("employee_ID",$employeeID);
        $query=$this->db->get();
        return $query;
    }
    public function getEmployeeFull($employeeID){
        $this->db->from("employee");
        $this->db->where("employee_ID",$employeeID);
        $query=$this->db->get();
        return $query->result();
    }
    public function employeeCount(){
        $this->db->select("COUNT(Address) as empCount");
        $this->db->from("employee");
        $query=$this->db->get();
        return $query;
    }

    public function LeaveCount(){
        $this->db->select("COUNT(reason) as leaveCount");
        $this->db->where("Status","pending");
        $this->db->from("employee_leaverequest");
        $query=$this->db->get();
        return $query;
    }


    public function OnLeaveCount(){
        $this->db->select("COUNT(reason) as OnLeaveCount");
        $this->db->where("Status","Approved");
        $dataToday = date("Y-m-d");
        $this->db->where('Date_of_LeaveFrom <=',$dataToday);
        $this->db->where('Date_of_LeaveTo >=', $dataToday);
        $this->db->from("employee_leaverequest");
        $query=$this->db->get();
        return $query;
    }

    public function getAttendanceList(){
        $this->db->select("*");
        $this->db->from("attendance_complete");
        $this->db->order_by("attendance_date", "desc");
        $this->db->order_by("time_out", "desc");
        $query=$this->db->get();
        return $query->result();
    }
    public function getAttendanceListOne($emp){
        $this->db->select("*");
        $this->db->from("attendance_complete");
        $this->db->order_by("attendance_date", "desc");
        $this->db->order_by("time_out", "desc");
        $this->db->where("Employee_ID",$emp);
        $query=$this->db->get();
        return $query->result();
    }

    public function addEmployee($data){
        $res = $this->db->insert('employee',$data); 
        if(!$res){
            return $this->db->error(); 
        }else{
            return 0;
        }
        
         

        
    }
    public function addUser($data){
        try{
            $this->db->insert('user',$data);
        }catch(UserException $error){
            return $error;
        }
        return null;
    }
    public function getEmployeeID($fname,$mname,$lname,$number){
        $this->db->select("*");
        $this->db->where("FirstName",$fname);
        $this->db->where("MiddleName",$mname);
        $this->db->where("LastName",$lname);
        $this->db->where("ContactNumber",$number);
        $this->db->from("employee");
        $query=$this->db->get();
        return $query;

    }
    public function resetPass($empID,$newpass){
        $data = array("password"=>$newpass);
        $this->db->where("employee_ID",$empID);
        $this->db->update("user",$data);
        return $empID;

    }
    public function disableUser($empID){
        $data = array("status"=>"disabled");
        $this->db->where("employee_ID",$empID);
        $this->db->update("user",$data);
        return $empID;
    }
    public function enableUser($empID){
        $data = array("status"=>"active");
        $this->db->where("employee_ID",$empID);
        $this->db->update("user",$data);
        return $empID;
    }
    public function updateEmp($empID,$data){
        $this->db->where("employee_ID",$empID);
        $this->db->update("employee",$data);
        return $empID;
    }
    public function updateOwn($data){
        $this->db->where("employee_ID",$_SESSION['employee_id']);
        $this->db->update("employee",$data);
        return true;
    }
    public function addLeaveRequest($data){
        $this->db->insert('employee_leaverequest',$data);
    }
    public function editLeaveRequest($reqID,$data){
        $this->db->where("Request_ID",$reqID);
        $this->db->update("employee_leaverequest",$data);
        return true;
    }
    public function getRequestsPending(){
        $this->db->select("*");
        $this->db->from("leavereqs");
        $this->db->where("Status","pending");
        $query=$this->db->get();
        return $query->result();
    }
    public function getRequestsPendingById($requestID){
        $this->db->select("*");
        $this->db->from("leavereqs");
        $this->db->where("Request_ID",$requestID);
        $query=$this->db->get();
        return $query;
    }
    public function OnLeaveList(){
        $this->db->select("*");
        $this->db->where("Status","Approved");
        $dataToday = date("Y-m-d");
        $this->db->where('Date_of_LeaveFrom <=',$dataToday);
        $this->db->where('Date_of_LeaveTo >=', $dataToday);
        $this->db->from("leavereqs");
        $query=$this->db->get();
        return $query->result();
    }
    public function OnLeaveHistoryList1(){
        $this->db->select("*");
        $this->db->where("Status","Approved");
        $dataToday = date("Y-m-d");
        $this->db->where('Date_of_LeaveTo <', $dataToday);
        $this->db->from("leavereqs");
        $query=$this->db->get();
        return $query->result();
    }
    public function OnLeaveHistoryList2(){
        $this->db->select("*");
        $this->db->where("Status","Declined");
        $this->db->from("leavereqs");
        $query=$this->db->get();
        return $query->result();
    }
    public function setNewLeaveCredits($workdays,$leavetype,$empid){
        if($leavetype =="Sick"){
            $sql = "UPDATE employee SET sick_leave_credits = sick_leave_credits- $workdays WHERE employee_ID = $empid";
            $query=$this->db->query($sql);
        }
        if($leavetype=="Vacation"){
            $sql = "UPDATE employee SET vacation_leave_credits = vacation_leave_credits- $workdays, WHERE employee_ID = $empid";
            $query=$this->db->query($sql);

        }


        // RETURN NEW VALUES
        $this->db->select("*");
        $this->db->where("employee_ID",$empid);
        $this->db->from("employee");
        $query2 = $this->db->get();
        return $query2->result();
        
    }
    public function approveReq($Request_ID,$recom,$app,$workdays,$sickLeave,$vacationLeave){
        date_default_timezone_set('Asia/Manila');
        $dateNow = date("Y-m-d H:i:s");
        $data = array
        (
            "Status"=>"Approved",
            "recommend_by"=>$recom,
            "approved_by"=>$app,
            "vacation_leave_credits"=>$vacationLeave,
            "sick_leave_credits"=>$sickLeave,
            "date_updated"=>$dateNow
        );
        $this->db->where("Request_ID",$Request_ID);
        $this->db->update("employee_leaverequest",$data);
        return $Request_ID;
    }
    public function declineReq($Request_ID){
        $data = array("Status"=>"Declined");
        $this->db->where("Request_ID",$Request_ID);
        $this->db->update("employee_leaverequest",$data);
        return $Request_ID;
    }
    public function changePass($newpass){
        $data = array("password"=>$newpass);
        $this->db->where("employee_ID",$_SESSION['employee_id']);
        $this->db->update("user",$data);
        return $newpass;
    }
    public function approveList(){
        $this->db->select("*");
        $this->db->where("Status","Approved");
        $this->db->where("employee_ID",$_SESSION['employee_id']);
        $this->db->from("leavereqs");
        $query=$this->db->get();
        return $query->result();
    }
    public function pendingList(){
        $this->db->select("*");
        $this->db->where("Status","pending");
        $this->db->where("employee_ID",$_SESSION['employee_id']);
        $this->db->from("leavereqs");
        $query=$this->db->get();
        return $query->result();
    }
    public function rejectedList(){
        $this->db->select("*");
        $this->db->where("Status","Declined");
        $this->db->where("employee_ID",$_SESSION['employee_id']);
        $this->db->from("leavereqs");
        $query=$this->db->get();
        return $query->result();
    }
    public function recom_signatories(){
        $this->db->select("*");
        $this->db->from("signatory_recommendation");
        $query=$this->db->get();
        return $query->result();
    }
    public function app_signatories(){
        $this->db->select("*");
        $this->db->from("approval_signatory");
        $query=$this->db->get();
        return $query->result();
    }
    public function batchRegister($data,$count){
        $this->db->trans_begin();
        for($i=0;$i<$count;$i++){
            // $this->db->trans_begin();
            // $this->db->trans_start();
            $entries = array(
                'employee_ID' => $data[$i]['BioID'],
                'BioName' => $data[$i]['BioName'],
                'FirstName' => $data[$i]['Firstname'],
                'LastName' => $data[$i]['Lastname'],
                'MiddleName' => $data[$i]['MI'],
                
                );
                $this->db->insert('employee',$entries);
                // $this->db->trans_complete();
            // $this->db->trans_commit();
            
            
        }
        
        for($i=0;$i<$count;$i++){
            // $this->db->trans_start();
            $entries2 = array(
                'employee_ID' => $data[$i]['BioID'],
                'username' => $data[$i]['BioName'],
                'password' => base64_encode(strtolower(str_replace(' ','',$data[$i]['Firstname'].$data[$i]['Lastname']))),
                );
            $this->db->insert('user',$entries2);
            // $this->db->trans_complete();
        }
        $this->db->trans_commit();

        
    }
    public function batchAttendance($data,$count){
        $tempArray = array();
        $idholder = array();
        for($x=0;$x<$count;$x++){
            $emp_id_temp = $data[$x]['No'];
            if(!in_array($emp_id_temp, $idholder)){
                $idholder[] = $emp_id_temp;
                $tempArray[]=$data[$x];
                
            }
        }    
        // CHECK IF EMP_ID EXISTS
        $this->db->trans_begin();
        foreach ($tempArray as $val) {
            
            $this->db->where('employee_ID',$val['No']);
            $query = $this->db->get('employee');
            if ($query->num_rows() <= 0){
                // CREATE USER
                $entries = array(
                'employee_ID' => $val['No'],
                'BioName' => $val['Name'],
                'FirstName' => $val['Name']
                );
                $this->db->insert('employee',$entries);


                $entries2 = array(
                'employee_ID' => $val['No'],
                'username' => $val['Name'],
                'password' => base64_encode(strtolower($val['Name']))
                );
                $this->db->insert('user',$entries2);
            }
        }
        $this->db->trans_commit();
        
        $this->db->trans_start();

        for($i=0;$i<$count;$i++){
            $emp_id = $data[$i]['No'];
            $tempAttend = (explode("/",$data[$i]['Date']));
            $attendance_date = $tempAttend[2]."-".$tempAttend[0]."-".$tempAttend[1];
            $time = $data[$i]['Time'];
            $name = $data[$i]['Name'];
            $noon ="12:00:00";



            if(strtotime($time)<strtotime($noon)){
                //IF MORNING - INSERT AS TIME IN
                $query = "INSERT INTO attendance (Employee_ID,Name,attendance_date,time_in)
                VALUES('$emp_id','$name','$attendance_date','$time')
                ON DUPLICATE KEY UPDATE 
                time_in = LEAST(time_in, VALUES(time_in))
                ";
            }else{
                //IF AFTERNOON - INSERT AS TIME OUT
                $query = "INSERT INTO attendance (Employee_ID,Name,attendance_date,time_out)
                VALUES('$emp_id','$name','$attendance_date','$time')
                ON DUPLICATE KEY UPDATE 
                time_out = GREATEST(time_out, VALUES(time_out))
                ";
            }
            $this->db->query($query);
            
        }
        $this->db->trans_complete();

        
    }
    public function getUndertime($data1,$data2){
        $this->db->select("*");
        $this->db->where('attendance_date >=', $data1);
        $this->db->where('attendance_date <=', $data2);
        $this->db->where("totalHour < 8");
        $this->db->from("undertime");
        $query=$this->db->get();
        return $query->result();
    }
    public function getLate($latetime,$data1,$data2){
        $this->db->select("*");
        $this->db->where('attendance_date >=', $data1);
        $this->db->where('attendance_date <=', $data2);
        $this->db->where("time_in > '$latetime'");
        $this->db->from("attendance");
        $query=$this->db->get();
        return $query->result();
    }
    public function getWorkingHours($employee_ID,$data1,$data2){
        $this->db->select("*");
        $this->db->where('employee_ID', $employee_ID);
        $this->db->where('attendance_date >=', $data1);
        $this->db->where('attendance_date <=', $data2);
        $this->db->from("undertime");
        $query=$this->db->get();
        return $query->result();
    }
    public function getDTRUser($employee_ID,$data1,$data2){
        $this->db->select("*");
        $this->db->where('Employee_ID', $employee_ID);
        $this->db->where('attendance_date >=', $data1);
        $this->db->where('attendance_date <=', $data2);
        $this->db->from("undertime");
        $query=$this->db->get();
        return $query->result();
    }
    public function getDTRDepartment($dep,$data1,$data2){
        $this->db->select("*");
        $this->db->where('Division', $dep);
        $this->db->where('attendance_date >=', $data1);
        $this->db->where('attendance_date <=', $data2);
        $this->db->from("undertime");
        $query=$this->db->get();
        return $query->result();
    }
    public function getAttendanceRecord($employee_ID,$data1,$data2){
        $this->db->select("*");
        $this->db->where('employee_ID', $employee_ID);
        $this->db->where('attendance_date >=', $data1);
        $this->db->where('attendance_date <=', $data2);
        $this->db->from("attendance_complete");
        $query=$this->db->get();
        return $query->result();
    }
    public function getAttendanceSummary($employee_ID,$data1,$data2){
        
        $sql = "SELECT employee_ID,sum(totalHour) as hourTotal,count(FirstName) as totalPresent,FirstName,LastName FROM undertime WHERE attendance_date between '$data1' and '$data2'
        and DayOfWeek(attendance_date) % 7 > 1 and employee_ID= '$employee_ID' GROUP by employee_ID ORDER by attendance_date DESC";
        $query=$this->db->query($sql);
        $row = $query->row_array();
        return $row;
    }


    public function getLeaveRequest($reqID){
        $this->db->select("*");
        $this->db->where("Request_ID",$reqID);
        $this->db->from("leavereqs");
        $query = $this->db->get();
        return $query;  

    }
    // LEAVE CREDITS ALGO
    public function leaveCreditsAlgo($f_date,$l_date,$all_dates){
        echo "string";
        $this->db->select("*");
        $this->db->where('attendance_date >=', $f_date);
        $this->db->where('attendance_date <=', $l_date);
        $this->db->from("undertime");
        $query = $this->db->get();
        $query = $query->result();
        $emp_id_array = array();
        foreach ($query as $key) {
            if( !in_array($key->employee_ID,$emp_id_array)) array_push($emp_id_array,$key->employee_ID);
        }
        $this->loop_records($emp_id_array,$all_dates);
    }

    public function loop_records($emp_id_array,$all_dates){
        foreach ($emp_id_array as $key) {
            // var_dump($key);
            $negative_leave_credits = 0;

            foreach ($all_dates as $date) {
                $this->db->select("*");
                $this->db->where("employee_ID",$key);
                $this->db->where('attendance_date', $date);
                $this->db->from("undertime");
                $query= $this->db->get();
                $query = $query->result();
                if(count($query)==0){
                    $negative_leave_credits +=1;

                }else{
                    $time_in = strtotime($query[0]->time_in);
                    if($time_in > strtotime("08:00:00")){
                        $first  = new DateTime( $query[0]->time_in );
                        $second = new DateTime( '08:00:00' );

                        $minutes = $first->diff($second)->format('%h.%i');

                        $result_negative_leave_credits = $minutes * 0.002;
                        $negative_leave_credits += $result_negative_leave_credits;
                    }
                    $totalHour = (float)$query[0]->totalHour;
                    if($totalHour >= 5){
                        $totalHour =  $totalHour-1;
                    }
                    if($totalHour < 8){
                        $minutes= (8 - $totalHour) * 60;
                        $result_negative_leave_credits = $minutes * 0.002;
                        $negative_leave_credits += $result_negative_leave_credits;
                    }
                }
                
            } 
            
            $final_result = ($negative_leave_credits*-1) + 1.25;
            $this->syncLeaveCredits($key,$final_result);
        }

    }
    public function syncLeaveCredits($emp_id,$creds){
        $sql = "UPDATE employee SET sick_leave_credits = sick_leave_credits+ $creds,vacation_leave_credits = vacation_leave_credits + $creds WHERE employee_ID = $emp_id";
        $query=$this->db->query($sql);
    }

}