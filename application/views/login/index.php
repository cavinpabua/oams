<?php 
   include "heads.php";

   
   
   ?>
<body>
   <!-- navbar-->
   <header class="header">
      <nav class="navbar navbar-expand-lg fixed-top">
         <h1>OAMS</h1>
         <button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right"><span></span><span></span><span></span></button>
         <div id="navbarSupportedContent" class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto align-items-start align-items-lg-center">
               <li class="nav-item"><a href="#about-us" class="nav-link link-scroll">About Us</a></li>
               <li class="nav-item"><a href="#efeatures" class="nav-link link-scroll">Features</a></li>
             
            </ul>
            <div class="navbar-text">
               <!-- Button trigger modal--><a href="#" data-toggle="modal" data-target="#exampleModal" class="btn btn-primary navbar-btn btn-shadow btn-gradient">Sign In</a>
            </div>
         </div>
      </nav>
   </header>
   <!-- Modal-->
   <div id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade">
      <div role="document" class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <h5 id="exampleModalLabel" class="modal-title">Sign In Employee</h5>
               <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
               <form id="signupform" action="<?php echo site_url('Welcome/index'); ?>" method="post">
                  <div class="form-group">
                     <label for="username">Username</label>
                     <input type="text" name="username" placeholder="Enter your username"  >
                  </div>
                  <div class="form-group">
                     <label for="Password">Password</label>
                     <input type="Password" name="password" placeholder="Enter your password" id="password" >
                  </div>
                  <div class="form-group">
                     <button type="submit" class="submit btn btn-primary btn-shadow btn-gradient" name='login'>Sign In</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <section id="hero" class="hero hero-home bg-gray">
      <div class="container">
         <div class="row d-flex">
            <div class="col-lg-6 text order-2 order-lg-1">
               <h1>Online Attendance Monitoring System</h1>
               <p class="hero-text">attendance monitoring is a key function in every HR department.</p>
               <div class="CTA"><a href="#features" class="btn btn-primary btn-shadow btn-gradient link-scroll">Discover More</a></div>
            </div>
            <div class="col-lg-6 order-1 order-lg-2"><img src="<?php echo base_url(); ?>/assets/img/time.jpg" alt="..." class="img-fluid"></div>
         </div>
      </div>
   </section>
   <section id="about-us" class="about-us bg-gray">
      <div class="container">
         <h2>About Us</h2>
         <div class="row">
            <p class="lead col-lg-10">Help the HR personnel to monitor and manage the records of each employee in a fastest, easiest and most convenient way. On the other hand, the system will automatically retrieve the attendance report from the biometric to the system and automatically calculate the total monthly working hours of every employees. The system will also allow the employee to file a leave request and will allow the HR personnel to view the leave credits and leave balance of each employee. </p>
         </div>
         <a href="#" class="btn btn-primary btn-shadow btn-gradient">Discover More</a>
      </div>
   </section>
   <section id="efeatures" class="extra-features bg-primary">
      <div class="container text-center">
         <header>
            <h2>More great features             </h2>
            <div class="row">
            </div>
         </header>
         <div class="grid row">
            <div class="item col-lg-4 col-md-6">
               <div class="icon"> <i class="icon-management"></i></div>
               <h3 class="h5">Employee Profiling</h3>
               <p>Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.</p>
            </div>
            <div class="item col-lg-4 col-md-6">
               <div class="icon"> <i class="icon-folder-1"></i></div>
               <h3 class="h5">Manage Leave</h3>
               <p>Manage the routing of employee leave request from employee to HR up to the Agency Head.</p>
            </div>
            <div class="item col-lg-4 col-md-6">
               <div class="icon"> <i class="icon-gears"></i></div>
               <h3 class="h5">Generate Reports</h3>
               <p></p>
            </div>
         </div>
   </section>
   <div id="scrollTop">
   </div>
   </div>
   <div class="copyrights">
      <div class="container">
         
         <center><p>&copy; The Researchers</p></center>
      </div>
   </div>
   </footer>
   <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"> </script>
   <script src="<?php echo base_url(); ?>/assets/vendors/bootstrap/js/bootstrap.min.js"></script>
   <script src="<?php echo base_url(); ?>/assets/vendors/jquery.cookie/jquery.cookie.js"> </script>
   <script src="<?php echo base_url(); ?>/assets/vendors/owl.carousel/owl.carousel.min.js"></script>
 
</body>
</html>