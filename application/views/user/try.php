<?php 
include 'session.php';
include ("dbconnect.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <style>
            .dot1 {
            height: 10px;
            width: 10px;
            background-color: green;
            border-radius: 50%;
            display: inline-block;
            }
            .dot2 {
            height: 10px;
            width: 10px;
            background-color: red;
            border-radius: 50%;
            display: inline-block;
            }
            .dot3 {
            height: 10px;
            width: 10px;
            background-color: pink;
            border-radius: 50%;
            display: inline-block;
            }
            #scrolltable { margin-top: 20px; height: 200px; overflow: auto; }
            #scrolltable th div { position: absolute; margin-top: -20px; }
        </style>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>HRISDA | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="../https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../dist/css/adminlte.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="../plugins/morris/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="../https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand border-bottom navbar-dark bg-success">
                <!-- Left navbar links -->
                <?php include("topnav.php") ?>
            </nav>
            <!-- /.navbar -->
            <!-- Main Sidebar Container -->
            <aside class="main-sidebar elevation-4 sidebar-light-success">
                <?php include("sidenav.php") ?>
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                            </div>
                            <div class="col-sm-6">
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-success">
                                <div class="card-header">
                                    <h3 class="card-title">Attendance Report</h3>
                                </div>
                                <div class="form-group" style='border: black;'>
                                    <br>
                                    &nbsp;&nbsp; &nbsp;&nbsp;
                                    <form method="POST">
                                      <select  name="month" style="width: 250px;    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;"><option selected disabled >Select Month</option>
    <option value='January'>January</option>
    <option value='February'>February</option>
    <option value='March'>March</option>
    <option value='April'>April</option>
    <option value='May'>May</option>
    <option value='June'>June</option>
    <option value='July'>July</option>
    <option value='August'>August</option>
    <option value='September'>September</option>
    <option value='October'>October</option>
    <option value='November'>November</option>
    <option value='December'>December</option>
  </select>  
                                        <select  name="Division" style="width: 250px;    padding: 12px 20px;
                                            margin: 8px 0;
                                            display: inline-block;
                                            border: 1px solid #ccc;
                                            border-radius: 4px;
                                            box-sizing: border-box;">
                                            <option selected="selected" disabled>Department</option>
                                            <?php
                                                include ("dbconnect.php");
                                                $department = $row2['department'];
                                                $sqli_query="SELECT CONCAT(e.`FirstName` , e.`Lastname`) AS Name,`Address`,`employee_ID`,`DateHired`,`PreviousWork`,`Salary`,`Position`,`Division`,`EmployementStatus` FROM `employee` as e group by Division   ";
                                                $result=$conn->query($sqli_query);
                                                if ($result->num_rows>0){
                                                while ($row=$result->fetch_assoc()){?>
                                            <option selected="selected" value="<?php echo htmlspecialchars($row['Division']);?>"><?php echo htmlspecialchars($row['Division']);?></option>
                                            <?php
                                                }}
                                                ?>
                                        </select>
                                        <button class="btn btn-success"  type="submit" name="display"> <i class="nav-icon fa fa-search"> </button></i> 
                                   <a href='undertime.php' class="btn btn-success"  ><i class="fa fa-reply"></a></i>&nbsp;&nbsp;<a href='yearly.php' class="btn btn-success" >Yearly</a></form>
                                </div>
                                <div class="card-body">
                                    <table id="example1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                            <tr>
                                                <th>Name</th>
                                                <th>Position</th>
                                                <th>Division</th>
                                                <th>Assign Office</th>
                                                  <th>Month</th>
                                                <th>Total Present</th>
                                                 <th>Total Absent</th>
                                                 
                                                <th>Working hour</th>
                                                <th>Status</th>
                                            </tr>
                                            </tr>
                                        </thead>
                                       
                                        </thead>
                                        <tbody>
                                            
                                                <?php

                                                    $sqli_query="SELECT * FROM employee  ";
                                                    $result=$conn->query($sqli_query);
                                                    while ($row=$result->fetch_assoc()){
                                                        $employee_ID = $row['employee_ID'];
                                                       
                                                ?>
                                                    <tr>
                                                        <td><?= $row['FirstName'] ?></td> 
                                                        <td><?= $row['Position'] ?></td>
                                                        <td><?= $row['Division'] ?></td>
                                                        <td><?= $row['OfficeAssign'] ?></td>
                                                       
                                                            <?php

                                                                $sqli_query2="SELECT COUNT(Time_IN_AM) as total,Time_IN_AM,Time_Out_AM,TimeIN_PM,TimeOUT_PM,month(`Date`) as Month,`Date` FROM attendance WHERE employee_ID='$employee_ID' ";
                                                                $result2=$conn->query($sqli_query2);
                                                                $count = 0;
                                                                while ($row2=$result2->fetch_assoc()){
                                                                    $date1 = new DateTime($row2['Time_IN_AM']);
                                                                    $date2 = $date1->diff(new DateTime($row2['TimeOUT_PM']));
                                                                    $date = date("F",strtotime($row2['Date']));
                                                                    $absent = 31 - $row2['total'] ;
                                                                    

                                                                 
                                                                    $count++;
                                                            ?>
                                                                <td><?php  echo $date; ?></td>
                                                               
                                                                <td><?=  $row2['total']  ?></td>
                                                                     <td><?php  echo $absent  ?></td>
                                                                <td><center><?php 
                                                                    echo $date2->h.' hours'."\n";
                                                                  ?>
                                                                </center>
                                                                </td>
                                                           
                                                                <td>Present</td>
                                                            <?php }?>
                                                            <?php  if ($count==0) {?>
                                                                <td><center>-</center></td>
                                                                <td><center>-</center></td>
                                                                <td><center>-</center></td>
                                                                <td><center>-</center></td>
                                                                <td><center>-</center></td> 
                                                                <td><center></td>
                                                                <td></td>
                                                             <?php }?>
                                                        
                                                    </tr>
                                                <?php }?>
                                                <?php   if (isset($_POST['display'])){
                                         $Division = $_POST['Division'];
                                         $month = $_POST['month'];
                                          ?>
                                           <?php

                                                    $sqli_query="SELECT `employee_ID`, `FirstName`, `MiddleName`, `LastName`, `Gender`, `Address`, `ContactNumber`, `Birthdate`, `MaritalStatus`, `DateHired`, `PreviousWork`, `Salary`, `Position`, `Division`, `EmployementStatus`, `OfficeAssign` from employee where `Division` = '$Division'  ";
                                                    $result=$conn->query($sqli_query);
                                                    while ($row=$result->fetch_assoc()){
                                                        $employee_ID = $row['employee_ID'];
                                                       
                                                ?>
                                                    <tr>
                                                        <td><?= $row['FirstName'] ?></td> 
                                                        <td><?= $row['Position'] ?></td>
                                                        <td><?= $row['Division'] ?></td>
                                                        <td><?= $row['OfficeAssign'] ?></td>
                                                       
                                                            <?php

                                                                $sqli_query2="SELECT COUNT(Time_IN_AM) as total,Time_IN_AM,Time_Out_AM,TimeIN_PM,TimeOUT_PM,month(`Date`) as Month,`Date` FROM attendance WHERE employee_ID='$employee_ID' and `Month` = $month ";
                                                                $result2=$conn->query($sqli_query2);
                                                                $count = 0;
                                                                while ($row2=$result2->fetch_assoc()){
                                                                    $date1 = new DateTime($row2['Time_IN_AM']);
                                                                    $date2 = $date1->diff(new DateTime($row2['TimeOUT_PM']));
                                                                    $date = date("F",strtotime($row2['Date']));
                                                                    $absent = 31 - $row2['total'] ;
                                                                    

                                                                 
                                                                    $count++;
                                                            ?>
                                                                <td><?php  echo $date; ?></td>
                                                               
                                                                <td><?=  $row2['total']  ?></td>
                                                                     <td><?php  echo $absent  ?></td>
                                                                <td><center><?php 
                                                                    echo $date2->h.' hours'."\n";
                                                                  ?>
                                                                </center>
                                                                </td>
                                                           
                                                                <td>Present</td>
                                                            <?php } }?>
                                                            <?php  if ($count==0) {?>
                                                                <td><center>-</center></td>
                                                                <td><center>-</center></td>
                                                                <td><center>-</center></td>
                                                                <td><center>-</center></td>
                                                                <td><center>-</center></td> 
                                                                <td><center></td>
                                                                <td></td>
                                                             <?php }}?>

                                        </tbody>
                                    </table>
                                    </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        </section>
        <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <?php include("footer.php") ?>
        </footer>
        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->
        <!-- jQuery -->
        <script src="../plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- DataTables -->
        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- SlimScroll -->
        <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="../plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="../dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../dist/js/demo.js"></script>
        <!-- page script -->
        <script>
            function myFunction() {
              var input, filter, table, tr, td, i;
              input = document.getElementById("myInput");
              filter = input.value.toUpperCase();
              table = document.getElementById("example1");
              tr = table.getElementsByTagName("tr");
              for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                  if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                  } else {
                    tr[i].style.display = "none";
                  }
                }       
              }
            }
            
        </script>
    </body>
</html>