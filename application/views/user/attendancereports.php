<?php include "header.php"; ?>
<?php include "dbconnect.php"; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    	<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
   


          <div class="card card-success">
            <div class="card-header">
              <h3 class="card-title">List of Employee</h3>
            </div>
                          
           <div class="form-group" style='border: black;'><br>
                 

                </div>

            <div class="card-body"> 
            
              <?php 

                 $date1 = $_POST['date1'];
             $date2 = $_POST['date2'];

               ?>
                <h3><center> Attendance Report between  <?php echo date("M d, Y",strtotime($date1))." to ".date("M d, Y",strtotime($date2));?></center></h3>
              <table id="example1" class="table table-bordered table-hover">
                <!-- Working Days between  and  -->
                <thead>

                <tr>
                  <tr>
                  <th>Name</th>
                  <th>Position</th>
                  <th>Division</th>
                  <th>Address</th>
                  
            
                    <th>Total Work Hour</th>
                   <th>Employee Assigned</th>
                                   </tr>
                </tr>
                </thead>
                <tbody>
  <?php
        if (isset($_POST['display']))
        {
          $Division = $_POST['Division'];
           $date1 = $_POST['date1'];
             $date2 = $_POST['date2'];
             
           
        

     
                
    include ("dbconnect.php");
     

 $sqli_query="SELECT CONCAT(e.`FirstName` , e.`Lastname`) AS Name,Hour(TIMEDIFF(`Time_IN_AM`,`Time_Out_AM`)) as Hour1,Hour(TIMEDIFF(`TimeIN_PM`,`TimeOUT_PM`)) as Hour2,`Address`,e.`employee_ID`,`DateHired`,`PreviousWork`,`Salary`,`Position`,`Division`,`EmployementStatus` `Attendace_ID`, `Date`, `Time_IN_AM`, `Time_Out_AM`, `TimeIN_PM`, `TimeOUT_PM` FROM `attendance` as a, employee as e where a.`employee_ID`=e.`employee_ID`and  division='$Division'and date(`Date`) BETWEEN $date1 and $date2 ";
 $result=$conn->query($sqli_query);
if ($result->num_rows>0){
while ($row=$result->fetch_assoc()){

 echo"<td>".$row['Name']."</td>";
echo"<td>".$row['Position']."</td>";
echo"<td>".$row['Division']."</td>";
echo"<td>".$row['Address']."</td>";
echo"<td>".$row['DateHired']."</td>";
echo"<td>".$row['Salary']."</td>";
echo"<td>".$row['Salary']."</td>";

}}}
?>

                   
                  
            
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
  <?php include("footer.php") ?>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- SlimScroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- page script -->
<script>
function myFunction() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("example1");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}

</script>
</body>
</html>
