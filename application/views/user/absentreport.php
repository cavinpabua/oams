<?php include 'session.php';?>

<!DOCTYPE html>
<html>
<head>
<style>
.dot1 {
    height: 10px;
    width: 10px;
    background-color: green;
    border-radius: 50%;
    display: inline-block;
}
.dot2 {
    height: 10px;
    width: 10px;
    background-color: red;
    border-radius: 50%;
    display: inline-block;
}
.dot3 {
    height: 10px;
    width: 10px;
    background-color: pink;
    border-radius: 50%;
    display: inline-block;
}
#scrolltable { margin-top: 20px; height: 200px; overflow: auto; }
#scrolltable th div { position: absolute; margin-top: -20px; }

</style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>HRISDA | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="../https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">

<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand border-bottom navbar-dark bg-success">
    <!-- Left navbar links -->
    <?php include("topnav.php") ?>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4 sidebar-light-success">
    <?php include("sidenav.php") ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    	<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
   


          <div class="card card-success">
            <div class="card-header">
              <h3 class="card-title">Attendance Report</h3>
            </div>
                       <?php 

                 $date1 = $_POST['date1'];
             $date2 = $_POST['date2'];

               ?>    
     <h3><center> Absent report between  <?php echo date("M d, Y",strtotime($date1))." to ".date("M d, Y",strtotime($date2));?></center></h3>

            <div class="card-body"> 
            
              
              <table id="example1" class="table table-bordered table-hover">
                <thead>

                <tr>
                  <tr>
                  <th>Name</th>
                  <th>Position</th>
                  <th>Division</th>
                  <th>Address</th>
                  
                  
                    <th>Salary</th>
                   <th>Employee Assigned</th>
                   <th>Total Absent</th>
                                   </tr>
                </tr>
                </thead>
                <tbody>
               

                  <?php
        if (isset($_POST['display']))
        {
          $Division = $_POST['Division'];
           $date1 = $_POST['date1'];
             $date2 = $_POST['date2'];
             
    include ("dbconnect.php");
     
$sqli_query="SELECT CONCAT(e.`FirstName` , e.`Lastname`) AS Name,Hour(TIMEDIFF(`Time_IN_AM`,`Time_Out_AM`)) as Hour1,Hour(TIMEDIFF(`TimeIN_PM`,`TimeOUT_PM`)) as Hour2,`Address`,e.`employee_ID`,`DateHired`,`PreviousWork`,`Salary`,`Position`,`Division`,`EmployementStatus` `Attendace_ID`, `Date`, `Time_IN_AM`, `Time_Out_AM`, `TimeIN_PM`, `TimeOUT_PM`,COUNT(*) AS absent,`Status` FROM `attendance` as a, employee as e where  a.`Date` between '$date1' and '$date2' and a.`employee_ID`=e.`employee_ID` and `Status`='Absent'  and `Division`='$Division'   group by e.employee_ID order by e.employee_ID";
$result=$conn->query($sqli_query);
if ($result->num_rows>0){
while ($row=$result->fetch_assoc()){

 echo"<td>".$row['Name']."</td>";
echo"<td>".$row['Position']."</td>";
echo"<td>".$row['Division']."</td>";
echo"<td>".$row['Address']."</td>";
echo"<td>".$row['Salary']."</td>";
echo"<td>Main Office</td>";
echo"<td>".$row['absent']."</td></tr>";


// }}}
}}}
?>

                   
                  
            
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
  <?php include("footer.php") ?>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- SlimScroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- page script -->
<script>
function myFunction() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("example1");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}

</script>
</body>
</html>
