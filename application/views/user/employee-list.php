
    <?php include("topnav.php") ?>
  </nav>

  <aside class="main-sidebar elevation-4 sidebar-light-success">
    <?php include("sidenav.php") ?>
  </aside>

  <div class="content-wrapper">
    	<section class="content-header">
     <div class="col-sm-6">
        <h1 class="m-0 text-dark">Employee  
          <?php if(isset($_SESSION['success-add'])){?>
          <span style="color:green;font-size: 20px;padding-left: 20px;"><?php echo $_SESSION['success-add']; ?></span>
        <?php } ?>
        <?php if(isset($_SESSION['fail-add'])){?>
          <span style="color:red;font-size: 20px;padding-left: 20px;"><?php echo $_SESSION['fail-add']; ?></span>
        <?php } ?>
        </h1>

     
    </div>
    </section>
    <section class="content">
        <div class="col-12">
          <div class="card card-success">
            <div class="card-header">
              <h3 class="card-title">List of Employees</h3>
            </div>
            
            <div class="card-body"> 
              <a href="<?php echo site_url('Welcome/employee_add'); ?>"><button type="submit" id='add' class="btn btn-success pull-left"  ><i class="fa fa-user-plus">&nbsp;Add Employee</button></a></i><br>
             
               <br><br>
              <table id="example" class="table table-hover table-bordered ">
                <thead>
                    <tr class="text-center">
                        <th>
                            Bio name
                        </th>
                        <th>
                            Address
                        </th>
                        <th>
                            Gender
                        </th>
                        <th >
                            Contact Number
                        </th>
                        <th >
                            Position
                        </th>
                        <th >
                            Division
                        </th>
                        <th>Actions</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($employees as $x): ?>
                        <?php if($x->FirstName!=""){ ?>
                    <tr class="text-center">
                        <td><?php echo $x->BioName; ?></td>
                        <td><?php echo $x->Address; ?></td>
                        <td><?php echo $x->Gender; ?></td>
                        <td><?php echo $x->ContactNumber; ?></td>
                        <td><?php echo $x->Position; ?></td>
                        <td>
                          <?php foreach ($divisionlist as $y): ?>
                          <?php if($x->Division==$y->division_id){ echo $y->division_name; } ?>
                          <?php endforeach ?>
                        </td>
                        <td>
                          <div class="dropdown">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Actions
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                              <a class="dropdown-item" href="<?php echo base_url();?>Welcome/employee_info/<?php echo $x->employee_ID; ?>">Show Full Info</a>
                            </div>
                          </div>
                        </td>
                    </tr>
                      <?php  } ?>
                    <?php endforeach ?>
                </tbody>
              </table>
              
               <form method="POST" action="<?php echo site_url('Welcome/uploadUser'); ?>" enctype="multipart/form-data">
                   <div class="input-group col-md-4" style="float: left;">
                      
                <div class="input-group-prepend ">

                  <span class="input-group-text " id="inputGroupFileAddon01">Import Data</span>
                </div>
                <div class="custom-file">
                  <input type="file" name="userFile" class="custom-file-input" id="inputGroupFile01"
                    aria-describedby="inputGroupFileAddon01">
                  <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                </div>
                
               
              </div>
              <!-- <input type = "submit" value = "upload" />  -->
              <button class="btn btn-secondary" type="submit" name="submit" value="upload" >Import</button>
               </form>
            </div>

    
          </div>
        </div>
    </section>
  </div>
  <footer class="main-footer">
  <?php include("footer.php") ?>

<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable( {
        responsive: {
            details: {
                type: 'column'
            }
        },
        
        order: [ 0, 'asc' ],
        "lengthMenu": [5,10,20, 40, 60, 80, 100],
    } );
} );
            
        </script>