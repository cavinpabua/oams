
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>HRISDA | Reports</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand border-bottom navbar-dark bg-success">
    <?php include("topnav.php") ?>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4 sidebar-light-success">
    <!-- Brand Logo -->
 <?php include("logo.php") ?>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
<?php include("logo1.php") ?>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="index.php" class="nav-link active">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fa fa-users"></i>
              <p>
                Manage Employee
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="employee-list.php" class="nav-link">
                  <i class="fa fa-bars nav-icon"></i>
                  <p>List of Employee</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="employee-add.php" class="nav-link">
                  <i class="fa fa-user-plus nav-icon"></i>
                  <p>Add Employee</p>
                </a>
              </li>
            </ul>
          </li>
		  <li class="nav-item has-treeview">
            <a href="leave-request-list.php" class="nav-link active">
              <i class="nav-icon fa fa-edit"></i>
              <p>
               List of Leave Request
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="monitor-attendance.php" class="nav-link active">
              <i class="nav-icon fa fa-calendar"></i>
              <p>
                Monitor Attendance
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview">
            <a href="memos.php" class="nav-link active">
              <i class="nav-icon fa fa-comment"></i>
              <p>
                Memo / Announcement
              </p>
            </a>
          </li>
			<li class="nav-item has-treeview">
            <a href="profile.php" class="nav-link active">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Profile
              </p>
            </a>
          </li>
            <li class="nav-item has-treeview menu-open">
            <a href="" class="nav-link active">
              <i class="nav-icon fa fa-users"></i>
              <p>
                Reports
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="attendancereport.php" class="nav-link">
                  <i class="fa fa-calendar nav-icon"></i>
                  <p>Attendance Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="reports.php" class="nav-link">
                  <i class="fa fa-edit nav-icon"></i>
                  <p>Work Hour Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="reports.php" class="nav-link">
                  <i class="fa fa-edit nav-icon"></i>
                  <p>Resigned Report</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="reports.php" class="nav-link">
                  <i class="fa fa-edit nav-icon"></i>
                  <p>Leave Report</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="reports.php" class="nav-link">
                  <i class="fa fa-clipboard nav-icon"></i>
                  <p>Retired Report</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="reports.php" class="nav-link">
                  <i class="fa fa-clipboard nav-icon"></i>
                  <p>Retirable Report</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="reports.php" class="nav-link">
                  <i class="fa fa-edit nav-icon"></i>
                  <p>Fired Report</p>
                </a>
              </li>
            </ul>
          </li>
		  <li class="nav-item has-treeview">
            <a href="../login.php" class="nav-link active">
              <i class="nav-icon fa fa-sign-out"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    	<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card card-success">
            <div class="card-header">
              <h3 class="card-title">Employee Work hour Reports</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                 <th>Name</th>
                  <th>Position</th>
                  <th>Division</th>
                  <th>Work Hour</th>
                  
                  <th>Year</th>
                   <th>Action</th>
                 
                  
                 
                </tr>
               
               
                <tbody>
                <tr>
                                   <?php
$sqli_query="SELECT CONCAT(e.`FirstName` , e.`Lastname`) AS Name,e.employee_ID,d.Division_Des, p.Position_Des, typ.Leave_Description, YEAR(Date_Applied) AS year FROM `employee_details` AS ed, employee AS e, division AS d, position AS p, `employee_request` AS emr, type_of_leave AS typ WHERE e.employee_ID=ed.Employee_ID and d.Division_ID=ed.Division_ID and p.Position_ID = ed.Position_ID and emr.`Requst_ID` = typ.Leave_ID AND e.employee_ID = emr.Employee_ID   ";
$result=$conn->query($sqli_query);
if ($result->num_rows>0){
while ($row=$result->fetch_assoc()){
echo"<td>".$row['Name']."</td>";
echo"<td>".$row['Position_Des']."</td>";
echo"<td>".$row['Division_Des']."</td>";
echo"<td>".$row['Division_Des']."</td>";
echo"<td>".$row['year']."</td>";
echo"<td><a href='workdetails.php?employee_ID=".$row['employee_ID']."' class='fa fa-gear' >Action</a></td></tr>";



}
}
?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
  <?php include("footer.php") ?>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- SlimScroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
</body>
</html>
