<?php
class MYPDF extends TCPDF {
	//Page header
	public function Header() {
		// get the current page break margin
		$bMargin = $this->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $this->AutoPageBreak;
		// disable auto-page-break
		$this->SetAutoPageBreak(false, 0);
		// set bacground image
		$img_file = K_PATH_IMAGES.'dtr.jpg';
		$this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		// restore auto-page-break status
		$this->SetAutoPageBreak($auto_page_break, $bMargin);
		// set the starting point for the page content
		$this->setPageMark();
	}
}

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('OAMS DA');
$pdf->SetTitle('DTR FORM');
$pdf->SetSubject('DA DTR FORM');
$pdf->SetKeywords('DTR, FORM');
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);
$pdf->setPrintFooter(false);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}
// $pdf->SetFont('times', '', 12);


function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}
set_error_handler('exceptions_error_handler');

function exceptions_error_handler($severity, $message, $filename, $lineno) {
  if (error_reporting() == 0) {
    return;
  }
  if (error_reporting() & $severity) {
    throw new ErrorException($message, 0, $severity, $filename, $lineno);
  }
}

foreach ($chunked as $reqDATA) {

$html="";
$months=array();

//GET HOW MANY MONTHS
// print_r($reqDATA);

foreach ($reqDATA as $x) {
		// print_r($x->employee_ID);
	$date = (explode("-",$x->attendance_date));
	if (!in_array_r($date[1], $months)){
		array_push($months,[$date[1],$date[0],$date[2]]);
	}
}
// print_r(count($months));
try{

	$fullname = $reqDATA[0]->LastName.", ".$reqDATA[0]->FirstName;
}catch(Exception $e){
	$fullname = "No records Found";
	$pdf->AddPage();
	$pdf->SetFont('times', '', 12);
		$pdf->writeHTMLCell(0, 0, 25, 24.5, $fullname, 0, 1, 0, true, '', true);
}

foreach ($months as $key => $value) {
	if($key%2==0){
		//FIRST PART
		$pdf->AddPage();
		$pdf->SetFont('times', '', 12);
		$pdf->writeHTMLCell(0, 0, 25, 24.5, $fullname, 0, 1, 0, true, '', true);
		$dateObj   = DateTime::createFromFormat('!m', $value[0]);
		$monthName = $dateObj->format('F').", ".$value[1];
		$pdf->writeHTMLCell(0, 0, 50, 30.5, $monthName, 0, 1, 0, true, '', true);
		$pdf->SetFont('times', '', 10);
		foreach ($reqDATA as $x) {
			$date = (explode("-",$x->attendance_date));
			if($date[1]==$value[0]){
				if($date[2]=="01"){
					$pdf->writeHTMLCell(0, 0, 27, 56,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
							$pdf->writeHTMLCell(0, 0, 40, 56,"12:00" , 0, 1, 0, true, '', true);
							$pdf->writeHTMLCell(0, 0, 55, 56,"1:00" , 0, 1, 0, true, '', true);
						}
						
						$pdf->writeHTMLCell(0, 0, 70, 56,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);
						
						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 56,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
				}
				if($date[2]=="02"){
					$pdf->writeHTMLCell(0, 0, 27, 61,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 61,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 61,"1:00" , 0, 1, 0, true, '', true);
						}
						$pdf->writeHTMLCell(0, 0, 70, 61,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 61,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
				}
				if($date[2]=="03"){
					$pdf->writeHTMLCell(0, 0, 27, 66,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 66,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 66,"1:00" , 0, 1, 0, true, '', true);
					}
						$pdf->writeHTMLCell(0, 0, 70, 66,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);


						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 66,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
				}
				if($date[2]=="04"){
					$pdf->writeHTMLCell(0, 0, 27, 71,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 71,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 71,"1:00" , 0, 1, 0, true, '', true);
					}
						$pdf->writeHTMLCell(0, 0, 70, 71,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 71,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
				}
				if($date[2]=="05"){
					$pdf->writeHTMLCell(0, 0, 27, 76,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 76,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 76,"1:00" , 0, 1, 0, true, '', true);
						}
						$pdf->writeHTMLCell(0, 0, 70, 76,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 76,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
				}
				if($date[2]=="06"){
					$pdf->writeHTMLCell(0, 0, 27, 81,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 81,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 81,"1:00" , 0, 1, 0, true, '', true);
						}
						$pdf->writeHTMLCell(0, 0, 70, 81,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 81,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
				}
				if($date[2]=="07"){
					$pdf->writeHTMLCell(0, 0, 27, 86,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 86,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 86,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 70, 86,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 86,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="08"){
					$pdf->writeHTMLCell(0, 0, 27, 90,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 90,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 90,"1:00" , 0, 1, 0, true, '', true);
						}
						$pdf->writeHTMLCell(0, 0, 70, 90,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 90,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="09"){
					$pdf->writeHTMLCell(0, 0, 27, 95,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 95,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 95,"1:00" , 0, 1, 0, true, '', true);
						}
						$pdf->writeHTMLCell(0, 0, 70, 95,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 95,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="10"){
					$pdf->writeHTMLCell(0, 0, 27, 100,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 100,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 100,"1:00" , 0, 1, 0, true, '', true);
						}
						$pdf->writeHTMLCell(0, 0, 70, 100,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 100,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="11"){
					$pdf->writeHTMLCell(0, 0, 27, 105,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 105,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 105,"1:00" , 0, 1, 0, true, '', true);
						}
						$pdf->writeHTMLCell(0, 0, 70, 105,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 105,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="12"){
					$pdf->writeHTMLCell(0, 0, 27, 109,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 109,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 109,"1:00" , 0, 1, 0, true, '', true);
							}

						$pdf->writeHTMLCell(0, 0, 70, 109,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 109,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="13"){
					$pdf->writeHTMLCell(0, 0, 27, 114,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 114,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 114,"1:00" , 0, 1, 0, true, '', true);
						}
						$pdf->writeHTMLCell(0, 0, 70, 114,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 114,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="14"){
					$pdf->writeHTMLCell(0, 0, 27, 119,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 119,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 119,"1:00" , 0, 1, 0, true, '', true);
					}

						$pdf->writeHTMLCell(0, 0, 70, 119,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 119,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="15"){
					$pdf->writeHTMLCell(0, 0, 27, 124,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 124,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 124,"1:00" , 0, 1, 0, true, '', true);
					}

						$pdf->writeHTMLCell(0, 0, 70, 124,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 124,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="16"){
					$pdf->writeHTMLCell(0, 0, 27, 129,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 129,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 129,"1:00" , 0, 1, 0, true, '', true);
					}
						$pdf->writeHTMLCell(0, 0, 70, 129,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 129,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="17"){
					$pdf->writeHTMLCell(0, 0, 27, 134,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 134,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 134,"1:00" , 0, 1, 0, true, '', true);
						
						}
						$pdf->writeHTMLCell(0, 0, 70, 134,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);
						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 134,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="18"){
					$pdf->writeHTMLCell(0, 0, 27, 139,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 139,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 139,"1:00" , 0, 1, 0, true, '', true);
					}
						$pdf->writeHTMLCell(0, 0, 70, 139,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 139,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="19"){
					$pdf->writeHTMLCell(0, 0, 27, 143,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 143,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 143,"1:00" , 0, 1, 0, true, '', true);
					}

						$pdf->writeHTMLCell(0, 0, 70, 143,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 143,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="20"){
					$pdf->writeHTMLCell(0, 0, 27, 148,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 148,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 148,"1:00" , 0, 1, 0, true, '', true);
					}

						$pdf->writeHTMLCell(0, 0, 70, 148,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 148,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="21"){
					$pdf->writeHTMLCell(0, 0, 27, 153,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 153,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 153,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 70, 153,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 153,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="22"){
					$pdf->writeHTMLCell(0, 0, 27, 158,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 158,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 158,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 70, 158,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 158,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="23"){
					$pdf->writeHTMLCell(0, 0, 27, 163,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 163,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 163,"1:00" , 0, 1, 0, true, '', true);
					}

						$pdf->writeHTMLCell(0, 0, 70, 163,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 163,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="24"){
					$pdf->writeHTMLCell(0, 0, 27, 168,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 168,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 168,"1:00" , 0, 1, 0, true, '', true);

					}
						$pdf->writeHTMLCell(0, 0, 70, 168,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 168,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="25"){
					$pdf->writeHTMLCell(0, 0, 27, 173,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 173,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 173,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 70, 173,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 173,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="26"){
					$pdf->writeHTMLCell(0, 0, 27, 177,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 177,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 177,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 70, 177,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 177,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="27"){
					$pdf->writeHTMLCell(0, 0, 27, 182,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 182,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 182,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 70, 182,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 182,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="28"){
					$pdf->writeHTMLCell(0, 0, 27, 187,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 187,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 187,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 70, 187,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 187,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="29"){
					$pdf->writeHTMLCell(0, 0, 27, 192,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 192,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 192,"1:00" , 0, 1, 0, true, '', true);

							}
						$pdf->writeHTMLCell(0, 0, 70, 192,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 192,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="30"){
					$pdf->writeHTMLCell(0, 0, 27, 197,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 197,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 197,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 70, 197,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 197,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="31"){
					$pdf->writeHTMLCell(0, 0, 27, 202,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 40, 202,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 55, 202,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 70, 202,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 85, 202,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}



			}
			
		}
			

	}if($key%2==0){
		//SECOND PART
		// $pdf->AddPage();
		$pdf->SetFont('times', '', 12);
		$pdf->writeHTMLCell(0, 0, 128, 24.5, $fullname, 0, 1, 0, true, '', true);
		$dateObj   = DateTime::createFromFormat('!m', $value[0]);
		$monthName = $dateObj->format('F').", ".$value[1];
		$pdf->writeHTMLCell(0, 0, 150, 30.5, $monthName, 0, 1, 0, true, '', true);
		$pdf->SetFont('times', '', 10);
		foreach ($reqDATA as $x) {
			$date = (explode("-",$x->attendance_date));
			if($date[1]==$value[0]){
				if($date[2]=="01"){
					$pdf->writeHTMLCell(0, 0, 130, 56,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 56,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 56,"1:00" , 0, 1, 0, true, '', true);

							}
						$pdf->writeHTMLCell(0, 0, 174, 56,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 56,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
				}
				if($date[2]=="02"){
					$pdf->writeHTMLCell(0, 0, 130, 61,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 61,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 61,"1:00" , 0, 1, 0, true, '', true);
						}
						$pdf->writeHTMLCell(0, 0, 174, 61,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 61,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
				}
				if($date[2]=="03"){
					$pdf->writeHTMLCell(0, 0, 130, 66,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 66,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 66,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 174, 66,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 66,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
				}
				if($date[2]=="04"){
					$pdf->writeHTMLCell(0, 0, 130, 71,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 71,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 71,"1:00" , 0, 1, 0, true, '', true);
						}
						$pdf->writeHTMLCell(0, 0, 174, 71,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 71,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
				}
				if($date[2]=="05"){
					$pdf->writeHTMLCell(0, 0, 130, 76,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 76,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 76,"1:00" , 0, 1, 0, true, '', true);
						}

						$pdf->writeHTMLCell(0, 0, 174, 76,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 76,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
				}
				if($date[2]=="06"){
					$pdf->writeHTMLCell(0, 0, 130, 81,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 81,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 81,"1:00" , 0, 1, 0, true, '', true);
					}
						$pdf->writeHTMLCell(0, 0, 174, 81,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 81,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
				}
				if($date[2]=="07"){
					$pdf->writeHTMLCell(0, 0, 130, 86,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 86,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 86,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 174, 86,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 86,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="08"){
					$pdf->writeHTMLCell(0, 0, 130, 90,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 90,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 90,"1:00" , 0, 1, 0, true, '', true);
					}
						$pdf->writeHTMLCell(0, 0, 174, 90,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 90,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="09"){
					$pdf->writeHTMLCell(0, 0, 130, 95,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 95,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 95,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 174, 95,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 95,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="10"){
					$pdf->writeHTMLCell(0, 0, 130, 100,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 100,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 100,"1:00" , 0, 1, 0, true, '', true);
					}
						$pdf->writeHTMLCell(0, 0, 174, 100,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 100,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="11"){
					$pdf->writeHTMLCell(0, 0, 130, 105,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 105,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 105,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 174, 105,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 105,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="12"){
					$pdf->writeHTMLCell(0, 0, 130, 109,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 109,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 109,"1:00" , 0, 1, 0, true, '', true);
					}
						$pdf->writeHTMLCell(0, 0, 174, 109,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 109,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="13"){
					$pdf->writeHTMLCell(0, 0, 130, 114,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 114,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 114,"1:00" , 0, 1, 0, true, '', true);
							}

						$pdf->writeHTMLCell(0, 0, 174, 114,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 114,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="14"){
					$pdf->writeHTMLCell(0, 0, 130, 119,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 119,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 119,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 174, 119,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 119,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="15"){
					$pdf->writeHTMLCell(0, 0, 130, 124,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 124,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 124,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 174, 124,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 124,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="16"){
					$pdf->writeHTMLCell(0, 0, 130, 129,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 129,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 129,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 174, 129,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 129,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="17"){
					$pdf->writeHTMLCell(0, 0, 130, 134,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 134,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 134,"1:00" , 0, 1, 0, true, '', true);
					}
						$pdf->writeHTMLCell(0, 0, 174, 134,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 134,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="18"){
					$pdf->writeHTMLCell(0, 0, 130, 139,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 139,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 139,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 174, 139,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 139,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="19"){
					$pdf->writeHTMLCell(0, 0, 130, 143,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 143,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 143,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 174, 143,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 143,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="20"){
					$pdf->writeHTMLCell(0, 0, 130, 148,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 148,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 148,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 174, 148,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 148,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="21"){
					$pdf->writeHTMLCell(0, 0, 130, 153,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 153,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 153,"1:00" , 0, 1, 0, true, '', true);
							}

						$pdf->writeHTMLCell(0, 0, 174, 153,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 153,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="22"){
					$pdf->writeHTMLCell(0, 0, 130, 158,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 158,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 158,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 174, 158,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 158,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="23"){
					$pdf->writeHTMLCell(0, 0, 130, 163,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 163,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 163,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 174, 163,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 163,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="24"){
					$pdf->writeHTMLCell(0, 0, 130, 168,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 168,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 168,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 174, 168,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 168,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="25"){
					$pdf->writeHTMLCell(0, 0, 130, 173,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 173,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 173,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 174, 173,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 173,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="26"){
					$pdf->writeHTMLCell(0, 0, 130, 177,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 177,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 177,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 174, 177,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 177,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="27"){
					$pdf->writeHTMLCell(0, 0, 130, 182,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 182,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 182,"1:00" , 0, 1, 0, true, '', true);
							}

						$pdf->writeHTMLCell(0, 0, 174, 182,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 182,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="28"){
					$pdf->writeHTMLCell(0, 0, 130, 187,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 187,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 187,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 174, 187,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 187,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="29"){
					$pdf->writeHTMLCell(0, 0, 130, 192,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){

						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 192,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 192,"1:00" , 0, 1, 0, true, '', true);
							}

						$pdf->writeHTMLCell(0, 0, 174, 192,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 192,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="30"){
					$pdf->writeHTMLCell(0, 0, 130, 197,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 197,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 197,"1:00" , 0, 1, 0, true, '', true);
							}
						$pdf->writeHTMLCell(0, 0, 174, 197,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 197,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}
				if($date[2]=="31"){
					$pdf->writeHTMLCell(0, 0, 130, 202,date('g:i', strtotime($x->time_in)) , 0, 1, 0, true, '', true);
					if(strtotime($x->time_out)!=strtotime("00:00:00")){
						if(!(strtotime($x->time_out)>=strtotime("12:00:00") && strtotime($x->time_out)<=strtotime("13:00:00"))){
						$pdf->writeHTMLCell(0, 0, 143.5, 202,"12:00" , 0, 1, 0, true, '', true);
						$pdf->writeHTMLCell(0, 0, 158, 202,"1:00" , 0, 1, 0, true, '', true);

							}
						$pdf->writeHTMLCell(0, 0, 174, 202,date('g:i', strtotime($x->time_out)) , 0, 1, 0, true, '', true);

						if(((int)$x->totalHour)<8){
							//Mark UnderTime
							$pdf->writeHTMLCell(0, 0, 188, 202,round($x->totalHour, 1) , 0, 1, 0, true, '', true);
						}
					}
					
				}



			}
			
		}
	}
}
}
// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(false, 0);

// restore auto-page-break status
$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
// set the starting point for the page content
$pdf->setPageMark();


// Print a text


// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('dtrgroup.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+