<table id="example1" class="table table-bordered table-hover">
   <thead>
      <tr>
      <tr>
         <th>Date</th>
         <th colspan="2" style="text-align: center;">AM</th>
         <th colspan="2" style="text-align: center;">PM</th>
         <th rowspan="2" style="background: #756969;color: #fff;">Working Hours</th>
      </tr>
      <tr>
         <th></th>
         <th style="text-align: center;">Time In</th>
         <th style="text-align: center;">Time Out</th>
         <th style="text-align: center;">Time In</th>
         <th style="text-align: center;">Time Out</th>
         <th></th>
      </tr>
  </thead>
  <tbody>
      <?php 
         session_start();
         include  ('../dbconnect.php'); 
         if ( isset($_SESSION['late_report_from']) ) {  

            $date_from = $_SESSION['late_report_from'];
            $date_to = $_SESSION['late_report_to']; 
         }
         $sum_total_hours = 0;
         $total_minutes = 0;
         $employee_ID = $_POST['employee_ID'];
         if ( isset($_SESSION['late_report_from']) ) {
       
            $sqli_query2="SELECT * FROM attendance WHERE employee_ID='$employee_ID'  AND  Date BETWEEN DATE('$date_from') AND DATE('$date_to')  ";
         }else{
   
            $sqli_query2="SELECT * FROM attendance WHERE employee_ID='$employee_ID'  AND  DATE(`Date`) = DATE(CURDATE()) ";
         }
        
          $late_time_minutes = 0;
          $late_time_hours = 0;
          $sum_total_hours = 0;
          $sum_total_minutes  = 0;
         $result2=$conn->query($sqli_query2);
         while ($row2=$result2->fetch_assoc()){
             $time_in_am = $row2['Time_IN_AM'] ;
              $time_in_pm = $row2['TimeIN_PM']; 

              $CURRENTTIME = new DateTime($row2['Time_IN_AM']);
              $OFFICETIME  = new DateTime('08:00:00');
              if ($CURRENTTIME  > $OFFICETIME) {

                  $date1 = new DateTime($time_in_am);
                  $date2 = $date1->diff(new DateTime('08:00:00'));
                  $attendace_pm_minutes = $date2->i;
                  $attendace_pm_hours = $date2->h;
                  $late_time_minutes += $attendace_pm_minutes ;
                  $late_time_hours += $attendace_pm_hours ;
          } 
          $total_hours = $late_time_hours ; 
          $total_minutes =  round($late_time_minutes/60*10);
          $sum_total_minutes += $total_minutes;
          $convert_total_hours = $total_hours.'.'.$total_minutes; 
          $sum_total_hours +=$convert_total_hours;

      ?>
      <tr >
         <td><?= date('Y-m-d', strtotime($row2['Date'])) ?></td>
         <td><?= $row2['Time_IN_AM'] ?></td>
         <td><?= $row2['Time_Out_AM'] ?></td>
         <td><?= $row2['TimeIN_PM'] ?></td>
         <td><?= $row2['TimeOUT_PM'] ?></td>
         <td style="background: #47985a;color: #fff;">
           <?= $convert_total_hours ?> hrs
            
         </td>
      
      </tr>
     <?php } ?>
     <tfoot style="font-weight: bold;font-size: 16px">
         <tr>
            <td colspan="5" style="text-align: right;">Total</td>
            <td><?= round($sum_total_hours,1) ?> hrs</td>
         </tr>
      </tfoot>


  </tbody>
</table>