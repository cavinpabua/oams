
    <?php include("topnav.php") ?>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4 sidebar-light-success">
    <?php include("sidenav.php") ?>
  </aside>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <?php if(isset($_SESSION['success-fail'])){?>
          <span style="color:red;font-size: 20px;padding-left: 20px;"><?php echo $_SESSION['success-fail']; ?></span>
        <?php } ?>
          </div>
          <div class="col-sm-6">
           
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Add Employee</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
       
            <div class="card-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-4">
                    
                    <form method="POST" action="<?php echo site_url('Welcome/employee_add'); ?>">
                      
                  
                      <label for="">First Name</label>
                      <div class="form-group">
                        <div class="input-group-prepend">
                           </div>
                             <input type="text" class="form-control" name="FirstName" placeholder="First Name" required>
                                </div>
                                  <!-- /input-group -->
                                     </div>
                                        <!-- /.col-lg-6 -->
                  <div class="col-lg-4">
                     <label for="">Middle Initial</label>
                    <div class="form-g roup">
                      <div class="input-group-prepend">
                          </div>
                            <input type="text" maxlength="1" name="MiddleName" class="form-control" placeholder="Middle Name" required>
                              </div>
                                <!-- /input-group -->
                                  </div>
                                     <!-- /.col-lg-6 -->
                                      
                <div class="col-lg-4">
                  <label for="exampleInputEmail1">Last Name</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="text" name="LastName" class="form-control" placeholder="Last Name" required>
                              </div>
                                <!-- /input-group -->
                                  </div>
                                     <!-- /.col-lg-6 -->
                                    <div class="col-lg-4">
                 <label for="exampleInputEmail1">Gender</label>
                    <div class="form-group">
                       <div class="input-group-prepend"></div>
                          <select class="form-control select2" name="Gender" style="width: 100%;" required>
                             <option selected="selected" disabled>Gender</option>
                                
                                   <option value="Female" selected>Female</option>
                                      <option value="Male">Male</option>
                                         </select> 
                                            </div>
                                              <!-- /input-group -->
                                                </div>

                                              <!-- /.col-lg-6 -->
                                             
                      <div class="col-lg-4">
               <label for="exampleInputPassword1" >Bio Name</label>
                  <input type="text" name="BioName" class="form-control"  placeholder="Bio Name" required>

                      </div>
                      <div class="col-lg-4">
               <label for="exampleInputPassword1" >Address</label>
                  <input type="text" name="Address" class="form-control"  placeholder="Address" required>

                      </div>
                      <div class="col-lg-4">
               <label for="exampleInputPassword1" >Email Address</label>
                  <input type="email" name="email" class="form-control"  placeholder="Email" required>

                      </div>
                       <div class="col-lg-4">
               <label for="exampleInputPassword1" >Contact Number</label>
                  <input type="text" name="ContactNumber" class="form-control"  placeholder="Contact Number" required>

                      </div>

               
                                     <!-- /.col-lg-6 -->
                                     <div class="col-lg-4">
                  <label for="exampleInputEmail1">Birthdate</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                           <input type="Date" name="Birthdate" class="form-control" required>
                              </div>
                                <!-- /input-group -->
                                  </div>
 <div class="col-lg-4">
                 <label for="exampleInputEmail1">Marital Status</label>
                    <div class="form-group">
                       <div class="input-group-prepend"></div>
                          <select class="form-control select2" name="MaritalStatus" style="width: 100%;" required>
                             <option selected="selected" disabled>Marital Status</option>
                                <option value="Single">Single</option>
                                   <option value="Widowed">Widowed</option>
                                      <option value="Married">Married</option>
                                         </select> 
                                            </div>
                                              <!-- /input-group -->
                                                </div>
                                                 <div class="col-lg-4">
                                                 
                  <label for="exampleInputEmail1">Date hired</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="date" name="DateHired" class="form-control" placeholder="Date hired" required>
                              </div>
                                <!-- /input-group -->
                                  </div>
                                   <div class="col-lg-4">
                  <label for="exampleInputEmail1">Previous work experience</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="text" name="PreviousWork" class="form-control" placeholder="Previous work experience" required>
                              </div>
                                <!-- /input-group -->
                                  </div>
                                   <div class="col-lg-4">
                  <label for="exampleInputEmail1">Salary Grade</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="text" name="Salary" class="form-control" placeholder="Salary Grade" required>
                              </div>
                                <!-- /input-group -->
                                  </div>
              
                                     <!-- /.col-lg-6 -->
                                  

 <div class="col-lg-4">
                 <label for="exampleInputEmail1">Position</label>
                    <div class="form-group">
                       <div class="input-group-prepend"></div>
                          <select class="form-control select2" name="Position" style="width: 100%;" required>
                             <option selected="selected" disabled>Position</option>
                                <option value="employee">Employee</option>
                                <option value="Supervisor">Supervisor</option>
                                   <option value="HumanResource">Human Resource</option>
                                      <option value="Head">Head</option>
                                         </select> 
                                            </div>
               </div>
                                     <!-- /.col-lg-6 -->

              <div class="col-lg-4">
                 <label for="exampleInputEmail1">Division</label>
                    <div class="form-group">
                       <div class="input-group-prepend"></div>
                          <select class="form-control select2" name="Division" style="width: 100%;" required>
                             <option selected="selected" disabled>Division</option>
                                <option value="Accounting">Accounting</option>
                                   <option value="HR">HR</option>
                                      <option value="ICT">ICT</option>
                                         </select> 
                                            </div>
                                                                           
            


                        
</div>
                <div class="col-lg-4">
                 <label for="exampleInputEmail1">Employment Status</label>
                    <div class="form-group">
                       <div class="input-group-prepend"></div>
                          <select class="form-control select2" name="EmploymentStatus" style="width: 100%;" required>
                             <option selected="selected" disabled>Employment Status</option>
                                <option value="Regular">Regular</option>
                                   <option value="HR">HR</option>
                                                                              </select> 
                                            </div>
                                     <!-- /.col-lg-6 --></div>
 <div class="col-lg-4">
                  <label for="exampleInputEmail1">Office Assign</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="text" name="OfficeAssign" class="form-control" placeholder="Office Assign" required>
                              </div>
                                <!-- /input-group -->
                                  </div>
                                  <div class="col-lg-4">
                  <label for="exampleInputEmail1">Password</label>
                    <div class="form-group" style="color:red;">
                      
                            Password is autogenerated with the combination of the first name and last name of the employee. <br>(Ex. John Abad = johnabad)
                              </div>
                                <!-- /input-group -->
                                  </div>
                       
                         
                </div>
                <!-- /.card-body -->	
                  <button type="submit" class="submit btn btn-success btn-shadow btn-gradient" name='addEmp'>Save</button>
                  <a class="btn btn-default" href="<?php echo site_url('Welcome/employee'); ?>">Cancel</a>
                  
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <!-- /.content-wrapper -->
  <footer class="main-footer">
	<?php include("footer.php") ?>

