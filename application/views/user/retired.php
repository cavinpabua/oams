<?php include "header.php"; ?>
<?php include "dbconnect.php"; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    	<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card card-success">
            <div class="card-header">
              <h3 class="card-title">List of Employee</h3>
            </div>
            <!-- /.card-header -->

            <div class="card-body"> 
              <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name" class="form-control">
              
              <table id="example1" class="table table-bordered table-hover">
                <thead>

                <tr>
                  <tr>
                  <th>Name</th>
                  <th>Position</th>
                  <th>Division</th>
                  <th>Address</th>
                  
                  <th>Date Hired</th>
                    <th>Salary</th>
                   <th>Employee Assigned</th>
                   <th>Date Retired</th>
                                   </tr>
                </tr>
                </thead>
                <tbody>
                  

<tr><td> Jinivib Delalamon</td>
<td>Department Head</td>
<td>IT Department</td>
<td>Corales Cagayan de Oro</td>
<td>March 07,1987</td>
<td>40000</td>
<td>Main Office</td>
<td>Date Retired</td></tr>
<tr><td> Nasser Macabangkit</td>
<td>Manager</td>
<td>Accounting Department</td>
<td>Bulua Cagayan de Oro</td>
<td>January 07,1991</td>
<td>40000</td>
<td>Annex Office</td>
<td>Date Retired</td></tr>




                   
                  
            
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
  <?php include("footer.php") ?>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- SlimScroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- page script -->
<script>
function myFunction() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("example1");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}

</script>
</body>
</html>
