<?php 
include 'session.php';
include ("dbconnect.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <style>
            .dot1 {
            height: 10px;
            width: 10px;
            background-color: green;
            border-radius: 50%;
            display: inline-block;
            }
            .dot2 {
            height: 10px;
            width: 10px;
            background-color: red;
            border-radius: 50%;
            display: inline-block;
            }
            .dot3 {
            height: 10px;
            width: 10px;
            background-color: pink;
            border-radius: 50%;
            display: inline-block;
            }
            #scrolltable { margin-top: 20px; height: 200px; overflow: auto; }
            #scrolltable th div { position: absolute; margin-top: -20px; }
        </style>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>OAMS | Report</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="../https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../dist/css/adminlte.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="../plugins/morris/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="../https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand border-bottom navbar-dark bg-success">
                <!-- Left navbar links -->
                <?php include("topnav.php") ?>
            </nav>
            <!-- /.navbar -->
            <!-- Main Sidebar Container -->
            <aside class="main-sidebar elevation-4 sidebar-light-success">
                <?php include("sidenav.php") ?>
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                            </div>
                            <div class="col-sm-6">
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-success">
                                <div class="card-header">
                                    <h3 class="card-title">Attendance Report</h3>
                                </div>
                                <div class="form-group" style='border: black;'>
                                    <br>
                                    &nbsp;&nbsp; &nbsp;&nbsp;
                                    <form method="POST">
                                     
                                     <input type="date"  name="date1" style="width: 250px;    padding: 12px 20px;
                                        margin: 8px 0;
                                        display: inline-block;
                                        border: 1px solid #ccc;
                                        border-radius: 4px;
                                        box-sizing: border-box;height: 45px;"><input type="date" name="date2" style="width: 250px;    padding: 12px 20px;
                                        margin: 8px 0;
                                        display: inline-block;
                                        border: 1px solid #ccc;
                                        border-radius: 4px;
                                        box-sizing: border-box;height: 45px;">
                                      
                                          <input id="myInput" type="text" onkeyup="myFunction()" name="FirstName"  style="width: 250px;    padding: 12px 20px;
                                        margin: 8px 0;
                                        display: inline-block;
                                        border: 1px solid #ccc;
                                        border-radius: 4px;
                                        box-sizing: border-box;height: 45px;" placeholder="Employee Name">

                                        <button class="btn btn-success"  onclick="submit_data()" type="button" name="display"> <i class="nav-icon fa fa-search">Display </button></i> <a href="attendance_summary.php" class="btn btn-success"   >Monthly Summary </a></i> 

                                  </form>
                                </div>
                                <div class="card-body">
                                    <table id="example1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                            <tr>
                                                <th>Employee ID</th>
                                                <th>Name</th>
                                                <th>Position</th>
                                                <th>Division</th>
                                                <th>Assign Office</th>
                                                <th>Month</th>
                                                <th>Total Present</th>
                                                <th>Total Absent</th>
                                                <th>Working hour</th>
                                                <th>Action</th>
                                            </tr>
                                            </tr>
                                        </thead>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $sqli_query="SELECT * FROM `user` inner JOIN employee ON user.employee_ID = employee.employee_ID INNER join attendance on employee.employee_ID = attendance.Employee_ID WHERE user_type!='admin' group by user.employee_ID ";
                                                $result=$conn->query($sqli_query);
                                                while ($row=$result->fetch_assoc()){
                                                    $employee_ID = $row['employee_ID'];
                                                   
                                                ?>
                                            <tr>
                                              <td><?= $row['employee_ID'] ?></td>
                                                <td><?= $row['FirstName'] ?>&nbsp;<?= $row['LastName'] ?></td>
                                                <td><?= $row['Position'] ?></td>
                                                <td><?= $row['Division'] ?></td>
                                                <td><?= $row['OfficeAssign'] ?></td>
                                                <?php
                                                //$sqli_query2="SELECT COUNT(Time_IN_AM) as total,Time_IN_AM,Time_Out_AM,TimeIN_PM,TimeOUT_PM,month(`Date`) as Month,`Date` FROM attendance WHERE employee_ID='$employee_ID' ";
                                                    $sqli_query2="SELECT * FROM attendance WHERE employee_ID='$employee_ID' ";
                                                    $result2=$conn->query($sqli_query2);
                                                    $count = 0;
                                                    $sum_attandance = 0;
                                                    while ($row2=$result2->fetch_assoc()){
                                                        $count++;
                                                        $attendace_am  = 0;
                                                        $attendace_pm  = 0;
                                                        # morning attendace
                                                        if ( $row2['Time_IN_AM'] != '00:00:00'  ) {
                                                            
                                                            $time_in = $row2['Time_IN_AM']; 
                                                            $time_out = $row2['Time_Out_AM']; 
                                                            $date1 = new DateTime($time_in);
                                                            $date2 = $date1->diff(new DateTime($time_out));
                                                            $attendace_am = $date2->h;
                                                        }

                                                        if ( $row2['TimeIN_PM'] != '00:00:00'  ) {
                                                            
                                                            $time_in = $row2['TimeIN_PM']; 
                                                            $time_out = $row2['TimeOUT_PM']; 
                                                            $date1 = new DateTime($time_in);
                                                            $date2 = $date1->diff(new DateTime($time_out));
                                                            $attendace_pm = $date2->h;
                                                        }

                                                        $total_hours = $attendace_am + $attendace_pm ;

                                                        if ( $total_hours > 8 ) {

                                                            $total_hours = 8;
                                                        }else{

                                                            $total_hours = $total_hours;
                                                        }

                                                        $sum_attandance += $total_hours;

                                                    }

                                                    $days_of_month = date('d');
                                                    $absent = $days_of_month - $count;
                                                   


                                                    
                                                    
                                                    ?>
                                               
                                                <td><?=  $count  ?></td>
                                                <td><?=  $count  ?></td>
                                                <td><?=  $absent  ?></td>
                                                <td align="center"><?= $sum_attandance ?></td> 
                                                 
                                                <td><?php echo"<a href='sum.php?' class='btn btn-block btn-outline-success' >View details</a>"?></td>
                                            </tr>
                                             <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        </section>
        <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <?php include("footer.php") ?>
        </footer>
        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->
        <!-- jQuery -->
        <script src="../plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- DataTables -->
        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- SlimScroll -->
        <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="../plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="../dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../dist/js/demo.js"></script>
        <!-- page script -->
        <script>
            function myFunction() {
              var input, filter, table, tr, td, i;
              input = document.getElementById("myInput");
              filter = input.value.toUpperCase();
              table = document.getElementById("example1");
              tr = table.getElementsByTagName("tr");
              for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                  if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                  } else {
                    tr[i].style.display = "none";
                  }
                }       
              }
            }

            function submit_data() {
                var month = $("#month").val();
                 var year = $("#year").val();
                window.location='absentsummary.php?month='+month+'&year='+year;
            }
            
        </script>
    </body>
</html>