<?php
class MYPDF extends TCPDF {
	//Page header
	public function Header() {
		// get the current page break margin
		$bMargin = $this->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $this->AutoPageBreak;
		// disable auto-page-break
		$this->SetAutoPageBreak(false, 0);
		// set bacground image
		$img_file = K_PATH_IMAGES.'LEAVEFORM.jpg';
		$this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		// restore auto-page-break status
		$this->SetAutoPageBreak($auto_page_break, $bMargin);
		// set the starting point for the page content
		$this->setPageMark();
	}
}

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('OAMS DA');
$pdf->SetTitle('LEAVE FORM');
$pdf->SetSubject('DA LEAVE FORM');
$pdf->SetKeywords('LEAVE, FORM');
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);
$pdf->setPrintFooter(false);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}
$pdf->SetFont('times', '', 12);
$pdf->AddPage();

// Print a text
//LAST NAME
$lastname = $reqDATA->row()->LastName;
$firstname = $reqDATA->row()->FirstName;
$html = <<<EOD
$lastname
EOD;
$pdf->writeHTMLCell(0, 0, 88, 26, $html, 0, 1, 0, true, '', true);
$firstname = $reqDATA->row()->FirstName;
$html = <<<EOD
$firstname
EOD;
$pdf->writeHTMLCell(0, 0, 122, 26, $html, 0, 1, 0, true, '', true);
$middlename = $reqDATA->row()->MiddleName;
$html = <<<EOD
$middlename
EOD;
$pdf->writeHTMLCell(0, 0, 176, 26, $html, 0, 1, 0, true, '', true);
$date_applied =date("F j, Y", strtotime($reqDATA->row()->Date_Applied)) ;
$html = <<<EOD
$date_applied
EOD;
$pdf->writeHTMLCell(0, 0, 22, 35, $html, 0, 1, 0, true, '', true);
$position = $reqDATA->row()->Position;
$html = <<<EOD
$position
EOD;
$pdf->writeHTMLCell(0, 0, 80, 35, $html, 0, 1, 0, true, '', true);
$salary = $reqDATA->row()->Salary;
if($salary=="0"){
	$salary="";
}

$html = <<<EOD
$salary
EOD;
$pdf->writeHTMLCell(0, 0, 160, 35, $html, 0, 1, 0, true, '', true);
$pdf->SetFont('times', '', 11);
$leavetype = $reqDATA->row()->leavetype;
$html = <<<EOD
x
EOD;
if($leavetype=="Maternity"){
	$pdf->writeHTMLCell(0, 0, 24, 79, $html, 0, 1, 0, true, '', true);
}else if($leavetype=="Sick"){
	$pdf->writeHTMLCell(0, 0, 24, 69, $html, 0, 1, 0, true, '', true);
	$leavespent = $reqDATA->row()->LeaveSpent;
	if($leavespent=="In Hospital"){

		$pdf->writeHTMLCell(0, 0, 122.5, 80, $html, 0, 1, 0, true, '', true);
	}else{
		$pdf->writeHTMLCell(0, 0, 121.5, 90, $html, 0, 1, 0, true, '', true);
	}
}else if($leavetype=="Vacation"){
	$pdf->writeHTMLCell(0, 0, 21, 50, $html, 0, 1, 0, true, '', true);
	$leavespent = $reqDATA->row()->LeaveSpent;
	if($leavespent=="Within the Philippines"){

		$pdf->writeHTMLCell(0, 0, 121.5, 55, $html, 0, 1, 0, true, '', true);
	}else{
		$pdf->writeHTMLCell(0, 0, 122.5, 65.5, $html, 0, 1, 0, true, '', true);
	}

}else{
	$pdf->writeHTMLCell(0, 0, 23.5, 89, $html, 0, 1, 0, true, '', true);
	$pdf->SetFont('times', '', 12);
	$other = $reqDATA->row()->other;
	$html = $other;
	$pdf->writeHTMLCell(0, 0, 45, 93, $html, 0, 1, 0, true, '', true);
}
if($leavetype!="Maternity"){
	$sickCreds = $reqDATA->row()->sick_leave_credits;
	$vacationCreds = $reqDATA->row()->vacation_leave_credits;
	$asOfDate = date("F j, Y", strtotime($reqDATA->row()->date_updated));
	$total = $sickCreds + $vacationCreds;
	$pdf->writeHTMLCell(0, 0, 45, 138, $asOfDate, 0, 1, 0, true, '', true);
	$pdf->writeHTMLCell(0, 0, 35, 158, $vacationCreds, 0, 1, 0, true, '', true);
	$pdf->writeHTMLCell(0, 0, 60, 158, $sickCreds, 0, 1, 0, true, '', true);
	$pdf->writeHTMLCell(0, 0, 80, 158, $total, 0, 1, 0, true, '', true);
}
$pdf->SetFont('times', '', 11);
$html = "x";
$commutations = $reqDATA->row()->commutations;
if($commutations=="Requested"){
	
	$pdf->writeHTMLCell(0, 0, 122, 104.5, $html, 0, 1, 0, true, '', true);
}
if($commutations=="Not Requested"){
	$pdf->writeHTMLCell(0, 0, 164.5, 104.5, $html, 0, 1, 0, true, '', true);
}

$pdf->SetFont('times', '', 13);
$html = $reqDATA->row()->NumberofWorkingDays." days";
$pdf->writeHTMLCell(0, 0, 50, 103, $html, 0, 1, 0, true, '', true);

$html =date("F j, Y", strtotime($reqDATA->row()->Date_of_LeaveFrom))."-".date("F j, Y", strtotime($reqDATA->row()->Date_of_LeaveTo)) ;
$pdf->writeHTMLCell(0, 0, 30, 116, $html, 0, 1, 0, true, '', true);

$recommend_by = $reqDATA->row()->recommend_by;
$recom_split=(explode(";",$recommend_by));
$html=<<<EOD
<div >
<strong >
<u>$recom_split[0]</u>
</strong><br>
$recom_split[1]
</div>
EOD;
$pdf->writeHTMLCell(0, 0, 130, 175, $html, 0, 1, 0, true, '', true);

$approved_by = $reqDATA->row()->approved_by;
$app_split=(explode(";",$approved_by));
$html=<<<EOD
<div >
<strong >
<u>$app_split[0]</u>
</strong><br>
$app_split[1]
</div>
EOD;
$pdf->writeHTMLCell(0, 0, 77, 238, $html, 0, 1, 0, true, '', true);
// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(false, 0);

// restore auto-page-break status
$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
// set the starting point for the page content
$pdf->setPageMark();


// Print a text


// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('leaveform.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+