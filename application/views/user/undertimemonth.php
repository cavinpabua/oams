<?php 
    include 'session.php';
    include ("dbconnect.php");
    ?>
<!DOCTYPE html>
<html>
    <head>
        <style>
            .dot1 {
            height: 10px;
            width: 10px;
            background-color: green;
            border-radius: 50%;
            display: inline-block;
            }
            .dot2 {
            height: 10px;
            width: 10px;
            background-color: red;
            border-radius: 50%;
            display: inline-block;
            }
            .dot3 {
            height: 10px;
            width: 10px;
            background-color: pink;
            border-radius: 50%;
            display: inline-block;
            }
            #scrolltable { margin-top: 20px; height: 200px; overflow: auto; }
            #scrolltable th div { position: absolute; margin-top: -20px; }
        </style>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>HRISDA | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="../https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../dist/css/adminlte.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="../plugins/morris/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="../https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand border-bottom navbar-dark bg-success">
            <!-- Left navbar links -->
            <?php include("topnav.php") ?>
        </nav>
        <!-- /.navbar -->
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar elevation-4 sidebar-light-success">
            <?php include("sidenav.php") ?>
        </aside>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-success">
                            <div class="card-header">
                                <h3 class="card-title">Attendance Report</h3>
                            </div>
                            <div class="form-group" style='border: black;'>
                                <br>
                                &nbsp;&nbsp; &nbsp;&nbsp;
                                <form method="POST">
                                    <?php  
                                        if ( isset($_GET['month']) ) {
                                            $month = $_GET['month'];
                                            $month_name = date('F');
                                            $year = $_GET['year'];
                                        }else{
                                            $month = date('m');
                                            $month_name = date('F');
                                            $year = date('Y');
                                        }
                                        
                                        ?>
                                    <select id="month"  name="month" style="width: 250px;    padding: 12px 20px;
                                        margin: 8px 0;
                                        display: inline-block;
                                        border: 1px solid #ccc;
                                        border-radius: 4px;
                                        box-sizing: border-box;">
                                        <option selected value="<?= $month?>"><?= $month_name?></option>
                                        <option value='1'>January</option>
                                        <option value='2'>February</option>
                                        <option value='3'>March</option>
                                        <option value='4'>April</option>
                                        <option value='5'>May</option>
                                        <option value='6'>June</option>
                                        <option value='7'>July</option>
                                        <option value='8'>August</option>
                                        <option value='9'>September</option>
                                        <option value='10'>October</option>
                                        <option value='11'>November</option>
                                        <option value='12'>December</option>
                                    </select>
                                    <select id="year" name="Year" style="width: 250px;    padding: 12px 20px;
                                        margin: 8px 0;
                                        display: inline-block;
                                        border: 1px solid #ccc;
                                        border-radius: 4px;
                                        box-sizing: border-box;">
                                        <option selected value="<?= $year?>"><?= $year?></option>
                                        <?php
                                            include ("dbconnect.php");
                                            
                                            $sqli_query="SELECT `Attendace_ID`, `Employee_ID`, `Date`, `Time_IN_AM`, `Time_Out_AM`, `TimeIN_PM`, `TimeOUT_PM` FROM `attendance` WHERE year(`Date`)=YEAR(CURDATE())  group by `Date`  ";
                                            $result=$conn->query($sqli_query);
                                            if ($result->num_rows>0){
                                            while ($row=$result->fetch_assoc()){
                                                $date=date_create($row['Date']);
                                                $date = date_format($date,"Y");
                                                ?>
                                        <option  value="<?php echo $date;?>"><?php  echo $date; ?>   </option>
                                        <?php
                                            }}
                                            ?>
                                    </select>
                                    <input type="text" onkeyup="myFunction()" name="date1"  style="width: 250px;    padding: 12px 20px;
                                        margin: 8px 0;
                                        display: inline-block;
                                        border: 1px solid #ccc;
                                        border-radius: 4px;
                                        box-sizing: border-box;height: 45px;" placeholder="Employee Name">
                                    <button class="btn btn-success"  onclick="submit_data()" type="button" name="display"> <i class="nav-icon fa fa-search"> </button></i> 
                                </form>
                            </div>
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <tr>
                                            <th >Employee ID</th>
                                            <th  data-target='Name'>Name</th>
                                            <!-- <th>Month</th> -->
                                            <th >
                                                Undertime
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        $total_undertime = 0;
                                        $sqli_query="SELECT * FROM `user` inner JOIN employee ON user.employee_ID = employee.employee_ID INNER join attendance on employee.employee_ID = attendance.Employee_ID WHERE user_type!='admin' group by user.employee_ID";
                                        $result=$conn->query($sqli_query);
                                        while ($row=$result->fetch_assoc()){

                                            $employee_ID = $row['employee_ID']; 
                                            if ( isset($_GET['month']) ) {
                                                
                                                
                                            }else{

                                                $month = date('m');
                                                $month_name = date('F');
                                                $year = date('Y');
                                            }
                                    ?>
                                        <tr>
                                            <td><?= $row['employee_ID'] ?></td>
                                            <td><?= $row['FirstName'] ?> <?= $row['LastName'] ?></td>
                                            <!-- <td><?= $month_name ?></td> -->
                                            <td>
                                                <?php 

                                                   $sqli_query2="SELECT * FROM attendance WHERE employee_ID='$employee_ID' ";
                                                    $result2=$conn->query($sqli_query2);
                                                    $count = 0;
                                                    $sum_attandance = 0;
                                                    while ($row2=$result2->fetch_assoc()){
                                                        $count++;
                                                        $attendace_am  = 0;
                                                        $attendace_pm  = 0;
                                                        # morning attendace
                                                        if ( $row2['Time_IN_AM'] != '00:00:00'  ) {
                                                            
                                                            $time_in = $row2['Time_IN_AM']; 
                                                            $time_out = $row2['Time_Out_AM']; 
                                                            $date1 = new DateTime($time_in);
                                                            $date2 = $date1->diff(new DateTime($time_out));
                                                            $attendace_am = $date2->h;
                                                        }

                                                        if ( $row2['TimeIN_PM'] != '00:00:00'  ) {
                                                            
                                                            $time_in = $row2['TimeIN_PM']; 
                                                            $time_out = $row2['TimeOUT_PM']; 
                                                            $date1 = new DateTime($time_in);
                                                            $date2 = $date1->diff(new DateTime($time_out));
                                                            $attendace_pm = $date2->h;
                                                        }

                                                        $total_hours = $attendace_am + $attendace_pm ;
                                                       
                                                        if ( $total_hours > 8 ) {

                                                            $total_hours = 8;
                                                            $late_time = 0;
                                                        }else{

                                                            $total_hours = $total_hours;
                                                            $late_time = 0;
                                                        } 
                                                        

                                                        $sum_attandance += $total_hours;

                                                    }
                                                    //$total_undertime
                                                   
                                                ?>
                                                
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                                </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                    <div class="modal modal-success fade" id="modal-success">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Undertime of <i style="color: #49c5b6" id="id-name"></i><br>
                                        Under &nbsp;<i style="color: #49c5b6" id="id-position"></i>
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <form method='POST' action="addemployee.php">
                                    <table id="example1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                            <tr>
                                                <th>Employee ID</th>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th >
                                                    Undertime
                                                </th>
                                            </tr>
                                        <tbody id="view-details-type" >
                                        </tbody>
                                        <!-- /input-group -->
                                        <!-- /.card-body -->
                                </div>
                            </div>
                            </tr></thead></table>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal -->
            </section>
            <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <?php include("footer.php") ?>
            </footer>
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->
        <!-- jQuery -->
        <script src="../plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- DataTables -->
        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- SlimScroll -->
        <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="../plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="../dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../dist/js/demo.js"></script>
        <!-- page script -->
        <script>
            function myFunction() {
              var input, filter, table, tr, td, i;
              input = document.getElementById("myInput");
              filter = input.value.toUpperCase();
              table = document.getElementById("example1");
              tr = table.getElementsByTagName("tr");
              for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                  if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                  } else {
                    tr[i].style.display = "none";
                  }
                }       
              }
            }
            
            function submit_data() {
                var month = $("#month").val();
                 var year = $("#year").val();
                window.location='absentsummary.php?month='+month+'&year='+year;
            }
             
            
            
            
        </script>
        <script type="text/javascript">
            function add_payment(el)
                {
                    var employee_ID = $(el).attr('employee_ID');
                    var member_name = $(el).attr('member_name');
                    var member_position = $(el).attr('member_position');
                    $("#employee_ID").val(employee_ID);
                    $("#id-name").html(member_name);
                    $("#id-position").html(member_position);
                    $("#modal-success").modal('show');
                         $.ajax({
                            url: 'transaction.php',
                            type: 'POST',
                            data: { payment_details_list :"",employee_ID:employee_ID },
                            success: function (msg) { console.log(msg);
                                $("#view-details-type").html(msg);
                                $("#modal-success").modal('show');
                            }
            
                        });
                   
                }
              
            
            
        </script>
    </body>
</html>