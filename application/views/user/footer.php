    <strong>Copyright &copy; 2018 <a href="">Department of Agriculture</a>.</strong>
    All rights reserved.
  </footer>
        </div>
        <!-- ./wrapper -->
        <!-- jQuery -->
        
        <!-- jQuery UI 1.11.4 -->
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        
        <script type="text/javascript">
            
            
            
            function digitalTime(){
            var date = new Date();
            var h = date.getHours();
            var m = date.getMinutes();
            var s = date.getSeconds();
            
            var session = "AM";
            
            if(h == 0){
            h = 12;
            }
            if(h > 12){
            h = h - 12;
            session = "PM";
            }
            
            
            
            h = (h<10) ? "0" + h : h;
            m = (m<10) ? "0" + m : m;
            s = (s<10) ? "0" + s : s;
            
            
            var time = h + ":" + m + ":" + s + " " + session;
            
            document.getElementById('DigitalClock').innerText = time;
            document.getElementById('DigitalClock').textContent = time;
            
            setTimeout(digitalTime, 1000);
            }
            
            
            digitalTime();
            
        </script>
        <!-- <script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script> -->
  <script src="<?php echo base_url().'assets/libs/jquery/jquery-2.2.4.js'?>"></script>
        <!-- Bootstrap 4 -->
        <script src="<?php echo base_url(); ?>/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

        <!-- Morris.js charts -->
        <script src="<?php echo base_url(); ?>/assets/plugins/morris/morris.min.js"></script>
        <!-- Sparkline -->
        <script src="<?php echo base_url(); ?>/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
        <!-- jvectormap -->
        <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        
        <!-- jQuery Knob Chart -->
        <script src="<?php echo base_url(); ?>/assets/plugins/knob/jquery.knob.js"></script>
        <!-- daterangepicker -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>/assets/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- datepicker -->
        <script src="<?php echo base_url(); ?>/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <!-- Slimscroll -->
        <script src="<?php echo base_url(); ?>/assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>/assets/plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>/assets/dist/js/adminlte.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <!-- AdminLTE for demo purposes -->
        

        
    </body>
</html>