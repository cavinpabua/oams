<?php
    session_start();
    if ( isset($_POST['undertime_report']) ) {

    	$daterange = $_POST['date'];
	    $datef = explode(" -",$daterange);
		$date_from = trim($datef[0]);
		$date_to = trim($datef[1]);
		$myDateFROM = date("Y-m-d", strtotime($date_from));
		$myDateTO = date("Y-m-d", strtotime($date_to));
		$_SESSION['undertime_report'] = $daterange;
		$_SESSION['undertime_report_from'] = $myDateFROM;
		$_SESSION['undertime_report_to'] = $myDateTO;
    }

    if ( isset($_POST['workingdays']) ) {

    	$daterange = $_POST['date'];
	    $datef = explode(" -",$daterange);
		$date_from = trim($datef[0]);
		$date_to = trim($datef[1]);
		$myDateFROM = date("Y-m-d", strtotime($date_from));
		$myDateTO = date("Y-m-d", strtotime($date_to));
		$_SESSION['workingdays_report'] = $daterange;
		$_SESSION['workingdays_report_from'] = $myDateFROM;
		$_SESSION['workingdays_report_to'] = $myDateTO;
    }

    if ( isset($_POST['late']) ) {

    	$daterange = $_POST['date'];
	    $datef = explode(" -",$daterange);
		$date_from = trim($datef[0]);
		$date_to = trim($datef[1]);
		$myDateFROM = date("Y-m-d", strtotime($date_from));
		$myDateTO = date("Y-m-d", strtotime($date_to));
		$_SESSION['late_report'] = $daterange;
		$_SESSION['late_report_from'] = $myDateFROM;
		$_SESSION['late_report_to'] = $myDateTO;
    }
    

?>