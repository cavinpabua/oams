
                <?php include("topnav.php") ?>
            </nav>
            <!-- /.navbar -->
            <!-- Main Sidebar Container -->
            <aside class="main-sidebar elevation-4 sidebar-light-success">
                <?php include("sidenav.php") ?>
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0 text-dark">Dashboard 
                                <?php if(isset($_SESSION['fail-add'])){?>
                                  <span style="color:red;font-size: 20px;padding-left: 20px;"><?php echo $_SESSION['fail-add']; ?></span>
                                <?php } ?>
                                <?php if(isset($_SESSION['success-add'])){?>
                                  <span style="color:green;font-size: 20px;padding-left: 20px;"><?php echo $_SESSION['success-add']; ?></span>
                                <?php } ?>
                                 </h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
                                    <li class="breadcrumb-item active">Dashboard</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->
                <!-- Main content -->
                <section class="content">
                    <?php if($_SESSION['user_type']=="admin"){ ?>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-12 specialbox" onclick="location.href='<?php echo site_url('Welcome/employee'); ?>'">
                                <div class="info-box">
                                    <span class="info-box-icon bg-success"><i class="fa fa-users"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Employees</span>
                                        
                                        <span class="info-box-number">
                                            <?php
                                            $this->load->model('login_model');
                                            $empCount = $this->login_model->employeeCount();
                                            echo ($empCount->row()->empCount);


                                            ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-12 specialbox" onclick="location.href='<?php echo site_url('Welcome/leaveReqList'); ?>'">
                                <div class="info-box">
                                    <span class="info-box-icon bg-success"><i class="fas fa-flag"></i></span>
                                    <div class="info-box-content"> 
                                        <span class="info-box-text">Leave Request</span>
                                        <span class="info-box-number">
                                            <?php
                                            $this->load->model('login_model');
                                            $leaveCount = $this->login_model->LeaveCount();
                                            echo $leaveCount->row()->leaveCount;


                                            ?>

                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-12 specialbox" onclick="location.href='<?php echo site_url('Welcome/leaveReqList'); ?>'">
                                <div class="info-box">
                                    <span class="info-box-icon bg-success"><i class="fas fa-walking"></i></span>
                                    <div class="info-box-content">
                                        
                                        <span class="info-box-text">On Leave Employees</span>
                                        <span class="info-box-number">
                                            <?php
                                            $this->load->model('login_model');
                                            $OnLeaveCount = $this->login_model->OnLeaveCount();
                                            echo $OnLeaveCount->row()->OnLeaveCount;


                                            ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <!-- /.col -->

                    <div class="card card-success">
                        <div class="card-header">
                            <?php if($_SESSION['user_type']=="admin"){ ?>
                            <h3 class="card-title">Attendance List</h3>
                            <?php }else{ ?>
                            <h3 class="card-title">Your Attendance List</h3>
                            <?php } ?>
                        </div>
                        <div class="row">

                            <div class="card-body">
                                
                                <div style="text-align:center">
                                    <b><div id="DigitalClock" class="clockDetail"></div></b>
                                </div>
                                    <table id="myTable" class="table table-bordered table-hover " >
                                        <thead>
                                            <tr class="text-center">
                                                <th>Bio Name</th>
                                                <th>Time In</th>
                                                <th>Time Out</th>
                                                <th>Date</th>
                                                <th>Total Work hour</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($attendanceList as $k): ?>

                                            <tr class="text-center">
                                                
                                               
                                                <td><?php echo ucwords($k->Name); ?></td>
                                                <td><?php echo date("g:i A", strtotime($k->time_in)); ?></td>
                                                <td ><?php echo date("g:i A", strtotime($k->time_out)); ?></td>
                                                <td><?php echo $k->attendance_date; ?></td>
                                                <td><?php 
                                                    $starttimestamp = strtotime($k->time_in);
                                                    $endtimestamp = strtotime($k->time_out);
                                                    $totalHour = (abs($endtimestamp - $starttimestamp)/3600)-1;
                                                    echo number_format((float)$totalHour, 2, '.', ''); ?>
                                                </td>


                                            </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                     
                                    </table>
                                    <?php if($_SESSION['user_type']=="admin"){ ?>
                                    <br>
                                    <form method="post" action="<?php echo site_url('Welcome/uploadAttendance'); ?>" enctype="multipart/form-data">
                                    
                                     <div class="input-group col-md-4" style="float: left;">
                                        
                                  <div class="input-group-prepend ">

                                    <span class="input-group-text " id="inputGroupFileAddon01">Import Data</span>
                                  </div>
                                  <div class="custom-file">
                                    <input type="file" name="userFile" class="custom-file-input" id="inputGroupFile01"
                                      aria-describedby="inputGroupFileAddon01">
                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                  </div>
                                  
                                 
                                </div>
                                <button class="btn btn-secondary" name="submit" value="upload" type="Submit" >Import</button>
                                 </form>
                                <?php } ?>
                                </div>

                                
                            </div>
                          
                        </div>

                    </div>
                </section>
            </div>

            <footer class="main-footer">
                <?php include("footer.php") ?>
          <script type="text/javascript">
            $(document).ready( function () {
                $('#myTable').DataTable(
                    {
                    "lengthMenu": [5,10,20, 40, 60, 80, 100],
                      "ordering": false
                    }
                    );
                });
        </script>