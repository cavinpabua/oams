<?php 
    include 'session.php';
    include ("dbconnect.php");
    ?>
<!DOCTYPE html>
<html>
    <head>
        <style>
            .dot1 {
            height: 10px;
            width: 10px;
            background-color: green;
            border-radius: 50%;
            display: inline-block;
            }
            .dot2 {
            height: 10px;
            width: 10px;
            background-color: red;
            border-radius: 50%;
            display: inline-block;
            }
            .dot3 {
            height: 10px;
            width: 10px;
            background-color: pink;
            border-radius: 50%;
            display: inline-block;
            }
            #scrolltable { margin-top: 20px; height: 200px; overflow: auto; }
            #scrolltable th div { position: absolute; margin-top: -20px; }
        </style>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>HRISDA | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="../https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../dist/css/adminlte.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="../plugins/morris/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="../https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand border-bottom navbar-dark bg-success">
                <!-- Left navbar links -->
                <?php include("topnav.php") ?>
            </nav>
            <!-- /.navbar -->
            <!-- Main Sidebar Container -->
            <aside class="main-sidebar elevation-4 sidebar-light-success">
                <?php include("sidenav.php") ?>
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                            </div>
                            <div class="col-sm-6">
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-success">
                                <div class="card-header">
                                    <h3 class="card-title">Attendance Report</h3>
                                </div>
                                <div class="form-group" style='border: black;'>
                                   
                                </div>
                                <div class="card-body">
                                    <?php
                                        $month = $_GET['month'];
                                        $year = $_GET['year'];
                                        $employee_ID = $_GET['employee_ID'];


                                    ?>
                                      <?php
                                                $sqli_query="SELECT * FROM employee where employee_ID=$employee_ID ";
                                                $result=$conn->query($sqli_query);
                                                while ($row=$result->fetch_assoc()){
                                                    ?>
                                    <button class="btn btn-success"  onclick="printdata()" type="button" name="display">Print  </button> 
                                               
                                    <div id='print'> 
                                    <table id="example1" class="table table-bordered table-hover" border="1">
                                            <thead>
                                                 <tr>
                                                  <h1><center>Report for the Month of 
                                                    <?php $monthn = $_GET['month'];
                                                  echo date('F',mktime(0,0,0,$monthn,10)); ?>&nbsp;<?= $_GET['year']; ?></h1>
                                                  <h3><center><?=  $row['FirstName'] ?>&nbsp;<?=  $row['LastName'] ?>&nbsp;<?=  $row['Position'] ?>&nbsp;<?=  $row['Division'] ?>&nbsp;Department</h3></center>
                                                    <th >Day</th>
                                                    <th colspan="2">AM</th>
                                                    <th colspan="2">PM</th>
                                                    <th>Working Hour</th>
                                                    <th>Total Hour</th>
                                                    <th>Status</th>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <th align="center">Time In</th>
                                                    <th align="center">Time Out</th>
                                                    <th align="center">Time In</th>
                                                    <th align="center">Time Out</th>
                                                    <th colspan="3"></th> 

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                    $query_date = $year.'-'.$month;
                                                    $date = new DateTime($query_date);
                                                    //First day of month
                                                    $date->modify('first day of this month');
                                                    $firstday= $date->format('Y-m-d');
                                                    //Last day of month
                                                    $date->modify('last day of this month');
                                                    $lastday= $date->format('Y-m-d');
                                                    $date->modify('last day of 1 month');
                                                    $month_days =   $date->format('d')  ;
                                                     #for leave 
                                                    $range = 0;
                                                    $date_leave = array();
                                                    $sqli_query3="SELECT * FROM employee_leaverequest WHERE employee_ID='$employee_ID' ";
                                                    $result3=$conn->query($sqli_query3);
                                                    while ($row3=$result3->fetch_assoc()){
                                                        $time_in = $row3['Date_of_LeaveFrom']; 
                                                        $time_out = $row3['Date_of_LeaveTo']; 
                                                        $date1 = new DateTime($time_in);
                                                        $date2 = $date1->diff(new DateTime($time_out));
                                                        $range = $date2->d;
                                                    }

                                                    if ( $range > 0 ) {
                                                        $range = $range + 1;
                                                       for ($i=0; $i <$range ; $i++) { 
                                                            $date=date_create($time_in);
                                                            date_add($date,date_interval_create_from_date_string("".$i." days"));
                                                            $date_leave[] =  date_format($date,"Y-m-d");
                                                        }
                                                        
                                                    }

                                                        

                                                    for ($i=1; $i < $month_days ; $i++) { 

                                                ?>
                                                <tr>
                                                    <?php 
                                                        $status =  'Absent';
                                                        $status_label =  '';
                                                        $Time_IN_AM = "";
                                                        $Time_OUT_AM = "";
                                                        $TimeIN_PM = "";
                                                        $TimeOUT_PM = "";
                                                        $attendance_am_data = "";
                                                        $date_format = $year.'-'.$month.'-'.$i;
                                                        $date_format2 = $year.'-'.$month.'-0'.$i;
                                                        $total_hours = 0;
                                                        $totalmonth = 0;
                                                        $total_minutes = 0;
                                                        $sqli_query2="SELECT * FROM attendance WHERE employee_ID='$employee_ID' AND  date(attendance.Date)='$date_format' ";
                                                        $result2=$conn->query($sqli_query2);
                                                        while ($row2=$result2->fetch_assoc()){
                                                            $Time_IN_AM = $row2['Time_IN_AM'];
                                                            $Time_OUT_AM = $row2['Time_Out_AM'];
                                                            $TimeIN_PM = $row2['TimeIN_PM'];
                                                            $TimeOUT_PM = $row2['TimeOUT_PM'];
                                                            if($Time_IN_AM == "00:00:00"){
                                                                 $Time_IN_AM = "";
                                                            }
                                                            if($Time_OUT_AM == "00:00:00"){
                                                                 $Time_OUT_AM = "";
                                                            }
                                                            if($TimeIN_PM == "00:00:00"){
                                                                 $TimeIN_PM = "";
                                                            }
                                                            if($TimeOUT_PM == "00:00:00"){
                                                                 $TimeOUT_PM = "";
                                                            }
                                                            $status =  'Present';
                                                            $status_label = '';

                                                        $attendace_am  = 0;
                                                        $attendace_pm  = 0;
                                                        # morning attendace
                                                        if ( $row2['Time_IN_AM'] != '00:00:00'  ) {
                                                            
                                                            $time_in = $row2['Time_IN_AM']; 
                                                            $time_out = $Time_OUT_AM ; //'12:00'
                                                           
                                                            if ( $time_out >=12 ) {

                                                                $time_out = '12:00';
                                                            } 
                                                            $date1 = new DateTime($time_in);
                                                            $date2 = $date1->diff(new DateTime($time_out));
                                                            $attendace_am = $date2->h;
                                                            $attendace_am_minutes = $date2->i;
                                                        }

                                                        if ( $row2['TimeIN_PM'] != '00:00:00'  ) {
                                                            
                                                            $time_in = $row2['TimeIN_PM']; 
                                                            $time_out = $row2['TimeOUT_PM']; 
                                                            if ( $time_in <= 13 ) {

                                                                $time_in = '13:00';
                                                            }  
                                                            $date1 = new DateTime($time_in);
                                                            $date2 = $date1->diff(new DateTime($time_out));
                                                            $attendace_pm = $date2->h;
                                                            $attendace_pm_minutes = $date2->i;

                                                        }   

                                                        $total_hours = $attendace_am + $attendace_pm ;
                                                        $total_minutes = $attendace_am_minutes + $attendace_pm_minutes ;
                                                        $total_minutes =  round($total_minutes/60*10); 
                                                       
                                                        

                                                        // if ( $total_hours > 8 ) {

                                                        //     $total_hours = 8;
                                                        // }else{

                                                        //     $total_hours = $total_hours;
                                                        // }



                                                        }

                                                        if (in_array($date_format2, $date_leave))
                                                        {
                                                            $status_label =  '';
                                                            $status =  'On Leave';
                                                        }

                                                    ?>

                                                    <td ><?= $i ?></td>
                                                    <td align="center"><?= $Time_IN_AM ?></td>
                                                    <td align="center"><?= $Time_OUT_AM ?></td>
                                                    <td align="center"><?= $TimeIN_PM ?></td>
                                                    <td align="center"><?= $TimeOUT_PM ?></td>
                                                    <td align="center">
                                                        <?= $total_hours ?>
                                                        <?php
                                                            if ( $total_minutes !=0 ) {
                                                                 
                                                                echo '.'.$total_minutes.'&nbsp; hrs';
                                                            }else{
                                                                echo "&nbsp; hrs";
                                                            }
                                                        ?>
                                                            
                                                    </td> <td align="center">
                                                        <?= $total_hours ?>
                                                        <?php
                                                            if ( $total_minutes !=0 ) {
                                                                 
                                                                echo '.'.$total_minutes.'&nbsp; hrs';
                                                            }else{
                                                                echo "&nbsp; hrs";
                                                            }
                                                        ?>
                                                            
                                                    </td>
                                                    <td align="center"><?= $status ?></td>
                                                    

                                                </tr>
                                               <?php }}?>
                                               <tr><td ></td>
                                                <td ></td>
                                                <td ></td>
                                                <td ></td>
                                                <td ></td>
                                                <td ></td>
                                                <td ></td>
                                               </tr>   

                                            </tbody>
                                            
                                            

                                    </table>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        </section>
        <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <?php include("footer.php") ?>
        </footer>
        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->
        <!-- jQuery -->
        <script src="../plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- DataTables -->
        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- SlimScroll -->
        <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="../plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="../dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../dist/js/demo.js"></script>
        <!-- page script -->
        <script>
            function myFunction() {
              var input, filter, table, tr, td, i;
              input = document.getElementById("myInput");
              filter = input.value.toUpperCase();
              table = document.getElementById("example1");
              tr = table.getElementsByTagName("tr");
              for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                  if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                  } else {
                    tr[i].style.display = "none";
                  }
                }       
              }
            }
            
        </script>
        <script type="text/javascript">
  function printdata(){
    var WinPrint = document.getElementById('print');
    newWin = window.open('');
    newWin.document.write(WinPrint.outerHTML);
    newWin.print();
    newWin.close();
    //var prtContent=document.getElementById('poprint');
    //var WinPrint = window.open('',''.'left=0, top=0, width=800, height=900, toolbar=0, scrollbars=0,status=0');
    //WinPrint = document.write(prtContent.innerHTML);
    //WinPrint = document.close();
    //WinPrint.focus();
    //WinPrint.print();
    //WinPrint.close();
    } 
    </script>
    </body>
</html>