
    <?php include("topnav.php") ?>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4 sidebar-light-success">
    <?php include("sidenav.php")?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Personal Details</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
         <form method="post" action="<?php echo site_url('Welcome/updateOwn'); ?>">
            <div class="card-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-4">
                    <label for="exampleInputEmail1">Employee ID</label>
                      <div class="form-group">
                        <div class="input-group-prepend">
                           </div>
                             <input type="text" class="form-control" placeholder="" value="<?php echo $employee_info->row()->employee_ID ?>" readonly>
                                </div>
                                  <!-- /input-group -->
                                     </div>
                                     <div class="col-lg-4">
                     <label for="exampleInputEmail1">First Name</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="text" class="form-control" name="FirstName" placeholder="" value="<?php echo $employee_info->row()->FirstName ?>" >
                              </div>
                                <!-- /input-group -->
                                  </div>
                                  <div class="col-lg-4">
                     <label for="exampleInputEmail1">Middle Name</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="text" class="form-control" name="MiddleName" placeholder="" value="<?php echo $employee_info->row()->MiddleName ?>" >
                              </div>
                                <!-- /input-group -->
                                  </div>
                                  <div class="col-lg-4">
                     <label for="exampleInputEmail1">Last Name</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="text" class="form-control" name="LastName" placeholder="" value="<?php echo $employee_info->row()->LastName ?>" >
                              </div>
                                <!-- /input-group -->
                                  </div>
                <div class="col-lg-4">
                     <label for="exampleInputEmail1">Bio Name</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="text" class="form-control" name="BioName" placeholder="" value="<?php echo $employee_info->row()->BioName ?>" >
                              </div>
                                <!-- /input-group -->
                                  </div>
                                        <!-- /.col-lg-6 -->
                  <div class="col-lg-4">
                     <label for="exampleInputEmail1">Date Hired</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="date" class="form-control" placeholder="" name="DateHired" value="<?php echo $employee_info->row()->DateHired ?>" readonly>
                              </div>
                                <!-- /input-group -->
                                  </div>
                                     <!-- /.col-lg-6 -->
                                      
               

                <div class="col-lg-4">
                  <label for="exampleInputEmail1">Contact number</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="text" class="form-control" name="ContactNumber" value="<?php echo $employee_info->row()->ContactNumber ?>" >
                              </div>
                                <!-- /input-group -->
                                  </div>
                                     <!-- /.col-lg-6 -->

               <div class="col-lg-4">
                  <label for="exampleInputEmail1">Position</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="text" class="form-control" placeholder="" name="Position" value="<?php echo $employee_info->row()->Position ?>" readonly>
                              </div>
                                <!-- /input-group -->
                                  </div>
                                     <!-- /.col-lg-6 -->

                <div class="col-lg-4">
                  <label for="exampleInputEmail1">Salary Grade</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="text" class="form-control" placeholder="" name="Salary" value="<?php echo $employee_info->row()->Salary ?>" readonly>
                              </div>
                                <!-- /input-group -->
                                  </div>
                                     <!-- /.col-lg-6 -->

                <div class="col-lg-4">
                  <label for="exampleInputEmail1">Division Assigned</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="text" class="form-control" placeholder="" name="Division" value="<?php echo $employee_info->row()->Division ?>" readonly>
                              </div>
                                <!-- /input-group -->
                                  </div>
                                     <!-- /.col-lg-6 -->

                <div class="col-lg-4">
                  <label for="exampleInputEmail1">Previous work experience</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="text" class="form-control" placeholder="" name="PreviousWork" value="<?php echo $employee_info->row()->PreviousWork ?>" readonly>
                              </div>
                                <!-- /input-group -->
                                  </div>
                                     <!-- /.col-lg-6 -->
                                    
               <div class="col-lg-4">
                  <label for="exampleInputEmail1">Birthdate</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="Date" class="form-control" name="Birthdate" value="<?php echo $employee_info->row()->Birthdate ?>" >
                              </div>
                                <!-- /input-group -->
                                  </div>
                                     <!-- /.col-lg-6 -->
                                      
              <div class="col-lg-4">
                <label for="">Marital Status</label>
                <div class="form-group">
                  
                   <select class="form-control select2" name="MaritalStatus" style="width: 100%;" required>
                    <?php if($employee_info->row()->MaritalStatus=="Single"){ ?>
                       <option value="Single" selected>Single</option>
                       <option value="Widowed">Widowed</option>
                       <option value="Married">Married</option>
                    <?php }elseif ($employee_info->row()->MaritalStatus=="Widowed"){?>
                       <option value="Single">Single</option>
                       <option value="Widowed" selected>Widowed</option>
                       <option value="Married">Married</option>
                    <?php }else{ ?>
                       <option value="Single">Single</option>
                       <option value="Widowed">Widowed</option>
                       <option value="Married" selected>Married</option>
                    <?php } ?>
                   </select> 
                </div>
                </div>  
                                                    <!-- /.col-lg-6 -->                                                       
                <div class="col-lg-4">
                  <label for="">Gender</label>
                  <div class="form-group">
                     <select class="form-control  select2"  name="Gender" style="width: 100%;" required>
                      <?php if($employee_info->row()->Gender=="Female"){ ?>
                      <option value="female" selected>Female</option>
                      <option value="male">Male</option>
                    <?php }else{ ?>
                      <option value="female" >Female</option>
                      <option value="male" selected>Male</option>
                    <?php } ?>
                     </select>
                  </div>
                </div>
                                              <!-- /.col-lg-6 -->

                <div class="col-lg-4">
                  <label for="">Employment Status</label>
                  <div class="form-group">
                     <input type="text" class="form-control" name="EmploymentStatus" value="<?php echo $employee_info->row()->EmploymentStatus ?>" readonly >
                     
                  </div>
                </div>
                                   <div class="col-lg-4">
                  <label for="exampleInputEmail1">Address</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="text" class="form-control" placeholder="" name="Address" value="<?php echo $employee_info->row()->Address ?>">
                              </div>
                                <!-- /input-group -->
                                  </div>
                  <div class="col-lg-4">
                    <label for="exampleInputEmail1">Email</label>
                    <div class="form-group">
                    <div class="input-group-prepend">
                    </div>
                    <input type="text" class="form-control" placeholder="" name="email" value="<?php echo $employee_info->row()->email ?>">
                    </div>
                  </div><br>
           
                <!-- /.card-body -->  
                
              
            </div>
                  <button type="submit" name="saveOwn" class="btn btn-success">Save</button>
                  </form>
          </div>

          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
  <?php include("footer.php") ?>
