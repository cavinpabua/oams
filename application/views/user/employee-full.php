
    <?php include("topnav.php") ?>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4 sidebar-light-success">
    <?php include("sidenav.php") ?>
  </aside>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">
              <?php if(isset($_SESSION['success-reset'])){?>
              <span style="color:green;"><?php echo $_SESSION['success-reset']; ?></span>
            <?php } ?>
              <?php if(isset($_SESSION['success-disable'])){?>
              <span style="color:red;"><?php echo $_SESSION['success-disable']; ?></span>
            <?php } ?>
            <?php if(isset($_SESSION['success-enable'])){?>
              <span style="color:green;"><?php echo $_SESSION['success-enable']; ?></span>
            <?php } ?>
            </h1>
          </div>
          <div class="col-sm-6">
           
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->

            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Complete Details</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
       
            <div class="card-body">
              <div class="form-group">
            <div class="row">
              <div class="col-lg-6">
                <h2>Personal Details</h2>
                <label>Employee ID Number: <?php echo $employee_info->row()->employee_ID; ?></label><br>
                <label>First Name: <?php echo $employee_info->row()->FirstName; ?></label><br>
                <label>Middle Name: <?php echo $employee_info->row()->MiddleName; ?></label><br>
                <label>Last Name: <?php echo $employee_info->row()->LastName; ?></label><br>
                <label>Gender: <?php echo $employee_info->row()->Gender; ?></label><br>
                <label>Address: <?php echo $employee_info->row()->Address; ?></label><br>
                <label>Contact Number: <?php echo $employee_info->row()->ContactNumber; ?></label><br>
                <label>Date Of Birth: <?php echo date("F j, Y", strtotime($employee_info->row()->Birthdate)); ?></label><br>
                <label>Email Address: <?php echo $employee_info->row()->email; ?></label><br>
                <label>Marital Status: <?php echo $employee_info->row()->MaritalStatus; ?></label><br>
                <br>
                <a class="btn btn-default" href="<?php echo site_url('Welcome/employee'); ?>">Back</a>
              </div>
              <div class="col-lg-6">
                <h2>Employment Details</h2>
                
                <label>Date Hired: <?php echo date("F j, Y", strtotime($employee_info->row()->DateHired)); ?></label><br>
                <label>Previous Work: <?php echo $employee_info->row()->PreviousWork; ?></label><br>
                <label>Sick Leave Credits: <?php echo $employee_info->row()->sick_leave_credits; ?></label><br>
                <label>Vacation Leave Credits: <?php echo $employee_info->row()->vacation_leave_credits; ?></label><br>
                <label>Salary Grade: <?php echo $employee_info->row()->Salary; ?></label><br>
                <label>Position: <?php echo ucwords($employee_info->row()->Position); ?></label><br>
                <label>Division: 
                  <?php  foreach ($division as $x): ?>
                  <?php if($x->division_id==$employee_info->row()->Division){ echo $x->division_name; } ?>
                    
                  <?php endforeach ?>
                </label>
                <br>
                <label>Employment Status: <?php echo $employee_info->row()->EmploymentStatus; ?></label><br>
                <label>Office Assign: <?php echo $employee_info->row()->OfficeAssign; ?></label><br>
                <label>User Status: <?php echo ucwords($employee_info->row()->status); ?></label><br>
                <br>
                <a href="#" data-toggle="modal" data-target="#exampleModal3">Edit Details</a><br>
                <?php if($employee_info->row()->status=="active"){ ?>
                <a href="#" style="color:red;" data-toggle="modal" data-target="#exampleModal2">
                  
                Disable User Login</a><br>
                  <?php }else{ ?>
                    <a href="#" style="color:green;" data-toggle="modal" data-target="#exampleModal2">
                  
                Enable User Login</a><br>
                
                  <?php } ?>
              
                <a href="#" style="color:red;" data-toggle="modal" data-target="#exampleModal">Reset Password</a> <span>(Resets Password automatically to default.)</span><br>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Reset Password </h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      Reset password of <?php echo $employee_info->row()->FirstName." ".$employee_info->row()->LastName; ?>?
                    </div>
                    <div class="modal-footer">
                      <a class="btn btn-danger" href="<?php echo base_url();?>Welcome/resetPass/<?php echo $employee_info->row()->employee_ID; ?>" >Reset</a>
                      <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                      
                    </div>
                  </div>
                </div>
              </div>

              <!-- MODAL 2 -->
              <?php if($employee_info->row()->status=="active"){ ?>
              <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Disable User </h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      Are you sure you want to disable <?php echo $employee_info->row()->FirstName." ".$employee_info->row()->LastName; ?>?
                    </div>
                    <div class="modal-footer">
                      <a class="btn btn-danger" href="<?php echo base_url();?>Welcome/disableUser/<?php echo $employee_info->row()->employee_ID; ?>" >Disable</a>
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      
                    </div>
                  </div>
                </div>
              </div>
              <?php }else{ ?>
              <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Enable User </h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      Are you sure you want to enable <?php echo $employee_info->row()->FirstName." ".$employee_info->row()->LastName; ?>?
                    </div>
                    <div class="modal-footer">
                      <a class="btn btn-success" href="<?php echo base_url();?>Welcome/enableUser/<?php echo $employee_info->row()->employee_ID; ?>" >Enable</a>
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>
              <!-- EDIT DETAILS -->
              <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Details </h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                     
                    
                    <form method="POST" action="<?php echo base_url();?>Welcome/updateEmp/<?php echo $employee_info->row()->employee_ID; ?>">
                      <div class="row">
                      <div class="col-lg-4">
                      <label for="">First Name</label>
                      <div class="form-group">
                         <input type="text" class="form-control" name="FirstName" value="<?php echo $employee_info->row()->FirstName ?>" required>
                      </div>
                      </div>
                      
                      <div class="col-lg-4">
                      <label for="">Middle Name</label>
                      <div class="form-group">
                         <input type="text" class="form-control" name="MiddleName" value="<?php echo $employee_info->row()->MiddleName ?>" >
                      </div>
                      </div>
                      <div class="col-lg-4">
                      <label for="">Last Name</label>
                      <div class="form-group">
                         <input type="text" class="form-control" name="LastName" value="<?php echo $employee_info->row()->LastName ?>" >
                      </div>
                      </div>
                      </div>
                      <div class="row">
                      <div class="col-lg-4">
                      <label for="">Gender</label>
                      <div class="form-group">
                         <select class="form-control  select2"  name="Gender" style="width: 100%;" >
                          <?php if($employee_info->row()->Gender=="Female"){ ?>
                          <option value="female" selected>female</option>
                          <option value="male">male</option>
                        <?php }else{ ?>
                          <option value="female" >female</option>
                          <option value="male" selected>male</option>
                        <?php } ?>
                         </select>
                      </div>
                      </div>
                      <div class="col-lg-4">
                      <label for="">Address</label>
                      <div class="form-group">
                         <input type="text" class="form-control" name="Address" value="<?php echo $employee_info->row()->Address ?>" >
                      </div>
                      </div>
                      <div class="col-lg-4">
                      <label for="">Email</label>
                      <div class="form-group">
                         <input type="email" class="form-control" name="email" value="<?php echo $employee_info->row()->email ?>" >
                      </div>
                      </div>
                      </div>
                      <div class="row">
                      <div class="col-lg-4">
                      <label for="">Contact Number</label>
                      <div class="form-group">
                         <input type="text" class="form-control" name="ContactNumber" value="<?php echo $employee_info->row()->ContactNumber ?>" >
                      </div>
                      </div>
                      <div class="col-lg-4">
                      <label for="">Birthdate</label>
                      <div class="form-group">
                         <input type="date" class="form-control" name="Birthdate" value="<?php echo $employee_info->row()->Birthdate ?>" >
                      </div>
                      </div>
                      <div class="col-lg-4">
                      <label for="">Marital Status</label>
                      <div class="form-group">
                        
                         <select class="form-control select2" name="MaritalStatus" style="width: 100%;" >
                          <?php if($employee_info->row()->MaritalStatus=="Single"){ ?>
                             <option value="Single" selected>Single</option>
                             <option value="Widowed">Widowed</option>
                             <option value="Married">Married</option>
                          <?php }elseif ($employee_info->row()->MaritalStatus=="Widowed"){?>
                             <option value="Single">Single</option>
                             <option value="Widowed" selected>Widowed</option>
                             <option value="Married">Married</option>
                          <?php }else{ ?>
                             <option value="Single">Single</option>
                             <option value="Widowed">Widowed</option>
                             <option value="Married" selected>Married</option>
                          <?php } ?>
                         </select> 
                      </div>
                      </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-4">
                      <label for="">Date hired</label>
                      <div class="form-group">
                         <input type="date" class="form-control" name="DateHired" value="<?php echo $employee_info->row()->DateHired ?>" >
                      </div>
                      </div>
                      <div class="col-lg-4">
                      <label for="">Previous work experience</label>
                      <div class="form-group">
                         <input type="text" class="form-control" name="PreviousWork" value="<?php echo $employee_info->row()->PreviousWork ?>" >
                      </div>
                      </div>
                      <div class="col-lg-4">
                      <label for="">Salary Grade</label>
                      <div class="form-group">
                         <input type="text" class="form-control" name="Salary" value="<?php echo $employee_info->row()->Salary ?>" >
                      </div>
                      </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-4">
                      <label for="">Position</label>
                      <div class="form-group">
                        
                         <select class="form-control select2" name="Position" style="width: 100%;" >
                          <?php if($employee_info->row()->Position=="employee"){ ?>
                            <option value="employee" selected>Employee</option>
                            <option value="Supervisor">Supervisor</option>
                            <option value="HumanResource">Human Resource</option>
                            <option value="Head">Head</option>
                          <?php }elseif ($employee_info->row()->Position=="Supervisor"){?>
                            <option value="employee">Employee</option>
                            <option value="Supervisor" selected>Supervisor</option>
                            <option value="HumanResource">Human Resource</option>
                            <option value="Head">Head</option>
                          <?php }elseif ($employee_info->row()->Position=="HumanResource"){?>
                            <option value="employee">Employee</option>
                            <option value="Supervisor">Supervisor</option>
                            <option value="HumanResource" selected>Human Resource</option>
                            <option value="Head">Head</option>
                          <?php }else{ ?>
                            <option value="employee">Employee</option>
                            <option value="Supervisor">Supervisor</option>
                            <option value="HumanResource">Human Resource</option>
                            <option value="Head" selected>Head</option>
                          <?php } ?>
                         </select>
                      </div>
                      </div>
                      <div class="col-lg-4">
                      <label for="">Division</label>
                      <div class="form-group">
                         
                         <select class="form-control select2" name="Division" style="width: 100%;" >
                          <?php  foreach ($division as $x): ?>
                          <?php if($x->division_id==$employee_info->row()->Division){ ?>
                            <option value="<?php echo $x->division_id ?>" selected><?php echo $x->division_name ?></option>
                          <?php } ?>
                          <option value="<?php echo $x->division_id ?>"><?php echo $x->division_name ?></option>
                          <?php endforeach ?>
                         </select> 
                      </div>
                      </div>
                      <div class="col-lg-4">
                      <label for="">Employment Status</label>
                      <div class="form-group">
                         
                         <select class="form-control select2" name="EmploymentStatus" style="width: 100%;" >
                          <?php if($employee_info->row()->EmploymentStatus=="Regular"){ ?>
                             <option value="Regular" selected>Regular</option>
                             <option value="HR">HR</option>
                          <?php }else{ ?>
                            <option value="Regular">Regular</option>
                             <option value="HR" selected>HR</option>
                          <?php } ?>
                         </select> 
                      </div>
                      </div>
                      <div class="col-lg-4">
                      <label for="">Office Assign</label>
                      <div class="form-group">
                         <input type="text" class="form-control" name="OfficeAssign" value="<?php echo $employee_info->row()->OfficeAssign ?>" >
                      </div>
                      </div>
                      <div class="col-lg-4">
                      <label for="">Sick Leave Credits</label>
                      <div class="form-group">
                         <input type="number" class="form-control" name="sickleavecredits" value="<?php echo $employee_info->row()->sick_leave_credits ?>" >
                      </div>
                      </div>
                      <div class="col-lg-4">
                      <label for="">Vacation Leave Credits</label>
                      <div class="form-group">
                         <input type="number" class="form-control" name="vacationleavecredits" value="<?php echo $employee_info->row()->vacation_leave_credits ?>" >
                      </div>
                      </div>
                      </div>
                      
                    </div>
                    <div class="modal-footer">
                      
                      <button type="submit" class="submit btn btn-success btn-shadow btn-gradient" name='saveEmp'>Save</button>
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      
                    </div>
                    </form>
                  </div>
                </div>
              </div>
              
              </div>
                <br>	
                  
                  
                
            </div>
            </div>
            </div>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <!-- /.content-wrapper -->
  <footer class="main-footer">
	<?php include("footer.php") ?>

