
                <?php include("topnav.php") ?>
            </nav>
            <!-- /.navbar -->
            <!-- Main Sidebar Container -->
            <aside class="main-sidebar elevation-4 sidebar-light-success">
                <?php include("sidenav.php") ?>
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                            </div>
                            <div class="col-sm-6">
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-12">
                            <!-- Custom Tabs -->
                            <div class="card">
                                <div class="card-header d-flex p-0">
                                    <h3 class="card-title p-3">Late  Reports Form</h3>
                                    <ul class="nav nav-pills ml-auto p-2">
                                        
                                    </ul>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_1">
                                            <!-- Date range -->
                                            <form method="post" id="frm">
                                            <div class="form-group" style="width: 300px;">
                                              <select class="form-control" name="latetime">
                                                <option selected disabled>Select Late Time</option>
                                                <option value="08:00:00">8:00 AM</option>
                                                <option value="08:30:00">8:30 AM</option>
                                                <option value="09:00:00">9:00 AM</option>
                                                <option value="09:30:00">9:30 AM</option>
                                                <option value="10:00:00">10:00 AM</option>
                                              </select>
                                            </div>
                                            <div class="form-group" style="width: 300px">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                        </span>
                                                    </div>
                                                    <input class="form-control" placeholder="Select date range here"  type="text"  id="reportrange" autocomplete="off"/>
                                                    <div  title="Search Now" class="input-group-append" style="cursor: pointer;" onclick="search_now()" >
                                                        <span onclick="onSelectChange()" class="input-group-text"><i class="fa fa-search"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.input group -->
                                            <input type="text" name="selectdate" id="hiddenSelect" hidden>
                                            <!-- /.input group -->
                                            </form>

                                             <table id="dataTable1" class="table table-bordered table-hover text-center">
                                                <thead>
                                                    <tr>
                                                    <tr>
                                                        <th >Employee ID</th>
                                                        <th>Bio Name</th>
                                                        <!-- <th>Month</th> -->
                                                        <th >
                                                            Time In
                                                        </th>
                                                        <th>Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   <?php  if($lates!=false){ ?>
                                                   <?php  foreach ($lates as $x): ?>
                                                    <tr>
                                                        <td><?php  echo $x->Employee_ID; ?></td>
                                                        <td><?php  echo $x->Name; ?></td>
                                                        <!-- <td><?= $month_name ?></td> -->
                                                        <td><?php  echo $x->time_in; ?>   
                                                                   
                                                        </td>
                                                        <td><?php echo date("F j, Y", strtotime($x->attendance_date)); ?></td>
                                                    </tr>
                                                    <?php endforeach ?>
                                                  <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane" id="tab_2">
                                             <form method="GET" id="form-monthly">
                                                  
                                                  <div class="row">
                                                       <div class="col-lg-4">
                                                            <div class="input-group mb-3">
                                                                 <div class="input-group-prepend">
                                                                   <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                                 </div>
                                                                 <select id="month"  name="month"  class="form-control" >
                                                                      <option selected value=""></option>
                                                                      <option value='1'>January</option>
                                                                      <option value='2'>February</option>
                                                                      <option value='3'>March</option>
                                                                      <option value='4'>April</option>
                                                                      <option value='5'>May</option>
                                                                      <option value='6'>June</option>
                                                                      <option value='7'>July</option>
                                                                      <option value='8'>August</option>
                                                                      <option value='9'>September</option>
                                                                      <option value='10'>October</option>
                                                                      <option value='11'>November</option>
                                                                      <option value='12'>December</option>
                                                                    </select>  
                                                            </div>
                                                       </div>
                                                       <div class="col-lg-4">
                                                            <div class="input-group mb-3">
                                                                 <div class="input-group-prepend">
                                                                   <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                                 </div>
                                                                 <select id="year"  name="year"  class="form-control" >
                                                                      <option selected value="<?= $year?>"></option>
                                                                     
                                                                 </select>  
                                                                 <div title="Search Now" class="input-group-append" style="cursor: pointer;" onclick="submit_data()">
                                                                  <span class="input-group-text"><i class="fa fa-search"></i></span>
                                                              </div>
                                                            </div>
                                                       </div>
                                                  </div>

                                             </form>
                                             <table id="example1" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                    <tr>
                                                        <th >Employee ID</th>
                                                        <th  data-target='Name'>Name</th>
                                                        <!-- <th>Month</th> -->
                                                        <th >
                                                            Late
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <!-- <td><?= $month_name ?></td> -->
                                                        <td>
                                                           
                                                                <a name="" employee_ID="" onclick="view_details(this)"  title="View Details" href="javascript:;">     
                                                                    
                                                               
                                                                </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /.tab-content -->
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- ./card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        </section>
        <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <?php include("footer.php") ?>
<script type="text/javascript">
$(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        // $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        document.getElementById("hiddenSelect").value = start.format('YYYY-MM-DD')+" "+end.format('YYYY-MM-DD');
        // onSelectChange(start, end);
    }

    $('#reportrange').daterangepicker({

        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});
function onSelectChange(start, end){
  
  

  document.getElementById('frm').submit();
}
$(document).ready(function() {
    $('#dataTable1').DataTable( {
        responsive: {
            details: {
                type: 'column'
            }
        },
        
        order: [ 0, 'asc' ],
        "lengthMenu": [5,10,20, 40, 60, 80, 100],
    } );
} );
</script>