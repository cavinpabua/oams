
    <?php include("topnav.php") ?>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4 sidebar-light-success">
    <?php include("sidenav.php") ?>
  </aside>

    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content-header">
    <div class="container-fluid">
      </div><!-- /.container-fluid -->
        </section>
          <!-- Main content -->

	<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
          <div class="col-md-12">
             <!-- general form elements -->
            
              <div class="card card-success">
                <div class="card-header">
                  <h5  card="card-title">Edit Leave Request Form</h5>
                    </div>
                      <!-- /.card-header -->
                         <!-- form start -->    

                               
                         
                 <form  method="POST" action="<?php echo base_url();?>Welcome/editLeaveRequest/<?php echo $pendingList->row()->Request_ID; ?>" >
               
                  <div class="card-body">
                  <div class="form-group">
                     
                    <label for="exampleInputEmail1">Type of leave</label>
                    <select class="form-control" onchange="showOther(this.selectedIndex);" id="LeaveType_ID" name="LeaveType_ID" style="width: 100%;">
                    <?php if($pendingList->row()->leavetype=="Sick"){ ?>
                      <option selected="selected" disabled>Type of Leave</option>
                      <option value='Sick' selected>Sick Leave</option>
                      <option value='Maternity'>Maternity Leave</option>
                      <option value='Vacation'>Vacation Leave</option>
                      <option value="other">Others</option>
                    <?php }elseif($pendingList->row()->leavetype=="Maternity"){ ?>
                      <option selected="selected" disabled>Type of Leave</option>
                      <option value='Sick' >Sick Leave</option>
                      <option value='Maternity' selected>Maternity Leave</option>
                      <option value='Vacation'>Vacation Leave</option>
                      <option value="other">Others</option>
                    <?php }elseif($pendingList->row()->leavetype=="Vacation"){ ?>
                      <option selected="selected" disabled>Type of Leave</option>
                      <option value='Sick' >Sick Leave</option>
                      <option value='Maternity' >Maternity Leave</option>
                      <option value='Vacation' selected>Vacation Leave</option>
                      <option value="other">Others</option>
                    <?php }else{ ?>
                      <option selected="selected" disabled>Type of Leave</option>
                      <option value='Sick' >Sick Leave</option>
                      <option value='Maternity' >Maternity Leave</option>
                      <option value='Vacation' >Vacation Leave</option>
                      <option value="other" selected>Others</option>
                    <?php } ?>
                  </select>
                  </div>
               
      <?php if($pendingList->row()->leavetype=="other"){ ?>
      <input type="text" id="otherLeave" value="<?php echo $pendingList->row()->other; ?>" class="form-control" name="other" placeholder="Please input here" style="display: block;">
      <?php }else{ ?>
       <input type="text" id="otherLeave" class="form-control" name="other" placeholder="Please input here" style="display: none;"> 
      <?php } ?>

      <label for="exampleInputEmail1">Where leave will be spent</label><br>
      <?php if($pendingList->row()->LeaveSpent=="In Hospital"){ ?>
      <input type="radio" id="bk1" name="leavespent" value="In Hospital" checked> In Hospital
 
      <input type="radio" id="bk2" name="leavespent" value="Out Patient" > Out Patient
      <input type="radio" id="cr1" name="leavespent"value="Within the Philippines" disabled> Within the Philippines
      <input type="radio" id="cr2" name="leavespent" value="Abroad" disabled> Abroad
      <input type="radio" id="cr2" name="leavespent" value="" disabled> None
      <?php }elseif($pendingList->row()->LeaveSpent=="Out Patient"){ ?>
      <input type="radio" id="bk1" name="leavespent" value="In Hospital" > In Hospital
      <input type="radio" id="bk2" name="leavespent" value="Out Patient" checked> Out Patient
      <input type="radio" id="cr1" name="leavespent"value="Within the Philippines" disabled> Within the Philippines
      <input type="radio" id="cr2" name="leavespent" value="Abroad" disabled> Abroad
      <input type="radio" id="cr2" name="leavespent" value="" disabled> None
      <?php }elseif($pendingList->row()->LeaveSpent=="Within the Philippines"){ ?>
      <input type="radio" id="bk1" name="leavespent" value="In Hospital" disabled> In Hospital
      <input type="radio" id="bk2" name="leavespent" value="Out Patient" disabled> Out Patient
      <input type="radio" id="cr1" name="leavespent"value="Within the Philippines" checked> Within the Philippines
      <input type="radio" id="cr2" name="leavespent" value="Abroad" > Abroad
      <input type="radio" id="cr2" name="leavespent" value="" disabled> None
      <?php }elseif($pendingList->row()->LeaveSpent=="Abroad"){ ?>
      <input type="radio" id="bk1" name="leavespent" value="In Hospital" disabled> In Hospital
      <input type="radio" id="bk2" name="leavespent" value="Out Patient" disabled> Out Patient
      <input type="radio" id="cr1" name="leavespent"value="Within the Philippines" > Within the Philippines
      <input type="radio" id="cr2" name="leavespent" value="Abroad" checked> Abroad
      <input type="radio" id="cr2" name="leavespent" value="" disabled> None
      <?php }else{ ?>
      <input type="radio" id="bk1" name="leavespent" value="In Hospital" disabled> In Hospital
      <input type="radio" id="bk2" name="leavespent" value="Out Patient" disabled> Out Patient
      <input type="radio" id="cr1" name="leavespent"value="Within the Philippines" disabled> Within the Philippines
      <input type="radio" id="cr2" name="leavespent" value="Abroad" disabled> Abroad
      <input type="radio" id="cr2" name="leavespent" value="" checked> None
      <?php } ?>
   <br><br>
      
 <div id="dvPassport2" style="display: block">
        
        <input class="form-control" type="text" id="Text1" name="Reason" placeholder="Specify Here" readonly>
    </div>
<br>

                <div class="row">
                  <div class="col-lg-6">
                    <label for="exampleInputEmail1">From</label>
                      <div class="form-group">
                        <div class="input-group-prepend">
                           </div>
                             <input class="form-control" value="<?php echo $pendingList->row()->Date_of_LeaveFrom ?>" type="date" name="Date_of_LeaveFrom">
                                </div>
                                  <!-- /input-group -->
                                     </div>
                                        <!-- /.col-lg-6 -->
                  <div class="col-lg-6">
                     <label for="exampleInputEmail1">To</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input class="form-control" value="<?php echo $pendingList->row()->Date_of_LeaveTo ?>" type="Date" name="Date_of_LeaveTo">
                              </div>
                                <!-- /input-group -->
                                  </div>
                                     <!-- /.col-lg-6 -->
                                       </div>
                                        <!-- /.row -->

            <div class="form-group">
               <label for="exampleInputPassword1">Commutations</label><br>
               <?php if($pendingList->row()->commutations=="Requested"){ ?>
               <input type="radio" name="commutations" value="Requested" checked> Requested
               <input type="radio"  name="commutations" value="Not Requested" > Not Requested
               <input type="radio"  name="commutations" value="" > None
               <?php }elseif($pendingList->row()->commutations=="Not Requested"){ ?>
               <input type="radio" name="commutations" value="Requested" > Requested
               <input type="radio"  name="commutations" value="Not Requested" checked> Not Requested
               <input type="radio"  name="commutations" value="" > None
               <?php }else{ ?>
               <input type="radio" name="commutations" value="Requested" > Requested
               <input type="radio"  name="commutations" value="Not Requested" > Not Requested
               <input type="radio"  name="commutations" value="" checked> None
               <?php } ?>
            </div>

                      <div class="form-group">
              
                          </div>
                         <b> Number of working days applied for</b> &nbsp;<input class="form-control" type="number" name="NumberofWorkingDays" value="<?php echo $pendingList->row()->NumberofWorkingDays ?>" >
                            
                          
                            <!-- /.card-body -->  
            
            <br>
              <button type="submit" class="btn btn-success"  name="saveReq">Save</button>
              <a type="button" class="btn btn-default" href="<?php echo site_url('Welcome/myleave'); ?>">Cancel</a>
            </form>
          
            <!-- /.card -->
            <!--/.col (left) -->
</section>
</div></div>
         <!-- /.row -->
        <!-- /.content -->
<script type="text/javascript">
  function showOther(val){
      if(val==4){
      document.getElementById("otherLeave").style.display = "block";
      document.getElementById("cr1").disabled = true;
      document.getElementById("cr2").disabled = true;
      document.getElementById("bk1").disabled = true;
      document.getElementById("bk2").disabled = true;
    }else if(val==1){
      document.getElementById("cr1").disabled = true;
      document.getElementById("cr2").disabled = true;
      document.getElementById("cr2").checked = false;
      document.getElementById("cr1").checked = false;
      document.getElementById("bk1").disabled = false;
      document.getElementById("bk2").disabled = false;
      document.getElementById("Text1").readOnly  = false;
      document.getElementById("otherLeave").style.display = "none";
    }else if(val==3){
      document.getElementById("bk1").disabled = true;
      document.getElementById("bk2").disabled = true;
      document.getElementById("bk2").checked = false;
      document.getElementById("bk1").checked = false;
      document.getElementById("cr1").disabled = false;
      document.getElementById("cr2").disabled = false;
      document.getElementById("Text1").readOnly  = false;
      document.getElementById("otherLeave").style.display = "none";
    }

    else{
      document.getElementById("cr2").checked = false;
      document.getElementById("cr1").checked = false;
      document.getElementById("bk2").checked = false;
      document.getElementById("bk1").checked = false;
      document.getElementById("cr1").disabled = true;
      document.getElementById("cr2").disabled = true;
      document.getElementById("bk1").disabled = true;
      document.getElementById("bk2").disabled = true;
      document.getElementById("Text1").readOnly  = true;
      
      document.getElementById("otherLeave").style.display = "none";
    }

    
  }
</script>
       <footer class="main-footer">
                <?php include("footer.php") ?>

