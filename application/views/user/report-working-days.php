<table id="example1" class="table table-bordered table-hover">
   <thead>
      <tr>
      <tr>
         <th>Date</th>
         <th colspan="2" style="text-align: center;">AM</th>
         <th colspan="2" style="text-align: center;">PM</th>
         <th rowspan="2" style="background: #756969;color: #fff;">Working Hours</th>
      </tr>
      <tr>
         <th></th>
         <th style="text-align: center;">Time In</th>
         <th style="text-align: center;">Time Out</th>
         <th style="text-align: center;">Time In</th>
         <th style="text-align: center;">Time Out</th>
         <th></th>
      </tr>
  </thead>
  <tbody>
      <?php 
         session_start();
         include  ('../dbconnect.php'); 
         if ( isset($_SESSION['workingdays_report_from']) ) {  

            $date_from = $_SESSION['workingdays_report_from'];
            $date_to = $_SESSION['workingdays_report_to']; 
         }
         $sum_total_hours = 0;
         $total_minutes = 0;
         $employee_ID = $_POST['employee_ID'];
         if ( isset($_SESSION['workingdays_report_from']) ) {
       
            $sqli_query2="SELECT * FROM attendance WHERE employee_ID='$employee_ID'  AND  Date BETWEEN DATE('$date_from') AND DATE('$date_to')  ";
         }else{
   
            $sqli_query2="SELECT * FROM attendance WHERE employee_ID='$employee_ID'  AND  DATE(`Date`) = DATE(CURDATE()) ";
         }
        
        $result2=$conn->query($sqli_query2);
        $late_time_minutes = 0;
        $late_time_hours = 0;
         while ($row2=$result2->fetch_assoc()){
            $attendace_am  = 0;
            $attendace_pm  = 0;
            # morning attendace
            if ( $row2['Time_IN_AM'] != '00:00:00'  ) {
               
               $time_in = $row2['Time_IN_AM'];
               $time_out = $row2['Time_Out_AM']; ; 
               if ( $time_out >=12 ) {

                   $time_out = '12:00';
               } 
              
               $date1 = new DateTime($time_in);
               $date2 = $date1->diff(new DateTime($time_out));
               $attendace_am = $date2->h;
               $attendace_am_minutes = $date2->i;
            }

            if ( $row2['TimeIN_PM'] != '00:00:00'  ) {
               
               $time_in = $row2['TimeIN_PM']; 
               $time_out = $row2['TimeOUT_PM'];
               if ( $time_in <= 13 ) {

                   $time_in = '13:00';
               }  
               $date1 = new DateTime($time_in);
               $date2 = $date1->diff(new DateTime($time_out));
               $attendace_pm = $date2->h;
               $attendace_pm_minutes = $date2->i;
            }



            $total_hours = $attendace_am + $attendace_pm ;
            if ( $total_hours > 8) {

                 $total_hours = 8;
                 $total_minutes =  0;  
             }else{

                 $total_minutes = $attendace_am_minutes + $attendace_pm_minutes;
                 $total_minutes =  round($total_minutes/60*10); 
             }
           $convert_total_hours = $total_hours.'.'.$total_minutes; 
           $sum_total_hours +=$convert_total_hours;

      ?>
      <tr >
         <td><?= date('Y-m-d', strtotime($row2['Date'])) ?></td>
         <td><?= $row2['Time_IN_AM'] ?></td>
         <td><?= $row2['Time_Out_AM'] ?></td>
         <td><?= $row2['TimeIN_PM'] ?></td>
         <td><?= $row2['TimeOUT_PM'] ?></td>
         <td style="background: #47985a;color: #fff;">
            <?= $total_hours ?>  
            <?php
               if ( $total_minutes !=0 ) {
                       
                  echo '.'.$total_minutes.' hrs';
               }else{

                  echo " hrs";
               }
            ?>
         </td>
      
      </tr>
     <?php } ?>
     <tfoot style="font-weight: bold;font-size: 16px">
         <tr>
            <td colspan="5" style="text-align: right;">Total</td>
            <td><?= round($sum_total_hours,1) ?> hrs</td>
         </tr>
      </tfoot>


  </tbody>
</table>