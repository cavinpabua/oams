
                <?php include("topnav.php") ?>
            </nav>
            <!-- /.navbar -->
            <!-- Main Sidebar Container -->
            <aside class="main-sidebar elevation-4 sidebar-light-success">
                <?php include("sidenav.php") ?>
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                            </div>
                            <div class="col-sm-6">
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-12">
                            <!-- Custom Tabs -->
                            <div class="card">
                                <div class="card-header d-flex p-0">
                                    <h3 class="card-title p-3">Undertime  Reports Form</h3>
                                    <ul class="nav nav-pills ml-auto p-2">
                                       
                                       
                                    </ul>

                                </div>
                                
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="tab-content">

                                        <div class="tab-pane active" id="tab_1">
                                           
                                            <form method="post" id="frm">
                                            <div class="input-group col-sm-4">
                                                <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                        </span>
                                                    </div>
                                            <input class="form-control" placeholder="Select date range here"  type="text"  id="reportrange" autocomplete="off"/>
                                            <div  title="Search Now" class="input-group-append" style="cursor: pointer;" onclick="search_now()" >
                                                        <span onclick="onSelectChange()" class="input-group-text"><i class="fa fa-search"></i></span>
                                                    </div>
                                              
                                          </div><br>
                                            <input type="text" name="selectdate" id="hiddenSelect" hidden>
                                            <!-- /.input group -->
                                            </form>
                                             <table id="dataTable1" class="table table-bordered table-hover text-center">
                                                <thead>
                                                    <tr>
                                                    <tr>
                                                        <th >Employee ID</th>
                                                        <th  data-target='Name'>Name</th>
                                                        <th>Date</th>
                                                        <th >
                                                            Undertime
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                  <?php  if($undertime!=false){ ?>
                                                   <?php  foreach ($undertime as $x): ?>
                                                    <tr>
                                                        <td><?php  echo $x->employee_ID; ?></td>
                                                        <td><?php  echo $x->FirstName." ".$x->LastName; ?></td>
                                                        <td><?php echo date("F j, Y", strtotime($x->attendance_date)); ?></td>
                                                        <td><?php  echo $x->totalHour; ?>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach ?>
                                                  <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.tab-pane -->
                                        
                                    </div>
                                    <!-- /.tab-content -->
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- ./card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        </section>
        <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <?php include("footer.php") ?>
<script type="text/javascript">
$(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        // $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        document.getElementById("hiddenSelect").value = start.format('YYYY-MM-DD')+" "+end.format('YYYY-MM-DD');
        // onSelectChange(start, end);
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});
function onSelectChange(start, end){
  
  

  document.getElementById('frm').submit();
}
$(document).ready(function() {
    $('#dataTable1').DataTable( {
        responsive: {
            details: {
                type: 'column'
            }
        },
        
        order: [ 0, 'asc' ],
        "lengthMenu": [5,10,20, 40, 60, 80, 100],
    } );
} );
</script>