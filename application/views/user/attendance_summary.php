
                <?php include("topnav.php") ?>
            </nav>
            <!-- /.navbar -->
            <!-- Main Sidebar Container -->
            <aside class="main-sidebar elevation-4 sidebar-light-success">
                <?php include("sidenav.php") ?>
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                            </div>
                            <div class="col-sm-6">
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-12">
                            <div class="card ">
                                <div class="card-header">
                                    <h3 class="card-title">Attendance Report</h3>
                                </div>
                                   <div class="card-body">
                                    <form method="post" id="frm">
                                        <div class="input-group col-sm-4" >
                                             <select class="form-control" name="employeeID">
                                                <option value="" disabled selected>Select Employee</option>
                                                <?php  foreach ($AllEmployee as $x): ?>
                                                <option value="<?php echo $x->employee_ID; ?>"><?php echo $x->FirstName." ".$x->LastName; ?></option>
                                                <?php endforeach ?>
                                              </select>
                                         </div><br>
                                        <div class="input-group col-sm-4">
                                            <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                        <input class="form-control" placeholder="Select date range here"  type="text"  id="reportrange" autocomplete="off"/>
                                        <div  title="Search Now" class="input-group-append" style="cursor: pointer;"  >
                                                    <span onclick="onSelectChange()" class="input-group-text"><i class="fa fa-search"></i></span>
                                                </div>
                                      </div>
                                            <input type="text" name="selectdate" id="hiddenSelect" hidden>
                                            <!-- /.input group -->
                                         
                                         
                                            </form>
                                             
                                         

                                         
                                <br>
                                    <table id="dataTable1" class="table table-bordered table-hover text-center">
                                        <thead>
                                            <tr>
                                            <tr>
                                                <th>Employee ID</th>
                                                <th>Name</th>
                                               
                                                <th>Total Present</th>
                                                <th>Total Absent</th>
                                                <th>Working hours</th>
                                                <th>Absent hours</th>
                                                <th>Total hour</th>
                                            </tr>
                                            </tr>
                                        </thead>
                                        </thead>
                                        <tbody>

                                            <?php  if($workingHours!=false){ ?>
                                            <tr>
                                                <td><?php  echo $workingHours['employee_ID']; ?></td>
                                                <td><?php  echo $workingHours['FirstName']." ".$workingHours['LastName']; ?></td>
                                                <td><?php  echo $workingHours['totalPresent']; ?></td>
                                                <td><?php  echo (((int)$finalDays)-((int)$workingHours['totalPresent'])) ?></td>
                                                <td><?php  echo $workingHours['hourTotal']; ?></td>
                                                <td><?php  echo ((int)$totalHours-(int)$workingHours['hourTotal']); ?></td>
                                                <td><?php  echo $totalHours; ?></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        </section>
        <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <?php include("footer.php") ?>
    <script type="text/javascript">
$(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        // $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        document.getElementById("hiddenSelect").value = start.format('YYYY-MM-DD')+" "+end.format('YYYY-MM-DD');
        // onSelectChange(start, end);
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});
function onSelectChange(start, end){
  
  

  document.getElementById('frm').submit();
}
$(document).ready(function() {
    $('#dataTable1').DataTable( {
        responsive: {
            details: {
                type: 'column'
            }
        },
        
        order: [ 0, 'asc' ],
        "lengthMenu": [5,10,20, 40, 60, 80, 100],
    } );
} );
</script>