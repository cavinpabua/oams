
    <?php include("topnav.php") ?>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4 sidebar-light-success">
    <?php include("sidenav.php") ?>

  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<br>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card card-success">
            <div class="card-header card-header1">
              <h3 class="card-title">My Pending Request</h3>
            </div>
            <!-- .card-header -->
            <div class="card-body">
              <table id="myTable" class="table table-bordered table-hover text-center">
                <thead>
                <tr>
                  <th>Request ID</th>
                  <th>Employee name</th>
                  <th>Position</th>
                  <th>Division</th>
                  <th>Leave Type</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                  <tbody>
                    <?php foreach ($pendingList as $x): ?>
                    <tr>
                      <td><?php echo ucwords($x->Request_ID);?></td>
                      <td><?php echo ucwords($x->FirstName." ".$x->LastName); ?></td>
                      <td><?php echo ucwords($x->Position);?></td>
                      <td><?php echo ucwords($x->Division);?></td>
                      <td><?php echo ucwords($x->leavetype);?></td>
                      <td><?php echo ucwords($x->Status);?></td>
                      <td>
                        <a href="<?php echo base_url();?>Welcome/editLeaveRequest/<?php echo $x->Request_ID; ?>" class="btn btn-success">Edit</a>
                      </td>
                    </tr>
                    <?php endforeach ?>
                  </tbody>
                
              </table>
              


             
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <div class="row">
        <div class="col-12">
          <div class="card card-success">
            <div class="card-header card-header2">
              <h3 class="card-title">My Approved Request</h3>
            </div>
            <!-- .card-header -->
            <div class="card-body">
              <table id="myTable2" class="table table-bordered table-hover text-center">
                <thead>
                <tr>
                  <th>Request ID</th>
                  <th>Employee name</th>
                  <th>Position</th>
                  <th>Division</th>
                  <th>Leave Type</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Status</th>

                  <th>Action</th>
                </tr>
                </thead>
                  <tbody>
                    <?php foreach ($approveList as $x): ?>
                    <tr>
                      <td><?php echo ucwords($x->Request_ID);?></td>
                      <td><?php echo ucwords($x->FirstName." ".$x->LastName); ?></td>
                      <td><?php echo ucwords($x->Position);?></td>
                      <td><?php echo ucwords($x->Division);?></td>
                      <td><?php echo ucwords($x->leavetype);?></td>
                      <td><?php echo ucwords($x->Date_of_LeaveFrom);?></td>
                      <td><?php echo ucwords($x->Date_of_LeaveTo);?></td>
                      <td><?php echo ucwords($x->Status);?></td>
                      <td>
                        <a href="<?php echo base_url();?>Welcome/popPrint/<?php echo $x->Request_ID; ?>" class="btn btn-success specialbtn1" onclick="return !window.open(this.href, 'Report', 'width=700,height=900')">Print</a>
                      </td>
                    </tr>
                    <?php endforeach ?>
                  </tbody>
                
              </table>



             
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <div class="row">
        <div class="col-12">
          <div class="card card-success">
            <div class="card-header card-header3">
              <h3 class="card-title">My Rejected Request</h3>
            </div>
            <!-- .card-header -->
            <div class="card-body">
              <table id="myTable3" class="table table-bordered table-hover text-center">
                <thead>
                <tr>
                  <th>Request ID</th>
                  <th>Employee name</th>
                  <th>Position</th>
                  <th>Division</th>
                  <th>Leave Type</th>
                  <th>Status</th>
                </tr>
                </thead>
                  <tbody>
                    <?php foreach ($rejectedList as $x): ?>
                    <tr>
                      <td><?php echo ucwords($x->Request_ID);?></td>
                      <td><?php echo ucwords($x->FirstName." ".$x->LastName); ?></td>
                      <td><?php echo ucwords($x->Position);?></td>
                      <td><?php echo ucwords($x->Division);?></td>
                      <td><?php echo ucwords($x->leavetype);?></td>
                      <td><?php echo ucwords($x->Status);?></td>
                     
                    </tr>
                    <?php endforeach ?>
                    
                  </tbody>
                
              </table>



             
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>


      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  
  <!-- /.content-wrapper -->
  <footer class="main-footer">
  <?php include("footer.php");?>

  <script type="text/javascript">
            $(document).ready( function () {
                $('#myTable').DataTable(
                    {
                    "lengthMenu": [5,10,20, 40, 60, 80, 100],
                      "ordering": false
                    }
                    );
                });
            $(document).ready( function () {
                $('#myTable2').DataTable(
                    {
                    "lengthMenu": [5,10,20, 40, 60, 80, 100],
                      "ordering": false
                    }
                    );
                });
             $(document).ready( function () {
                $('#myTable3').DataTable(
                    {
                    "lengthMenu": [5,10,20, 40, 60, 80, 100],
                      "ordering": false
                    }
                    );
                });
        </script>