
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>HRISDA | Leave Request</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
  
   $(function () {
        $("#LeaveType").change(function () {
            if ($(this).val() == "SickLeave") {
                $("#dvPassport").show();
            } else {
                $("#dvPassport").hide();
            }
        });
    });
     $(function () {
        $("#LeaveType").change(function () {
            if ($(this).val() == "VacationLeave") {
                $("#dvleave").show();
            } else {
                $("#dvleave").hide();
            }
        });
    });
       $(function () {
        $("#LeaveType").change(function () {
            if ($(this).val() == "SickLeave") {
                $("#dvsick").show();
            } else {
                $("#dvsick").hide();
            }
        });
    });
         $(function () {
            $("#others").click(function () {
                if ($(this).is(":checked")) {
                    $("#dvPassport2").show();
                } else {
                    $("#dvPassport2").hide();
                }
            });
        });
 
</script>
<script>
function DoCheckUncheckDisplay(d,dchecked,dunchecked)
{
   if( d.checked == true )
   {
      document.getElementById(dchecked).style.display = "block";
      document.getElementById(dunchecked).style.display = "none";
   }
   else
   {
      document.getElementById(dchecked).style.display = "none";
      document.getElementById(dunchecked).style.display = "block";
   }
}
</script>
<style type="text/css">
  input[type=text], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
  input[type=number], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
  input[type=date], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
input[type=submit] {
    width: 100%;
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

input[type=submit]:hover {
    background-color: #45a049;
}
textarea {
    width: 100%;
    height: 150px;
    padding: 12px 20px;
    box-sizing: border-box;
    border: 2px solid #ccc;
    border-radius: 4px;
    
    font-size: 16px;
    resize: none;
}
/* The container */
.container {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
   
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* Hide the browser's default checkbox */
.container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
}

/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container input:checked ~ .checkmark {
    background-color: #008000;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
    display: block;
}

/* Style the checkmark/indicator */
.container .checkmark:after {
    left: 9px;
    top: 5px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
</style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand border-bottom navbar-dark bg-success">
    <?php include("topnav.php") ?>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4 sidebar-light-success">
    <!-- Brand Logo -->
    <a href="dashboard.php"  class="brand-link bg-success">
      <img src="../dist/img/da.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
       <span class="brand-text font-weight-bold">OAMSDA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
<?php include("logo1.php") ?>

      <!-- Sidebar Menu -->
    <nav class="main-header navbar navbar-expand border-bottom navbar-dark bg-success">
    <!-- Left navbar links -->
    <?php include("topnav.php") ?>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4 sidebar-light-success">
    <?php include("sidenav.php") ?>
  </aside>

    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content-header">
    <div class="container-fluid">
      </div><!-- /.container-fluid -->
        </section>
          <!-- Main content -->

	<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
          <div class="col-md-12">
             <!-- general form elements -->
            
              <div class="card card-success">
                <div class="card-header">
                  <h5  card="card-title">Leave Request Form</h5>
                    </div>
                      <!-- /.card-header -->
                         <!-- form start -->
                         <?php

if(isset($_GET['Request_ID'])){
    include ("dbconnect.php");
$Request_ID = $_GET['Request_ID'];
$sqli_query="SELECT CONCAT(e.`FirstName` , e.`Lastname`) AS Name,e.employee_ID,e.Division,e.Position, emr.LeaveType,emr.Reason, Date_Applied,emr.Date_of_LeaveFrom,emr.Date_of_LeaveTo,emr.Request_ID,emr.NumberofWorkingDays FROM employee AS e, `employee_leaverequest` AS emr WHERE e.employee_ID = emr.`Employee_ID`";
$result=$conn->query($sqli_query);
if ($result->num_rows>0){
while ($row=$result->fetch_assoc()){
?>
               <form role="form" method="POST" >
                  <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Type of leave</label>
                    <select  id="LeaveType" style="width: 100%;">
                    <option selected="selected" ><?php echo htmlspecialchars($row['LeaveType']);?></option>
                    <option value='SickLeave'>Sick Leave</option>
                    <option value='MaternityLeave'>Maternity Leave</option>
                    <option value='VacationLeave'>Vacation Leave</option>
                  </select>
                  </div>
               
 <div id="dvleave" style="display: none">
  
 
    <label class="container"> Within the Philippines
  <input type="checkbox"  >
  <span class="checkmark"></span>
</label>
      <label class="container"> Abroad
  <input type="checkbox" >
  <span class="checkmark"></span>
</label>
      
</div>
 <div id="dvsick" style="display: none">
  
      <label class="container"> In Hospital
  <input type="checkbox" >
  <span class="checkmark"></span>
</label>
      <label class="container"> Out Patient
  <input type="checkbox" >
  <span class="checkmark"></span>
</label>
</div>
 <div id="dvPassport2" style="display: none">
        
        <input type="text" id="Text1" placeholder="Enter text here" />
    </div>


<div id="checkbox-checked" style="display:none;">
<input type="checkbox" id="sick" /> Sick Leave
      <input type="checkbox" id="mat" /> Maternity LeaveTypee
</div>

                <div class="row">
                  <div class="col-lg-6">
                    <label for="exampleInputEmail1">From</label>
                      <div class="form-group">
                        <div class="input-group-prepend">
                           </div>
                             <input type="Date" id="Date_of_LeaveFrom" placeholder="<?php echo htmlspecialchars($row['Date_of_LeaveFrom']);?>">
                                </div>
                                  <!-- /input-group -->
                                     </div>
                                        <!-- /.col-lg-6 -->
                  <div class="col-lg-6">
                     <label for="exampleInputEmail1">To</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="Date" id="Date_of_LeaveTo" placeholder="<?php echo htmlspecialchars($row['Date_of_LeaveFrom']);?>">
                              </div>
                                <!-- /input-group -->
                                  </div>
                                     <!-- /.col-lg-6 -->
                                       </div>
                                        <!-- /.row -->

            <div class="form-group">
               <label for="exampleInputPassword1">Reason</label>
                  <textarea id="Reason"  placeholder="<?php echo htmlspecialchars($row['Reason']);?>"></textarea>
                      </div>
                      <div class="form-group">
              
                          </div>
                         <b> Number of working days applied for</b> &nbsp;<input type="number" id="NumberofWorkingDays" placeholder="<?php echo htmlspecialchars($row['NumberofWorkingDays']);?>" />
                          <?php }}}?>
                          <input type="hidden" id="Request_ID" />
                            <!-- /.card-body -->  
            
            <div class="card-footer">
              <button type="submit"  id="submit" class="btn btn-success">Submit</button>
              <button type="button" class="btn btn-default">Cancel</button>
            </div>
            </form>
          
            </div>
            <!-- /.card -->
            </div>
            <!--/.col (left) -->

         </div>
         <!-- /.row -->
        </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
        </div>

  <!-- /.content-wrapper -->
 <?php
                include("dbconnect.php") ;         
if (isset($_POST['submit'])) 
                        {
$Request_ID= $_REQUEST['Request_ID'];
$LeaveType= $_REQUEST['LeaveType'];

$Date_of_LeaveFrom= $_REQUEST['Date_of_LeaveTo'];
$Date_of_LeaveTo = $_REQUEST['Date_of_LeaveTo'];
$Reason = $_REQUEST['Reason'];
$NumberofWorkingDays = $_REQUEST['NumberofWorkingDays'];



$sql = ("UPDATE employee_leaverequest SET `LeaveType`='$LeaveType',`Date_of_LeaveFrom`='$Date_of_LeaveTo',`Reason`='$Reason',`NumberofWorkingDays`='$NumberofWorkingDays'  WHERE Request_ID = $Request_ID");
if($conn->query($sql)=== TRUE)
{
    
    echo "<script type='text/javascript'>
        alert('successfully updated Student Info.');
        window.location.href='leave-request-list.php';
        
        
        document.getElementByID('success').style.display = 'block';
    </script>";
    
    
}
else 
{
    echo "OH, SNAP! " . $sql . "<br>" . $conn->error;
}
$conn->close();
}

  ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  </div>
   <!-- ./wrapper -->

<!-- jQuery -->

<script type="text/javascript">
    function printpage() {
        //Get the print button and put it into a variable
        var printButton = document.getElementById("printpagebutton");
        //Set the print button visibility to 'hidden' 
        printButton.style.visibility = 'hidden';

        //Print the page content
        window.print()
        //Set the print button to 'visible' again 
        //[Delete this line if you want it to stay hidden after printing]
        printButton.style.visibility = 'visible';
    }
</script>
<script>
$(document).ready(function(){
    $('#LeaveType').on('change', function(){
        var theVal = $(this).val();
        $('.answer').addClass('hidden');
        $('.answer#answer' + theVal).removeClass('hidden');
    });
});
</script>
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- SlimScroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- page script -->

</body>
</html>
