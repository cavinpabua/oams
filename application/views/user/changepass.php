
    <?php include("topnav.php") ?>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4 sidebar-light-success">
    <?php include("sidenav.php")?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <?php if(isset($_SESSION['success'])){?>
              <span style="color:green;"><?php echo $_SESSION['success']; ?></span>
            <?php } ?>
            <?php if(isset($_SESSION['fail'])){?>
              <span style="color:red;"><?php echo $_SESSION['fail']; ?></span>
            <?php } ?>
            <!-- general form elements -->
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Change Password</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
         <form method="post" action="<?php echo site_url('Welcome/changepass'); ?>">
            <div class="card-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-4">
                    <label for="exampleInputEmail1">Current Password</label>
                      <div class="form-group">
                        <div class="input-group-prepend">
                           </div>
                             <input type="password" class="form-control" placeholder="" name="currentPass">
                                </div>
                                  <!-- /input-group -->
                     <label for="exampleInputEmail1">New Password</label>
                    <div class="form-group">
                      <div class="input-group-prepend">
                          </div>
                            <input type="password" name="newpass" class="form-control" placeholder=""  >
                              </div>
                                <!-- /input-group -->
                                  </div>
              
                                     <!-- /.col-lg-6 -->
                                      
               

                
                  </div><br>
           
                <!-- /.card-body -->  
                
              
            </div>
                  <button type="submit" name="changeNow" class="btn btn-success">Save</button>
                  </form>
          </div>

          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
  <?php include("footer.php") ?>
