
                <?php include("topnav.php") ?>
            </nav>
            <!-- /.navbar -->
            <!-- Main Sidebar Container -->
            <aside class="main-sidebar elevation-4 sidebar-light-success">
                <?php include("sidenav.php") ?>
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                            </div>
                            <div class="col-sm-6">
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-12">
                            <!-- Custom Tabs -->
                            <div class="card">
                                <div class="card-header d-flex p-0">
                                    <h3 class="card-title p-3">Generate DTR</h3>
                                    <ul class="nav nav-pills ml-auto p-2">
                                        
                                    </ul>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_1">
                                            <!-- Date range -->
                                            
                                            <div class="form-group" style="width: 300px">
                                                
                                              <form method="post" id="frm">

                                                <?php if($_SESSION['user_type']=="admin"){ ?>
                                                    <div class="radio">
                                                  <label><input type="radio" value="1" onclick="handleClick(this);" name="optradio" checked>Generate Individually</label>
                                                </div>
                                                <div class="radio">
                                                  <label><input type="radio" value="2" onclick="handleClick(this);" name="optradio">Generate By Division</label>
                                                </div>
                                              <select class="form-control  selectpicker" data-live-search="true" name="employeeID" id="employee_ID" >

                                                <option value="" disabled selected>Select by Employee</option>
                                                <?php  foreach ($AllEmployee as $x): ?>
                                                <?php if($x->FirstName!=""){ ?>
                                                <option data-subtext="<?php echo $x->employee_ID; ?>" value="<?php echo $x->employee_ID; ?>">
                                                    <?php echo $x->FirstName." ".$x->LastName; ?>
                                                    
                                                </option>
                                                 <?php } ?>
                                                <?php endforeach ?>
                                              </select>
                                              <select class="form-control  selectpicker hideMe" data-live-search="true" name="department" id="department">
                                                <option value="" disabled selected>Select by Division</option>
                                                <?php  foreach ($division as $x): ?>
                                                    <option value="<?php echo $x->division_id ?>"><?php echo $x->division_name; ?></option>
                                                <?php endforeach ?>
                                              </select>

                                          <?php }else{ ?>
                                            <input type="hidden" id="employee_ID" name="employeeID" value="<?php echo $_SESSION['employee_id'] ?>">
                                          <?php } ?>
                                              
                                                <div class="input-group" style="padding-top:15px;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                        </span>
                                                    </div>
                                                    

                                                    <input class="form-control" placeholder="Select date range here"  type="text"  id="reportrange" autocomplete="off"/>
                                                    </form>
                                                    <input type="text" name="workingDays" id="hiddenSelect" hidden>
                                                    <div  title="Search Now" class="input-group-append" style="cursor: pointer;"  >
                                                      
                                                        <span onclick="onSelectChange()" class="input-group-text"><i class="fa fa-search"></i></span>
                                                    </div>

                                                </div>
                                            </div>
                                          
                                        </div>
                                        <!-- /.tab-pane -->
                                      
                                    </div>
                                    <!-- /.tab-content -->
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- ./card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        </section>
        <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <?php include("footer.php") ?>

 <script type="text/javascript">
$(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        // $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        document.getElementById("hiddenSelect").value = start.format('YYYY-MM-DD')+" "+end.format('YYYY-MM-DD');
        // onSelectChange(start, end);
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});
function onSelectChange(){

  var base_url=window.location.origin+"/oams";
  var dateRange = document.getElementById('hiddenSelect').value;
  var empID = "";
  var department = "";
  try{
    empID = document.getElementById('employee_ID').value;
    department = document.getElementById('department').value;
  }catch(err){
    empID = ""
    department = document.getElementById('department').value;
  }
  
  var href =base_url+"Welcome/genDTR?workingDays="+dateRange+"&empID="+empID+"&dep="+department;
  window.open(href, 'DTR', 'width=700,height=900');


  // document.getElementById('frm').submit();
}
$(document).ready(function() {
    $('#dataTable1').DataTable( {
        responsive: {
            details: {
                type: 'column'
            }
        },
        
        order: [ 0, 'asc' ],
        "lengthMenu": [5,10,20, 40, 60, 80, 100],
    } );
} );
</script>
<script type="text/javascript">
    function handleClick(myRadio) {
        if(myRadio.value=="1"){
            $('#employee_ID').parent().addClass('showMe');
            $('#employee_ID').parent().removeClass('hideMe');
            $('#department').parent().removeClass('showMe');
            $('#department').parent().addClass('hideMe');
           
        }else{
            $('#employee_ID').parent().addClass('hideMe');
            $('#employee_ID').parent().removeClass('showMe');

            $('#department').parent().removeClass('hideMe');
            $('#department').parent().addClass('showMe');
            
        }
        
        
    }
</script>