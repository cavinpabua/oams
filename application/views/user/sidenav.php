<?php include("logo.php") ?>  

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
  <?php include("logo1.php") ?>
<?php if($_SESSION['user_type']=="employee"){ ?>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

          <li class="nav-item has-treeview menu-open">
            <a href="<?php echo site_url('Welcome/inside'); ?>" class="nav-link active">
              </i><i class=" nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="<?php echo site_url('Welcome/genDTR'); ?>" class="nav-link active">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Generate DTR
              </p>
            </a>
          </li>
      <li class="nav-item has-treeview">
            <a href="" class="nav-link active">
              <i class="nav-icon fa fa-envelope"></i>
              <p>
                Leave Request
        <i class="fa fa-angle-left right"></i>
              </p>
            </a>
      <ul class="nav nav-treeview">
              
        <li class="nav-item">
                <a href="<?php echo site_url('Welcome/leaveRequest'); ?>" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>Request Leave</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo site_url('Welcome/myleave'); ?>" class="nav-link">
                  <i class="fa fa-dot-circle nav-icon"></i>
                  <p>My Leave Requests</p>
                </a>
              </li>
               
            </ul>
          </li>
       
      <li class="nav-item has-treeview">
            <a href="<?php echo site_url('Welcome/profile'); ?>" class="nav-link active">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Profile
              </p>
            </a>
          </li>
      <li class="nav-item has-treeview">
            <a href="<?php echo site_url('Welcome/changepass'); ?>" class="nav-link active">
              </i><i class="nav-icon fas fa-cog"></i>
              <p>
                Change Password
              </p>
            </a>
          </li>
      <li class="nav-item has-treeview">
            <a href="<?php echo site_url('Welcome/logout'); ?>" class="nav-link active">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
<?php }else{ ?>
       <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="<?php echo site_url('Welcome/inside'); ?>" class="nav-link active">
              </i><i class=" nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="<?php echo site_url('Welcome/employee'); ?>" class="nav-link active">
              <i class="nav-icon fa fa-user"></i>
              <p>
                List of Employee
              </p>
            </a>
          </li>
          
            <li class="nav-item has-treeview">
            <a href="<?php echo site_url('Welcome/leaveReqList'); ?>" class="nav-link active">
              <i class="nav-icon fa fa-edit"></i>
              <p>
                List of Leave Request
              </p>
            </a>
          </li>
          
          
            <li class="nav-item has-treeview">
            <a href="" class="nav-link active">
              <i class="nav-icon fa fa-envelope"></i>
              <p>
                Reports
        <i class="fa fa-angle-left right"></i>
              </p>
            </a>
      <ul class="nav nav-treeview">
           <li class="nav-item">
                <a href="<?php echo site_url('Welcome/genDTR'); ?>" class="nav-link">
                  <i class="fa fa-bars nav-icon"></i>
                  <p>Generate DTR</p>
                </a>
              </li>  
        <li class="nav-item">
                <a href="<?php echo site_url('Welcome/undertime'); ?>" class="nav-link">
                  <i class="fa fa-bars nav-icon"></i>
                  <p>Undertime Reports</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="<?php echo site_url('Welcome/workingdays'); ?>" class="nav-link">
                  <i class="fa fa-bars nav-icon"></i>
                  <p>Working hours Reports</p>
                </a>
              </li>

               <li class="nav-item">
                <a href="<?php echo site_url('Welcome/attendance_summary'); ?>" class="nav-link">
                  <i class="fa fa-bars nav-icon"></i>
                  <p>Attendance Reports</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo site_url('Welcome/lateness'); ?>" class="nav-link">
                  <i class="fa fa-bars nav-icon"></i>
                  <p>Late Reports</p>
                </a>
              </li>
               
               <li class="nav-item">
                <a href="<?php echo site_url('Welcome/absentsummary'); ?>" class="nav-link">
                  <i class="fa fa-bars nav-icon"></i>
                  <p>Absent  Reports</p>
                </a>
              </li>

            </ul>
          </li>

      <li class="nav-item has-treeview">
            <a href="<?php echo site_url('Welcome/profile'); ?>" class="nav-link active">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Profile
              </p>
            </a>
          </li>
      <li class="nav-item has-treeview">
            <a href="<?php echo site_url('Welcome/changepass'); ?>" class="nav-link active">
              </i><i class="nav-icon fas fa-cog"></i>
              <p>
                Change Password
              </p>
            </a>
          </li>    
      <li class="nav-item has-treeview">
            <a href="<?php echo site_url('Welcome/logout'); ?>" class="nav-link active">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
<?php } ?>