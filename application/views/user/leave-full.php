
    <?php include("topnav.php") ?>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4 sidebar-light-success">
    <?php include("sidenav.php") ?>
  </aside>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">
              <?php if(isset($_SESSION['success-reset'])){?>
              <span style="color:green;"><?php echo $_SESSION['success-reset']; ?></span>
            <?php } ?>
              <?php if(isset($_SESSION['success-disable'])){?>
              <span style="color:red;"><?php echo $_SESSION['success-disable']; ?></span>
            <?php } ?>
            <?php if(isset($_SESSION['success-enable'])){?>
              <span style="color:green;"><?php echo $_SESSION['success-enable']; ?></span>
            <?php } ?>
            </h1>
          </div>
          <div class="col-sm-6">
           
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->

            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Complete Details</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
       
            <div class="card-body">
              <div class="form-group">
            <div class="row">
              <div class="col-lg-6">
                <h2>Personal Details</h2>
                <h5>Sick Leave Credits Available: <?php 
                $this->load->model('login_model');
                $creds=$this->login_model->getLeaveCredits($requestDetails->row()->employee_ID);
                echo $creds->row()->sick_leave_credits;
                 ?></h5>
                 <h5>Vacation Leave Credits Available: <?php 
                $this->load->model('login_model');
                $creds=$this->login_model->getLeaveCredits($requestDetails->row()->employee_ID);
                echo $creds->row()->vacation_leave_credits;
                 ?></h5>
                <label>Full Name: <?php echo ucwords($requestDetails->row()->FirstName." ".$requestDetails->row()->MiddleName." ".$requestDetails->row()->LastName); ?></label><br>
                
                <label>Contact Number: <?php echo $requestDetails->row()->ContactNumber; ?></label><br>
                <label>Salary (monthly): <?php echo $requestDetails->row()->Salary; ?></label><br>
                <label>Position: <?php echo ucwords($requestDetails->row()->Position); ?></label><br>
                <label>Division: <?php echo $requestDetails->row()->Division; ?></label><br>
                <label>Status: <?php echo ucwords($requestDetails->row()->Status); ?></label><br>
                <br>
                <?php if($requestDetails->row()->Status=="pending"){ ?>
                <a class="btn btn-success" href="#" data-toggle="modal" data-target="#exampleModal">Approve</a>
                <a class="btn btn-danger" href="#" data-toggle="modal" data-target="#exampleModal2">Decline</a>
                <?php } ?>
                <a class="btn btn-default" href="<?php echo site_url('Welcome/leaveReqList'); ?>">Back</a>
              </div>
              <div class="col-lg-6">
                <h2>Leave Details</h2>
                <label>Request ID Number: <?php echo $requestDetails->row()->Request_ID; ?></label><br>
                <label>Date Applied: <?php echo date("F j, Y", strtotime($requestDetails->row()->Date_Applied)); ?></label><br>
                <label>From: <?php echo date("F j, Y", strtotime($requestDetails->row()->Date_of_LeaveFrom)); ?></label><br>
                <label>To: <?php echo date("F j, Y", strtotime($requestDetails->row()->Date_of_LeaveTo)); ?></label><br>
                <label>Number of Working Days: <?php echo $requestDetails->row()->NumberofWorkingDays; ?></label><br>
                <label>Leave Type: <?php echo $requestDetails->row()->leavetype; ?></label><br>
                <?php if($requestDetails->row()->Reason!=""){ ?>
                <label>Reason: <?php echo $requestDetails->row()->Reason; ?></label><br>
                <?php } ?>
                <?php if($requestDetails->row()->LeaveSpent!=""){ ?>
                <label>Where Leave will be spent: <?php echo $requestDetails->row()->LeaveSpent; ?></label><br>
                <?php } ?>
                <?php if($requestDetails->row()->commutations!=""){ ?>
                <label>Commutation: <?php echo $requestDetails->row()->commutations; ?></label><br>
                <?php } ?>
                <br>

               
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Approve Request </h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>

                    <form method="POST" action="<?php echo site_url('Welcome/approveReq'); ?>">
                      <input type="text" name="workdays" value="<?php echo $requestDetails->row()->NumberofWorkingDays; ?>" hidden>
                       <input type="text" name="leavetype" value="<?php echo $requestDetails->row()->leavetype; ?>" hidden>
                      <input type="text" name="empid" value="<?php echo $requestDetails->row()->employee_ID; ?>" hidden>
                      <input type="text" name="request_id" value="<?php echo $requestDetails->row()->Request_ID; ?>" hidden>
                    <div class="modal-body">
                      
                      <div class="form-group">
                        <label>Recommendation</label>
                        <select name="recommendation" class="form-control">
                          <?php foreach ($recom_signatories as $x): ?>
                            <option value="<?php echo $x->fullname.';'.$x->description ?>"><?php echo $x->fullname.' - '.$x->description ?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Approval</label>
                        <select name="approval" class="form-control">
                          <?php foreach ($app_signatories as $x): ?>
                            <option value="<?php echo $x->fullname.';'.$x->description ?>"><?php echo $x->fullname.' - '.$x->description ?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button class="btn btn-success" type="submit" >Approve</button>
                    </form>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      
                    </div>
                  </div>
                </div>
              </div>

              <!-- MODAL 2 -->
              <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Decline Request </h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      Are you sure?
                    </div>
                    <div class="modal-footer">
                      <a class="btn btn-danger" href="<?php echo base_url();?>Welcome/declineReq/<?php echo $requestDetails->row()->Request_ID; ?>" >Decline</a>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      
                    </div>
                  </div>
                </div>
              </div>
             
                <!-- EDIT DETAILS -->
             
                </div>
              </div>
              
              </div>
                <br>	
                  
                  
                
            </div>
            </div>
            </div>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <!-- /.content-wrapper -->
  <footer class="main-footer">
	<?php include("footer.php") ?>

